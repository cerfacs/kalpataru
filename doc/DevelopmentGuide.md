VSCode IDE: KalpaTARU integration
--------------------------------

We recommend VSCode for KalpaTARU development since it is free and support remote development. It has excellent C/C++/Fortran code Intellisense and CMake integration is easy with plugins.

Download the latest version of VSCode from

https://code.visualstudio.com/

Then install the following extension from VSCode marketplace,

1. C/C++ IntelliSense
2. CMake language support
3. CMake Tools
4. Code Spellchecker
5. Doxygen documentation generator (for Doxygen tag generation)
6. Modern Fortran + Fortran Intellisense (For Fortran development)
7. Git Lens (tool for Git)
8. Some markdown stuff (for documentation)
9. XML tools for editing the XDMF ParaView files

Here is a list of recommended extensions for KalpaTARU development.
![Recommended Extensions](doc/figures/vscode_extensions.gif "Recommended Extensions")


Configuring the CMake environment variables (MacOSX)
----------------------------------------------------

In order to get the project working we need to set up the environment variables necessary to run the CMake configuration phase. For this we provide a sample `taru.code-workspace` file that should be placed in the ROOT folder of KalpaTARU sources. Make sure you install flex (`brew install flex`) and bison (`brew install bison`) in Homebrew for installing Scotch. For installing the documentation using Doxygen make sure to install latex and Doxygen (`brew install doxygen`).

![Loading workspace](doc/figures/open-workspace.gif "Loading workspace")

This file should look like this

`taru.code-workspace`
```json
{
  "folders": [
    {
      "path": "."
    }
  ],
  "settings": {
    "cmake.environment": {
      "LIBGS":"/opt/homebrew/lib/libgs.dylib",
      "HDF5_ROOT":"/Users/mpkumar/bin/phdf5",
      "CC":"/Users/mpkumar/bin/mpich/bin/mpicc",
      "CXX":"/Users/mpkumar/bin/mpich/bin/mpic++",
      "FC":"/Users/mpkumar/bin/mpich/bin/mpif90",
      "METIS_DIR":"/Users/mpkumar/treepart_sandbox/metis_32bit",
      "PARMETIS_DIR":"/Users/mpkumar/treepart_sandbox/metis_32bit",
    },
    "fortran.linter.modOutput": "${workspaceRoot}/build",
    "cmake.configureArgs": [
      "-DCMAKE_INSTALL_PREFIX=${workspaceRoot}/install",
      "-DCMAKE_BUILD_TYPE=Debug",
      "-DENABLE_ASAN=OFF",
      "-DTARU_ENABLE_DOWNLOAD=ON",
      "-DTARU_USE_SYSTEM_PHDF5=ON",
      "-DTARU_ENABLE_BUNDLEDLIB=OFF",
      "-DBISON_EXECUTABLE=/opt/homebrew/Cellar/bison/3.8.2/bin/bison",
      "-DFLEX_EXECUTABLE=/opt/homebrew/Cellar/flex/2.6.4_2/bin/flex"
    ],
...
}
```

The inputs are quite obvious in the JSON file. You can specify the compiler using the build kits in VSCode. Make sure your MPI compiler is in path so that CMake picks it up by default. Otherwise, you have to add the following to the workspace JSON file.

```json
		"cmake.environment": {
			...
			"MPI_CC_COMPILER" : "path/to/c/mpi/compiler/wrapper",
			"MPI_CXX_COMPILER" : "path/to/c++/mpi/compiler/wrapper",
			"MPI_FC_COMPILER" : "path/to/fortran/mpi/compiler/wrapper",
            ...
```
*Note: The `...` in the above example means there are other entries. Please do not copy the `...` verbatim!*


You can specify the compiler using the build kits in VSCode code. For a quick start guide on using the CMake tools read the link below.

https://vector-of-bool.github.io/docs/vscode-cmake-tools/kits.html


Usually compiler kits are automatically detected from the path and loaded automatically. So VSCode will display the kits you want to use for the project (when the workspace is loaded for the first time!) as shown below.

![](doc/figures/kit_selector.png)

If you want to change the compiler kits later you can choose the kits
at the bottom (status bar).

![](doc/figures/kit_selected.png)

For details of making your own CMake kits refer to the link below,

https://vector-of-bool.github.io/docs/vscode-cmake-tools/getting_started.html

Happy coding !
