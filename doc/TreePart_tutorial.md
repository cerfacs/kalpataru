# Mesh Partitioner (do_treepart)

`do_treepart` is an unstructured mesh partitioning and load-balancing CLI tool. It exploits the hierarchical structure of the parallel hardware resources and perform a top-down partition placement to ensure hardware locality. This tool has been used to partition meshes in excess of 1.2B on more than 127k MPI ranks.

## do_treepart CLI inputs

The following are the major inputs necessary for running `do_treepart` in command line mode. The overall structure of the command line input is as follows:

```bash
mpirun -np <num_procs> do_treepart \
    -l <leafs-per-level> -p <partitioner-type> \
    -i <input-mesh-file>
```

The final partitioning is output in `el2part_#.h5` file.

1. `-l` : Number of leaves per node required in the partition hierarchy
    - for flat partition give a value `1`
    - for a 2 socket and 4 core topology use `4 2`

2. `-p` : The partitioner algorithm
   - `RCB`     (Recursive Coordinate Bisection)
   - `RIB`     (Recursive Inertial Bisection)
   - `HFSC`    (Hilbert Space-filling curve)
   -  `Scotch` (PT-Scotch/Scotch combination)
   -  `METIS`  (ParMETIS/METIS combination)
   -  `KaHIP`  under development (Karlsruhe HIGH Quality Partitioning)
   -  `GRAPH`  is deprecated (ParMetis/Metis combination)

   **Note:** *You can give different partitioner for partitioning different levels of the hierarchy. For example, `-l 4 2 -p GRAPH RCB` is a 4 core 2 socket topology, where the 2 socket partitioning uses `GRAPH` partitioner and the 4 core partitioning uses `RCB` partitioner.*

3. `-i` : The input mesh file prefix i.e. the file name after removing the `.mesh.h5` suffix
