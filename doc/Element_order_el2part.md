# Element numbering convention

KalpaTARU uses a contiguous in element type global element numbering scheme i.e., if the following is the sizes of mesh elements,

```
TET - 4
PYR - 3
PRI - 3
HEX - 3
```

TOTAL - 13 elements

Then the global element numbering (ordering looks like the following)

```
TET : | 1 | 2 | 3 | 4 |
PYR : | 5 | 6 | 7 |
PRI : | 8 | 9 | 10|
HEX : | 11| 12| 13|
```

For parallel computations the bootstrap element partition is first constructed by reading contiguous chunks of these elements into processors and a round-robin distribution in element count is adopted when the number of elements is not divisible by the number of ranks. This will ensure that we only have a one cell imbalance b/w ranks. If we distribute the 13 elements in our example into two ranks then the element count per rank is,

1. rank 0 : \f$ 13/2 = 6 + 1 = 7 \f$
2. rank 1 : \f$ 13/2 = 6 + 0 = 6 \f$

If \f$N\f$ is the number of elements and p is the number of processors and reminder of \f$N/p = r\f$, we distribute this r equally among the processors i.e., celing(p / r) ranks get one element each.

Now on rank 0 we have the following distribution of elements

```
TET : | 1 | 2 | 3 | 4 |
PYR : | 5 | 6 | 7 |
```

and rank 1 has the following distribution

```
PRI : | 8 | 9 | 10|
HEX : | 11| 12| 13|
```

## AVBP ordering

In AVBP a different distribution of elements and ordering is used where the global ordering changes with the number of processor ranks. But in KalpaTARU the global ordering is the same and independent of number
of processor ranks.

To illustrate this ordering let us take the same 13 elements example on two processor ranks.

```
---------------             ----------------
    Rank 0                       Rank 1
---------------             -----------------
TET : | 1 | 2 |             TET : | 3 | 4 |
PYR : | 5 |                 PYR : | 6 | 7 |
PRI : | 8 |                 PRI : | 9 | 10|
HEX : | 11|                 HEX : | 12| 13|
```

So each element type count \f$N_e\f$ is divided by the number of ranks \f$p\f$ and the reminder elements are put in the last rank.

## Generating el2part keys in KalpaTARU ordering

To write the el2part file we need to now generate the key values (element ids) in KalpaTARU. Now we get the global offsets of each element type i.e., for our example case it is,

```
Element type offset : [ 0,  4,  7,  10,  13 ]
```

And the local element offset in each rank is

```
-------------------------------------
                Rank 0
-------------------------------------
Local type size : [    2, 1,  1,  1 ]
Local  offset   : [ 0, 2, 3,  4,  5 ]
Global offset   : [ 0, 4, 7, 10, 13 ]
El2Part_start   : [    0, 4,  7, 10 ] (zero indexed)
-------------------------------------

-------------------------------------
                Rank 1
-------------------------------------
Local type size : [    2, 2,  2,  2 ]
Local offset    : [ 0, 2, 4,  6,  8 ]
Global offset   : [ 0, 4, 7, 10, 13 ]
El2Part_start   : [    2, 5,  8, 11 ] (zero indexed)
-------------------------------------

```

So how do we create this **El2part_start**?

- First create the global starting index \f$s_k\f$ (in each rank \f$k\f$) for each element type (and let the total number of rank be \f$p\f$). Note that reminder \f$r_k\f$ is only added to the last processor rank i.e. for \f$k=p-1\f$.

\f$s_k = \frac{k N_{type}}{p}\f$

For the example this is

\f$s_0=[ 0, 0, 0, 0]\f$

\f$s_1=[2, 1, 1, 1]\f$

- To this we add the global element offset value to get the el2part_start

\f$el2s = s_k + e_{ofs}\f$

For our simple example this is,

```
Global offset   : [ 0, 4, 7, 10, 13 ]
```

\f$el2s_0=[ 0, 4, 7, 10]\f$

\f$el2s_1=[2, 5, 8, 11]\f$

Now we simply loop over each element type and then loop over all local element sizes and then use the el2part_start to create the keys as shown,

```
    count = 0;
    for (unsigned ielm = 0; ielm < C_NumElementTypes; ++ielm) {
      for (id_size_t i = 0; i < lcl_ecnt[ielm]; ++i) {
        keys[count++] = i + el2part_start[ielm]; // zero-indexing
      }
    }
```
