# KalpaTARU - Toolkit for Topology-aware Load-balancing and Adaptation of Unstructured Meshes

![logo](doc/figures/kalpaTARU.png)

A hierarchical graph/mesh partitioning tool and library capable of generating very high quality partitioning
with complete support for a scalable running algorithm. It exploits the hierarchical structure of the parallel hardware resources and tries to optimise the partitioning and load-balancing. KalpaTARU comes in two flavours a callable external library and as standalone application. Refer to the documentation pages for more information.

## Installation

KalpaTARU uses modern CMake packaging system for a seamless installation of external dependency and the sources. Refer to [INSTALL.md](INSTALL.md) for detailed information.

## Documentation

Refer to the [documentation pages](https://open-source.pg.cerfacs.fr/treeadapt/pages.html) for various usage guide and documentation.

## Acknowledgement

This work has been supported by EU H2020 FET-HPC project EPEEC, contract #801051 and the EXCELLERAT CoE https://www.excellerat.eu/
