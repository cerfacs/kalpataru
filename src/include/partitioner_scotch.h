#ifdef __cplusplus

extern "C" {
#include <ptscotch.h>
#include <module.h>
#include <common.h>
}
#endif /* __cplusplus */

const int TARU_METIS_OK = 1;
const int TARU_METIS_ERROR = -4;

/**
 * @brief Partition using PT-Scotch
 *
 * @param vtxdist
 * @param xadj
 * @param adjncy
 * @param vwgt
 * @param adjwgt
 * @param wgtflag
 * @param numflag
 * @param nparts
 * @param twintab
 * @param edgecut
 * @param part
 * @param commptr
 * @return int
 */
int partition_ptscotch(const SCOTCH_Num *const vtxdist,
                       SCOTCH_Num *const xadj,
                       SCOTCH_Num *const adjncy,
                       SCOTCH_Num *const vwgt,
                       SCOTCH_Num *const adjwgt,
                       const SCOTCH_Num *const wgtflag,
                       const SCOTCH_Num *const numflag,
                       const SCOTCH_Num *const nparts,
                       const SCOTCH_Num *const twintab,
                       SCOTCH_Num *const edgecut,
                       SCOTCH_Num *const part,
                       MPI_Comm *commptr) {
  MPI_Comm proccomm;
  int procglbnbr;
  int proclocnum;
  SCOTCH_Num baseval;
  SCOTCH_Arch archdat;
  SCOTCH_Dgraph
      grafdat; /* Scotch distributed graph object to interface with libScotch */
  SCOTCH_Dmapping mappdat; /* Scotch distributed mapping object to interface
                              with libScotch */
  SCOTCH_Strat stradat;
  SCOTCH_Num vertlocnbr;
  SCOTCH_Num *veloloctab;
  SCOTCH_Num edgelocnbr;
  SCOTCH_Num *edloloctab;

  proccomm = *commptr;
  if (SCOTCH_dgraphInit(&grafdat, proccomm) != 0)
    goto scotch_exit_on_error_0;

  MPI_Comm_size(proccomm, &procglbnbr);
  MPI_Comm_rank(proccomm, &proclocnum);
  baseval = *numflag;
  vertlocnbr = vtxdist[proclocnum + 1] - vtxdist[proclocnum];
  edgelocnbr = xadj[vertlocnbr] - baseval;
  veloloctab = ((vwgt != NULL) && ((*wgtflag & 2) != 0)) ? vwgt : NULL;
  edloloctab = ((adjwgt != NULL) && ((*wgtflag & 1) != 0)) ? adjwgt : NULL;

  if (SCOTCH_dgraphBuild(&grafdat,
                         baseval,
                         vertlocnbr,
                         vertlocnbr,
                         xadj,
                         xadj + 1,
                         veloloctab,
                         NULL,
                         edgelocnbr,
                         edgelocnbr,
                         adjncy,
                         NULL,
                         edloloctab) != 0)
    goto scotch_exit_on_error_1;

  if (SCOTCH_stratInit(&stradat) != 0)
    goto scotch_exit_on_error_1;

#ifdef SCOTCH_DEBUG_ALL
  if (SCOTCH_dgraphCheck(&grafdat) != 0)
    goto scotch_exit_on_error_2;
#endif

  if (SCOTCH_archInit(&archdat) != 0)
    goto scotch_exit_on_error_2;

  if (SCOTCH_archCmpltw(&archdat, *nparts, twintab) != 0)
    goto scotch_exit_on_error_3;

  if (SCOTCH_dgraphMapInit(&grafdat, &mappdat, &archdat, part) != 0)
    goto scotch_exit_on_error_3;

  if (SCOTCH_dgraphMapCompute(&grafdat, &mappdat, &stradat) != 0)
    goto scotch_exit_on_error_4;

  /* Clean up of Scotch memory */
  SCOTCH_dgraphMapExit(&grafdat, &mappdat);
  SCOTCH_archExit(&archdat);
  SCOTCH_stratExit(&stradat);
  SCOTCH_dgraphExit(&grafdat);

  *edgecut = 0; /* TODO : compute real edge cut for people who might want it */
  if (baseval != 0) { /* MeTiS part array is based, Scotch is not */
    SCOTCH_Num vertlocnum;
    for (vertlocnum = 0; vertlocnum < vertlocnbr; vertlocnum++)
      part[vertlocnum] += baseval;
  }
  return TARU_METIS_OK;

  /* Return error on exit with proper clean up points */

scotch_exit_on_error_4:
  SCOTCH_dgraphMapExit(&grafdat, &mappdat);

scotch_exit_on_error_3:
  SCOTCH_archExit(&archdat);

scotch_exit_on_error_2:
  SCOTCH_stratExit(&stradat);

scotch_exit_on_error_1:
  SCOTCH_dgraphExit(&grafdat);

scotch_exit_on_error_0:
  return TARU_METIS_ERROR;
}
