/**
 *  @file treepart_app.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 15 May 2019
 *  @brief Documentation for tree_part_app.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */
#pragma once

#include "el2part.hpp"
#include "mesh.hpp"
#include "mesh_reader_hip.hpp"
#include "partitioner_hybrid_payload.hpp"
#include <CLI/CLI.hpp>

using namespace taru;

/**
 * @brief The tree-part application class that
 *        takes command line arguments and runs the
 *        partitioning code
 */
class TreePartApp : CLI::App {

public:
  /**
   * @brief The constructor for the App and the pseudo main function
   * @param nargs
   * @param args
   */
  TreePartApp(int nargs, char *args[])
      : CLI::App{"TreePart : A scalable hierarchical mesh partitioning tool"},
        m_communicator(MPI_COMM_WORLD, "world_comm") {
    InitZoltan(nargs, args);
    AddStandaloneOptions();
    AddTopologyOptions();
    AddMeshOptions();
    int error_code = ParseCLI(nargs, args);
    if (error_code > -1)
      return;

    Runner<HybridPayload, HipMeshReader, BootstrapHybridFunctor<Mesh>>();
  }

  /**
   * @brief Runner that does the actual work of LB partition
   * @tparam T_Payload
   * @tparam T_Mesh
   * @tparam T_Bootstrap
   * @tparam T_Strategy
   */
  template <typename T_Payload, typename T_MeshReader, typename T_Bootstrap>
  void Runner() {
    Time(global_timer::TIME_OVERALL, global_timer::C_TimerStart);
    if (m_communicator.IsMaster())
      std::cout << "Message: Running TreePart hierarchical strategy\n";
    TreeCommunicator<T_Payload> t(m_communicator);
    for (std::size_t i = 0; i < m_topology_leafs_per_branch.size(); ++i)
      t.AddLevel(m_topology_leafs_per_branch[i],
                 m_topology_partition_method[i]);
    T_MeshReader reader(m_mesh_filename, m_communicator);
    Mesh hip0_mesh(reader, m_communicator);
    if (!m_element_weights_file.empty())
      hip0_mesh.ReadElementWeights(m_element_weights_file,
                                   m_element_weights_dset);
    if (hip0_mesh.Is2D())
      for (std::size_t i = 0; i < t.size(); ++i)
        t[i].Payload().SetDimension(2);
    T_Bootstrap bootstrapper(hip0_mesh);
    PartitionRunner<T_Payload> partition_runner;
    partition_runner.Run(t, bootstrapper);

#if 1
    /**
     *  Write the el2part file
     */
    if (!m_disable_el2part) {
      t.Root().Barrier();
      if (t.Root().IsMaster())
        std::cout << "Message: Writing el2part file \n";
      WriteEl2PartFile(t.Root(), t.front().Payload(), hip0_mesh);
      t.Root().Barrier();
      if (t.Root().IsMaster())
        std::cout << "Message: Completed writing el2part file \n";
    }
#endif

    std::vector<rank_t> schedule;
    GetCommunicationSchedule(t.Root(), t.front().Payload(), schedule);
    std::vector<int> min_max_ngbr = {int(schedule.size()),
                                     -int(schedule.size())};
    int avg_ngbr = schedule.size();
    m_communicator.AllReduceMin(min_max_ngbr.data(), 2);
    m_communicator.AllReduceSum(&avg_ngbr, 1);
    if (m_communicator.IsMaster()) {
      std::cout << "Message: Maximum # of neighbour rank = " << -min_max_ngbr[1]
                << "\n";
      std::cout << "Message: Minimum # of neighbour rank = " << min_max_ngbr[0]
                << "\n";
      std::cout << "Message: Average # of neighbour rank = "
                << avg_ngbr / m_communicator.size() << "\n";
    }

    Time(global_timer::TIME_OVERALL, global_timer::C_TimerStop);
    m_communicator.AllReduceMax(global_timer::C_GlobalTimerValues,
                                global_timer::TIME_ARRAY_SIZE);
    global_timer::PrintTimingStats(m_communicator.Rank());
    global_timer::ClearTimers();
  }

  /**
   * @brief Function that write the el2part file
   * @tparam T_Payload
   * @param comm
   * @param payload
   * @todo Merge this with the libtreepart el2part writer
   *       cannot maintain two things (Kumar: cannot merge because
   *       libtreepart assumes a different partitioning of element
   *       types than do_treepart because AVBP does contiguous partitioning
   *       of elements in each type (num_tet / num_procs, ...) but
   *       do_treepart flats out the elements and uses round-robin
   *       distribution which is more fail safe)
   */
  template <typename T_Payload, typename T_Mesh>
  void WriteEl2PartFile(CommunicatorConcept &comm,
                        T_Payload &payload,
                        T_Mesh &mesh) {
    // Create the keys and update the hash
    rank_t my_rank = comm.Rank();
    rank_t comm_size = comm.size();
    my_size_t g_elmnt_cnt[C_NumElementTypes] = {0};
    my_size_t g_elmnt_ofs[C_NumElementTypes + 1] = {0};
    [[maybe_unused]] my_size_t lcl_eofs[C_NumElementTypes] = {0};
    my_size_t el2part_start[C_NumElementTypes] = {0};
    my_size_t lcl_ecnt[C_NumElementTypes] = {0};
    my_size_t every_body_ecnt = 0;
    my_size_t tot_ecnt = 0;
    my_size_t count = 0;
    my_size_t lcl_tot_ecnt = 0;
    /**
     *  Count number of elements in each cell type
     */
    for (const auto &item : payload.connectivity) {
      int n_elm_nod = 0;
      for (const auto &nodes : item.nodes)
        if (nodes > 0)
          n_elm_nod++;
      auto elm_type = GetElementType(n_elm_nod, mesh.Is2D());
      g_elmnt_cnt[elm_type]++;
    }
    /**
     *  Allreduce to get the global count of
     *  element of a particular cell type
     */
    comm.AllReduceSum(g_elmnt_cnt, C_NumElementTypes);
    /**
     *  Create global element offset array
     */
    for (my_size_t i = 0; i < C_NumElementTypes; ++i)
      g_elmnt_ofs[i + 1] = g_elmnt_cnt[i] + g_elmnt_ofs[i];

    // Code copied from pmesh lib (but changed to zero indexing)
    for (my_size_t i = 0; i < C_NumElementTypes; ++i) {
      lcl_ecnt[i] = g_elmnt_cnt[i] / comm_size;
      el2part_start[i] = my_rank * lcl_ecnt[i] + g_elmnt_ofs[i];
      lcl_eofs[i] = lcl_ecnt[i] * my_rank;
      lcl_tot_ecnt = lcl_tot_ecnt + lcl_ecnt[i];
      every_body_ecnt = every_body_ecnt + lcl_ecnt[i];
      tot_ecnt = tot_ecnt + g_elmnt_cnt[i];
    }
    // Be careful the sum of lcl_ecnt(i) for every proc might not give
    // g_elmnt_cnt(i) So correct on last processor
    if (my_rank == comm_size - 1) {
      lcl_tot_ecnt = 0;
      for (my_size_t i = 0; i < C_NumElementTypes; ++i) {
        // Corrected local element count is remainder+old local count
        lcl_ecnt[i] = (g_elmnt_cnt[i] - lcl_ecnt[i] * comm_size) + lcl_ecnt[i];
        lcl_tot_ecnt = lcl_tot_ecnt + lcl_ecnt[i];
      }
    }

    DistributedHashTable<id_size_t, rank_t> part_dht(comm);
    std::vector<rank_t> parts(payload.global_ids.size(), my_rank);
    part_dht.SetDebugLevel(0);
    auto g_num_elements = std::accumulate(
        g_elmnt_cnt, g_elmnt_cnt + C_NumElementTypes, my_size_t(0));
    part_dht.SetSize(g_num_elements);
    part_dht.Populate(payload.global_ids, parts);

    // Ok this is done now read corresponding elements
    // Create element GIDs using tree part scheme and put it into
    // pmesh style numbering
    std::vector<id_size_t> keys(lcl_tot_ecnt);
    parts.resize(lcl_tot_ecnt);
    /*
     *  Generate keys in the TreePart ordering for the AVBP style ordered
     * elements
     */
    count = 0;
    for (unsigned ielm = 0; ielm < C_NumElementTypes; ++ielm) {
      for (my_size_t i = 0; i < lcl_ecnt[ielm]; ++i) {
        keys[count++] = i + el2part_start[ielm]; // zero-indexing
      }
    }
    assert(count == lcl_tot_ecnt);
    comm.Barrier();
    part_dht.GetDataFromKeys(keys, parts);
    comm.Barrier();
    /* Make parts start from rank 1 (FORTRAN indexing) */
    for (auto &item : parts)
      item++;
    WriteEl2Part(comm.RawHandle(),
                 comm.Rank(),
                 comm.size(),
                 3,
                 g_num_elements,
                 parts,
                 nullptr);
  }

  /**
   * @brief Specify misc command line options
   */
  void AddStandaloneOptions() {
    this->add_flag("-g,--generate",
                   m_generate_config_flag,
                   "Generate configuration file from command line inputs");
    this->add_flag("--disable-el2part",
                   m_disable_el2part,
                   "Disable writing the el2part file");
  }

  /**
   * @brief Init zoltan library and check for errors
   * @param nargs
   * @param args
   */
  void InitZoltan(int nargs, char *args[]) {
    int rc = Zoltan_Initialize(nargs, args, &m_zoltan_version);
    if (rc != ZOLTAN_OK) {
      if (m_communicator.IsMaster())
        std::cout << "Failed to initialize Zoltan -- sorry...\n";
      MPI_Finalize();
      std::exit(0);
    }
    if (m_communicator.IsMaster())
      std::cout
          << "Message: Zoltan initialised with ZOLTAN_OK and found {version: "
          << m_zoltan_version << "}\n";
  }

  /**
   * @brief Adds the topology related options to CLI11
   */
  void AddTopologyOptions() {
    this->add_flag(
            "--auto",
            m_topology_auto_flag,
            "Automatically determine topology using Hwloc (if installed)")
        ->default_val(false);
    this->add_option("-l,--leafs-per-m_branch",
                     m_topology_leafs_per_branch,
                     "The number of leafs at each level of the topology");
    this->add_option(
            "-p,--partition-method",
            m_topology_partition_method,
            R"(The partition algorithm for cycling adaptation ("Scotch", "METIS", "KaHIP", "RCB", "RIB", "HSFC"). Note "GRAPH" is deprecated and defaults to Scotch)")
        ->required()
        ->transform(
            CLI::CheckedTransformer(partition_mapper, CLI::ignore_case));
  }

  /**
   * @brief Adds the mesh related options to CLI11
   */
  void AddMeshOptions() {
    this->add_option("-f,--format",
                     m_mesh_format,
                     "File format of the mesh (supported formats Hip)");
    this->add_option(
            "-i,--input", m_mesh_filename, "Name of the input mesh file")
        ->required();
    this->add_option("--element-weights-hdf5",
                     m_element_weights_file,
                     "Name of the HDF5 file containing the nodal weights");
    this->add_option(
        "--element-weights-dset",
        m_element_weights_dset,
        "Name of the HDF5 dataset of the nodal weights in the file");
  }

  /**
   * @brief Parse the command line options and
   *        do necessary validation of inputs
   * @param nargs
   * @param args
   * @return
   */
  int ParseCLI(int nargs, char *args[]) {
    int retval = -1;
    try {

      this->parse(nargs, args);

      if (m_mesh_filename.empty())
        throw CLI::ParseError(
            "Mesh file must be specified using sub-command mesh", 1);

      if (!m_topology_auto_flag)
        if (m_topology_leafs_per_branch.empty())
          throw CLI::ParseError(
              "Topology info must be specified using sub-command topo", 1);

      if (m_topology_partition_method.empty())
        m_topology_partition_method.emplace_back(partition_method::RIB);

      // Read topology info only if --auto is not present
      if (m_topology_auto_flag) {
        m_topology_leafs_per_branch.clear();
        CommunicatorConcept::GetTopologyInfo(m_topology_leafs_per_branch);
        if (m_communicator.IsMaster())
          std::cout << "Message: Using HWLOC auto-detect of topology\n";
      }
      // When topology input does not equal the method string
      // the last method is used for the rest of the partitions
      if (m_topology_leafs_per_branch.size() >
          m_topology_partition_method.size()) {
        auto last_item = m_topology_partition_method.size() - 1;
        m_topology_partition_method.resize(m_topology_leafs_per_branch.size());
        for (std::size_t i = last_item + 1;
             i < m_topology_partition_method.size();
             ++i)
          m_topology_partition_method[i] =
              m_topology_partition_method[last_item];
      }

      rank_t num_ranks_root = m_communicator.size();
      for (auto item : m_topology_leafs_per_branch) {
        if (num_ranks_root % item != 0)
          throw CLI::ParseError(
              "size(comm_world) not divisible by leafs_per_branch", 1);
        num_ranks_root /= item;
        if (num_ranks_root < 1)
          throw CLI::ParseError("Size mismatch: size(comm_world) != sum( "
                                "m_branch * leafs_per_branch )",
                                1);
      }

      if (!m_element_weights_file.empty() && m_element_weights_dset.empty()) {
        throw CLI::ParseError(
            "Need to specifiy the HDF5 dataset in the HDF5 file = " +
                m_element_weights_file,
            1);
      }

    } catch (const CLI::ParseError &e) {
      if (m_communicator.IsMaster())
        retval = this->exit(e);
      else
        retval = 1;
    }
    return retval;
  }

  /**
   * @brief Checks the partition method string for erroneous inputs
   * @param method
   * @return
   */
  static bool CheckPartitionMethod(const std::vector<std::string> &method) {
    bool retval = true;
    for (const auto &text : method) {
      if (text == "GRAPH" || text == "RCB" || text == "RIB" || text == "HSFC") {
      } else {
        retval = false;
      }
    }
    return retval;
  }

private:
  float m_zoltan_version = float(0.0);
  bool m_generate_config_flag = false;
  bool m_topology_auto_flag = false;
  bool m_disable_el2part = false;
  std::vector<int> m_topology_leafs_per_branch;
  std::vector<partition_method> m_topology_partition_method;
  std::string m_mesh_format = "Hip";
  std::string m_mesh_filename;
  std::string m_element_weights_file;
  std::string m_element_weights_dset;
  CommunicatorConcept m_communicator;
};
