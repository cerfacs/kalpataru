/**
 *  @file treeadapt_app.h
 *  @author Pavanakumar Mohanamuraly
 *  @date 24 September 2019
 *  @brief Documentation for adapt_app.h
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "adapt_helper.hpp"
#include "mesh.hpp"
#include "solution_field.hpp"
#include "partition_method.hpp"
#include <CLI/CLI.hpp>
#include <sys/resource.h>

using namespace taru;

/**
 * @brief The tree adapt application class that
 *        takes command line arguments and runs the
 *        partitioning code
 */
class AdaptApp : public CLI::App {

public:
  /**
   * @brief The constructor for the App and the pseudo main function
   * @param nargs
   * @param args
   */
  AdaptApp(int nargs, char *args[])
      : CLI::App{"TreeAdapt : A scalable hierarchical mesh adaptation tool"},
        m_communicator(MPI_COMM_WORLD, "world_comm") {
    InitZoltan(nargs, args);
    // CLI11 can read the inputs also from a config file
    // and enabled below
    this->set_config("--config");
    AddStandaloneOptions();
    AddTopologyOptions();
    AddMeshOptions();
    AdaptMetricOptions();
    if (ParseCLI(nargs, args) > -1)
      return;

    if (m_communicator.IsMaster() && m_generate_config_flag) {
      std::cout << "Message: CLI parameters passed to the code \n";
      std::cout << "===========================================\n";
      std::cout << this->config_to_str(true, true);
      std::cout << "===========================================\n";
    }

    InitTreeComm();

    // Read solution groups
    SolutionField solution(m_solution_file,
                           m_solution_group_ignore,
                           m_metric_input_file,
                           m_metric_input_dataset,
                           m_communicator);
    solution.SetPrefix(m_mesh_filename);
    ///////////////////////  ADAPT LOOP STARTS HERE   ///////////////////////
    ///////// Bootstrap the adaptation process by reading from file  ////////
    std::unique_ptr<Mesh> hip_mesh;
    std::stringstream cat;
    cat << m_mesh_filename << ".mesh.h5";
    HipMeshReader reader(cat.str(), m_communicator);
    hip_mesh = std::unique_ptr<Mesh>(new Mesh(reader, m_communicator));

    // MPK: Disabling since bootstrap weighting implemented
    // Gs: disable weights usage for now ( if you enable here ,
    // please change also in include/adapt_helper.hpp
    // if (m_adapt_opt.use_deviation_weights && !m_metric_input_file.empty())
    //   hip_mesh->DeviationElementWeights(m_topology_partition_method[1],
    //                                     solution.GetMetricHashTable());

    if (m_adapt_opt.m_adapt_interp_type == barycentric_interpol)
      if (m_communicator.IsMaster())
        std::cout << "Mesage: Using Barycentric Interpolation "
                  << m_adapt_opt.m_adapt_interp_type << "\n";
    if (m_adapt_opt.m_adapt_interp_type == least_squares_interpol)
      if (m_communicator.IsMaster())
        std::cout << "Mesage: Using Least-Squares Interpolation "
                  << m_adapt_opt.m_adapt_interp_type << "\n";

    if (hip_mesh->Is2D())
      for (auto &ilevel : *m_tree_comm)
        ilevel.Payload().SetDimension(2);

    auto &i_adapt_iter = m_adapt_opt.m_mmg_cur_iter;
    auto loop_time = MPI_Wtime();
    bool do_bootstrap = true;
    for (i_adapt_iter = 0; i_adapt_iter <= m_adapt_opt.m_mmg_max_iter;
         ++i_adapt_iter) {
      AdaptMetaData adapt_meta;
      auto &threshold_count = m_adapt_opt.m_threshold_count;
      m_tree_comm->SetPartitionMethod(
          m_topology_partition_method[(i_adapt_iter + 1) % 2]);
      if (m_communicator.IsMaster()) {
        std::cout << "======================================================\n";
        std::cout << "      Treeadapt step #1 loop #" << i_adapt_iter + 1
                  << "\n";
        std::cout << "======================================================\n";
      }
      if (hip_mesh->Is2D())
        m_adapt_opt.m_threshold_count = AdaptLoop2D(hip_mesh,
                                                    *m_tree_comm,
                                                    m_adapt_opt,
                                                    adapt_meta,
                                                    solution,
                                                    do_bootstrap);
      else
        m_adapt_opt.m_threshold_count = AdaptLoop3D(hip_mesh,
                                                    *m_tree_comm,
                                                    m_adapt_opt,
                                                    adapt_meta,
                                                    solution,
                                                    do_bootstrap);
      do_bootstrap = false;
      /* Check maximum use memory. */
      rusage use;
      if (getrusage(RUSAGE_SELF, &use) == 0) {
        /* On linux Kb, on OSX in bytes for now just handle linux */
        /* Need to divide by 1024 again on OSX! */
        double mem = use.ru_maxrss;
        std::vector<double> min_max_mem = {-mem, mem};
        std::vector<double> sum_mem = {mem, mem};
        m_communicator.AllReduceMax(min_max_mem.data(), 2);
        m_communicator.AllReduceSum(sum_mem.data(), 2);
        sum_mem[0] = sum_mem[0] / m_communicator.size();
        min_max_mem[0] = -min_max_mem[0]; // Trick avoid dual reduce
        if (m_communicator.IsMaster()) {
          // Rescale to get MB ( depends on platform )
          std::cout << "Message: getrusage predicted heap usage [MB]: \n"
                    << " [ Min  : " << min_max_mem[0] / RUSAGE_MEM_TO_MBYTES
                    << " - "
                    << " Max  : " << min_max_mem[1] / RUSAGE_MEM_TO_MBYTES
                    << " - "
                    << " Mean : " << sum_mem[0] / RUSAGE_MEM_TO_MBYTES << " - "
                    << " Sum  : " << sum_mem[1] / RUSAGE_MEM_TO_MBYTES
                    << " ] \n";
        }
      }

      if (threshold_count == 0) {
        if (m_communicator.IsMaster())
          std::cout << "Message: Adaptation Threshold reached for iteration "
                    << i_adapt_iter + 1 << "\n";
        break;
      }
      if (i_adapt_iter == m_adapt_opt.m_mmg_max_iter)
        if (m_communicator.IsMaster())
          std::cout
              << "Message: Max iteration reached. Threshold count remaining "
              << threshold_count << "\n";
    } ///////////////////////  ADAPT LOOP ENDS HERE   ///////////////////////
    loop_time = MPI_Wtime() - loop_time;
    if (m_communicator.IsMaster()) {
      std::cout << "Message: Total elapsed time in adaptation loop : "
                << loop_time << "s\n";
    }
  }

  /**
   *
   */
  void InitTreeComm() {
    m_tree_comm = std::unique_ptr<TreeCommunicator<HybridPayload>>(
        new TreeCommunicator<HybridPayload>(m_communicator));
    TreeCommunicator<HybridPayload> &t = *m_tree_comm;
    for (std::size_t i = 0; i < m_topology_leaves_per_branch.size(); ++i)
      t.AddLevel(m_topology_leaves_per_branch[i],
                 m_topology_partition_method[0]);
  }

  /**
   * @brief Specify misc command line options
   */
  void AddStandaloneOptions() {
    this->add_flag("-g,--generate",
                   m_generate_config_flag,
                   "Generate configuration file named \"generated.ini\" from "
                   "command line inputs");
  }

  /**
   * @brief Init zoltan library and check for errors
   * @param nargs
   * @param args
   */
  void InitZoltan(int nargs, char *args[]) {
    int rc = Zoltan_Initialize(nargs, args, &m_zoltan_version);
    if (rc != ZOLTAN_OK) {
      if (m_communicator.IsMaster())
        std::cerr << "Failed to initialize Zoltan -- sorry...\n";
      MPI_Finalize();
      std::exit(0);
    }
    if (m_communicator.IsMaster())
      std::cout
          << "Message: Zoltan initialised with ZOLTAN_OK and found {version: "
          << m_zoltan_version << "}\n";
  }

  /**
   * @brief Adds the topology related options to CLI11
   */
  void AddTopologyOptions() {
    this->add_flag(
            "--auto",
            m_topology_auto_flag,
            "Automatically determine topology using Hwloc (if installed)")
        ->default_val(false);
    this->add_option("-l,--leaves-per-branch",
                     m_topology_leaves_per_branch,
                     "The number of leaves at each level of the topology")
        ->default_val("1");
    this->add_option(
            "-p,--partition-method",
            m_topology_partition_method,
            R"(The partition algorithm for cycling adaptation ("Scotch", "METIS", "KaHIP", "RCB", "RIB", "HSFC"). "GRAPH" is now deprecated and defaults to the ParMETIS partitioner.)")
        ->required()
        ->transform(
            CLI::CheckedTransformer(partition_mapper, CLI::ignore_case));
  }

  /**
   * @brief Adds the mesh related options to CLI11
   */
  void AddMeshOptions() {
    this->add_option("-f,--format",
                     m_mesh_format,
                     "File format of the mesh (supported formats Hip)")
        ->default_val("Hip");
    this->add_option(
            "-i,--input", m_mesh_filename, "Name of the input mesh file")
        ->required();
  }

  /**
   * @brief Add the input options for adaptation metric
   */
  void AdaptMetricOptions() {
    this->add_option(
        "--metric-file",
        m_metric_input_file,
        "Input HDF5 file of the adaptation metric. If metric file is "
        "unspecified a mesh quality improvement is performed.)");
    //->required();
    this->add_option("--metric-dset",
                     m_metric_input_dataset,
                     "Name of the metric dset in HDF5 file");
    //->required();
    this->add_option(
        "--hausdorff",
        m_adapt_opt.m_mmg_hausdorff,
        "Set the hausdorff parameter (default is automatically computed)");
    this->add_option("--hGrad",
                     m_adapt_opt.m_mmg_hGrad,
                     "Set the gradient parameter (default: 1.4)")
        ->default_val("1.4");
    this->add_option("--mmg_verbosity",
                     m_adapt_opt.m_verbosity,
                     "Set mmg calls verbosity (default:-1 silent)")
        ->default_val("-1");
    this->add_option("--hmin",
                     m_adapt_opt.m_mmg_hmin,
                     "Set the minimum edge length parameter (default: None)");
    this->add_option("--hmax",
                     m_adapt_opt.m_mmg_hmax,
                     "Set the maximum edge length parameter (default: None)");
    this->add_option("--threshold",
                     m_adapt_opt.m_adapt_threshold_max,
                     "Set the maximum deviation threshold (default: 60)")
        ->default_val("60");
    this->add_option("--freeze-threshold",
                     m_adapt_opt.m_adapt_threshold_min,
                     "Set the deviation threshold to freeze node (default: 10)")
        ->default_val("10");
    this->add_flag("--write-intermediate",
                   m_adapt_opt.m_write_intermediate,
                   "enable output of intermediate files (default: disabled)");
    this->add_option("--iter-max",
                     m_adapt_opt.m_mmg_max_iter,
                     "Set the maximum iteration for the parallel adaptation "
                     "(exit when this # is reached even if the target metric "
                     "is not reached (default: 10)");
    this->add_option("--solution-file",
                     m_solution_file,
                     "The HDF5 containing the solution variables");
    this->add_option("--solution-group-ignore",
                     m_solution_group_ignore,
                     "Groups in the solution HDF5 file to ignore.");
    this->add_flag("--use-deviation-weights",
                   m_adapt_opt.use_deviation_weights,
                   "Enable deviation metric for weighting elements in the "
                   "load-balancer (default: disabled)");
    this->add_option(
            "--adapt-interp-type",
            m_adapt_opt.m_adapt_interp_type,
            "Interplation method type for metric and solution fields "
            "0 (least_squares_interpol); 1 (barycentric_interpol) (default)")
        ->default_val("1");
    this->add_flag("--do-interpolation-check",
                   m_adapt_opt.do_interpolation_check,
                   "Check and output interpolation errors using a linear "
                   "analytical function (default: disabled)");
    this->add_flag("--use-interface-weights",
                   m_adapt_opt.use_interface_weights,
                   "Enable weighting of interface elements in load-balancer "
                   "(default: disabled)");
    this->add_flag("--use-interface-edge-weights",
                   m_adapt_opt.use_interface_edge_weights,
                   "Enable weighting of interface edges/faces in load-balancer "
                   "(only GRAPH) (default: disabled)");
    this->add_option(
        "--adapt-mesh-filename",
        m_adapt_opt.m_adapt_mesh_filename,
        "Mesh file name of the final adapted mesh (default is empty string)");
  }

  /**
   * @brief Parse the command line options and
   *        do necessary validation of inputs
   *
   * @param nargs
   * @param args
   * @return
   *
   */
  int ParseCLI(int nargs, char *args[]) {
    int retval = -1;
    try {

      this->parse(nargs, args);

      if (m_mesh_filename.empty())
        throw CLI::ParseError(
            "Mesh file must be specified using sub-command mesh", 1);

      if (!m_topology_auto_flag)
        if (m_topology_leaves_per_branch.empty())
          throw CLI::ParseError(
              "Topology info must be specified using sub-command topo", 1);

      if (m_topology_partition_method.empty()) {
        m_topology_partition_method.emplace_back(partition_method::Scotch);
        m_topology_partition_method.emplace_back(partition_method::RIB);
      }

      if (m_topology_partition_method.size() == 1)
        m_topology_partition_method.emplace_back(partition_method::RIB);

      // Read topology info only if --auto is not present
      if (m_topology_auto_flag) {
        m_topology_leaves_per_branch.clear();
        CommunicatorConcept::GetTopologyInfo(m_topology_leaves_per_branch);
        if (m_communicator.IsMaster())
          std::cout << "Message: Using HWLOC auto-detect of topology\n";
      }

      // // When topology input does not equal the method string
      // // the last method is used for the rest of the partitions
      // if (m_topology_leaves_per_branch.size() >
      //     m_topology_partition_method.size()) {
      //   int last_item = m_topology_partition_method.size() - 1;
      //   m_topology_partition_method.resize(m_topology_leaves_per_branch.size());
      //   for (std::size_t i = last_item + 1;
      //        i < m_topology_partition_method.size(); ++i)
      //     m_topology_partition_method[i] =
      //         m_topology_partition_method[last_item];
      // }

      rank_t num_ranks_root = m_communicator.size();
      for (auto item : m_topology_leaves_per_branch) {
        if (num_ranks_root % item != 0)
          throw CLI::ParseError(
              "size(comm_world) not divisible by leafs_per_branch", 1);
        num_ranks_root /= item;
        if (num_ranks_root < 1)
          throw CLI::ParseError("Size mismatch: size(comm_world) != sum( "
                                "m_branch * leafs_per_branch )",
                                1);
      }
    } catch (const CLI::ParseError &e) {
      if (m_communicator.IsMaster())
        retval = this->exit(e);
      else
        retval = 1;
    }

    // Print the configuration file for user to view
    if (m_communicator.IsMaster()) {
      if (m_generate_config_flag) {
        std::ofstream fout("generated.ini");
        fout << this->config_to_str(true, true);
      }
    }

    return retval;
  }

private:
  float m_zoltan_version = float(0.0);
  bool m_generate_config_flag = false;
  bool m_topology_auto_flag = false;
  AdaptOptions m_adapt_opt;
  std::vector<int> m_topology_leaves_per_branch;
  std::vector<partition_method> m_topology_partition_method;
  std::string m_mesh_format = "Hip";
  std::string m_mesh_filename;
  std::unique_ptr<TreeCommunicator<HybridPayload>> m_tree_comm;
  CommunicatorConcept m_communicator;
  std::string m_metric_input_file;
  std::string m_metric_input_dataset;
  std::string m_solution_file;
  std::vector<std::string> m_solution_group_ignore;
};
