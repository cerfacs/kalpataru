#pragma once
/**
 * @file partition_method.hpp
 * @author Pavanakumar Mohanamuraly (mpkumar@cerfacs.fr)
 * @brief
 *
 * @copyright Copyright (c) 2024 COOP Team CERFACS
 *
 */

#include <string>
#include <map>

namespace taru {

/**
 * @brief
 *
 */
enum class partition_method : unsigned long {
  Scotch = 0,
  METIS = 1,
  KaHIP = 2,
  RCB = 3,
  RIB = 4,
  HSFC = 5,
  LAST = 6
};

/**
 * @brief
 *
 */
const std::string
    C_PARTITION_METHOD_STRING[static_cast<int>(partition_method::LAST)]{
        "Scotch", // Scotch
        "METIS",  // METIS
        "KaHIP",  // KaHIP
        "RCB",    // RCB
        "RIB",    // RIB
        "HSFC"    // HSFC
    };

/**
 * @brief
 *
 */
const std::map<std::string, partition_method> partition_mapper{
    {"Scotch", partition_method::Scotch},
    {"METIS", partition_method::METIS},
    {"GRAPH", partition_method::METIS}, // to be deprecated
    {"KaHIP", partition_method::KaHIP},
    {"RCB", partition_method::RCB},
    {"RIB", partition_method::RIB},
    {"HSFC", partition_method::HSFC}};

/**
 * @brief Get the partition name object
 *
 * @param p
 * @return const std::string
 */
const std::string get_partition_name(const partition_method p) {
  const auto index = static_cast<unsigned int>(p);
  return C_PARTITION_METHOD_STRING[index];
}

/**
 * @brief Returns true if partition method is a graph-based one
 *
 * @param p
 * @return true
 * @return false
 */
const bool is_graph_partitioner(const partition_method p) {
  const auto index = static_cast<unsigned>(p);
  const auto geometric = static_cast<unsigned>(partition_method::RCB);
  if (index < geometric)
    return true;
  return false;
}

} // namespace taru
