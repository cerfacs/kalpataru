/** @file mesh_dual_graph.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 07 February 2020
 *  @brief Documentation for mesh_dual_graph.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */
#pragma once

#include <connectivity.hpp>
#include <numeric>

namespace taru {

/**
 * @brief Forms the assumed node rank data.
 *
 * This function forms the assumed node rank data by sending the local partition
 * node ids to the assumed partition and receiving the data in the assumed
 * partition. It estimates the number of receives to expect from the local
 * partition in the assumed partition and removes all non-shared nodes from the
 * assumed partition.
 *
 * @param comm The communicator concept object.
 * @param local_node The vector of local node ids.
 * @param send_proc_rank The vector of send processor ranks.
 * @param send_proc_displ The vector of send processor displacements.
 * @param assumed_node_rank The map of assumed node ranks.
 */
inline void FormAssumedNodeRankData(
    CommunicatorConcept &comm,
    const std::vector<id_size_t> &local_node,
    const std::vector<rank_t> &send_proc_rank,
    const std::vector<std::size_t> &send_proc_displ,
    std::map<id_size_t, std::vector<rank_t>> &assumed_node_rank) {

  // Estimate the number of receives to expect from local partition
  // in the assumed partition
  auto num_recvs = comm.InferNumReceive(send_proc_rank);
  assert(num_recvs != 0);
  // Send the local partition node ids to assumed partition
  std::vector<MPI_Request> send_req(send_proc_rank.size());
  std::vector<MPI_Status> send_stat(send_proc_rank.size());
  auto my_rank = comm.Rank();
  for (std::size_t i = 0; i < send_proc_rank.size(); ++i) {
    auto send_count = send_proc_displ[i + 1] - send_proc_displ[i];
    auto *send_buf = &local_node[send_proc_displ[i]];
    auto send_rank = send_proc_rank[i];
    MPI_Issend(send_buf,
               send_count,
               GetMpiType<id_size_t>(),
               send_rank,
               my_rank,
               comm.RawHandle(),
               &send_req[i]);
  }
  // Receive the data in the assumed partition end using probe
  // in a temporary buffer
  std::vector<id_size_t> temp_buffer;
  MPI_Status recv_stat;
  for (int i = 0; i < num_recvs; ++i) {
    int recv_size;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
    MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
    temp_buffer.resize(recv_size);
    MPI_Recv(&temp_buffer[0],
             recv_size,
             GetMpiType<id_size_t>(),
             recv_stat.MPI_SOURCE,
             recv_stat.MPI_TAG,
             comm.RawHandle(),
             &recv_stat);
    for (const auto &item : temp_buffer)
      assumed_node_rank[item].push_back(recv_stat.MPI_SOURCE);
  }
  temp_buffer.clear();
  MPI_Waitall(send_proc_rank.size(), send_req.data(), send_stat.data());
  // Remove all non-shared nodes from the assumed partition
  for (auto item = assumed_node_rank.cbegin();
       item != assumed_node_rank.cend();) {
    if (item->second.size() == 1)
      item = assumed_node_rank.erase(item);
    else
      ++item;
  }
}

/**
 * @brief Creates a schedule from an assumed node rank map.
 *
 * This function takes a communicator, an assumed node rank map, and two empty
 * maps for the assumed and local schedules. It creates the assumed rank
 * schedule by iterating over the assumed node rank map and populating the
 * assumed schedule map. The assumed schedule map is then sorted in ascending
 * order.
 *
 * Next, the function sends the assumed shared node data to the local partition
 * using non-blocking sends. It probes the local partition to determine the
 * number of receives required and receives the data accordingly. The received
 * data is then copied to the local schedule map.
 *
 * @param comm The communicator object.
 * @param assumed_node_rank The assumed node rank map.
 * @param assumed_shed The assumed schedule map.
 * @param local_shed The local schedule map.
 */
inline void MakeScheduleFromAssumedNodeRankMap(
    CommunicatorConcept &comm,
    std::map<id_size_t, std::vector<rank_t>> &assumed_node_rank,
    std::map<rank_t, std::vector<id_size_t>> &assumed_shed,
    std::map<rank_t, std::vector<id_size_t>> &local_shed) {
  // Create the assumed rank schedule
  for (const auto &i_node : assumed_node_rank)
    for (const auto &i_rank : i_node.second)
      assumed_shed[i_rank].push_back(i_node.first);
  // Make the list of shared nodes in sorted order to match send/recv
  for (auto &i_map : assumed_shed)
    std::sort(i_map.second.begin(), i_map.second.end());
  // Transfer the data to the actual local distribution of nodes
  std::vector<MPI_Request> send_req(assumed_shed.size());
  std::vector<MPI_Status> send_stat(assumed_shed.size());
  // Send the assumed shared node data to local partition
  auto my_rank = comm.Rank();
  unsigned count = 0;
  for (const auto &item : assumed_shed) {
    auto send_count = item.second.size();
    auto *send_buf = &(item.second[0]);
    auto send_rank = item.first;
    MPI_Issend(send_buf,
               send_count,
               GetMpiType<id_size_t>(),
               send_rank,
               my_rank,
               comm.RawHandle(),
               &send_req[count++]);
  }
  // Probe in local to receive from assumed partition
  auto num_recvs = comm.InferNumReceive(assumed_shed);
  std::vector<id_size_t> temp_buffer;
  MPI_Status recv_stat;
  for (int i = 0; i < num_recvs; ++i) {
    int recv_size;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
    MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
    temp_buffer.resize(recv_size);
    MPI_Recv(&temp_buffer[0],
             recv_size,
             GetMpiType<id_size_t>(),
             recv_stat.MPI_SOURCE,
             recv_stat.MPI_TAG,
             comm.RawHandle(),
             &recv_stat);
    auto &dest = local_shed[recv_stat.MPI_SOURCE];
    const auto &source = temp_buffer;
    std::copy(source.begin(), source.end(), std::back_inserter(dest));
  }
  temp_buffer.clear();
  MPI_Waitall(assumed_shed.size(), send_req.data(), send_stat.data());
}

/**
 * @brief Calculates the assumed local schedule based on the given element
 * connectivity, communicator, and maps for assumed and local schedules.
 *
 * This function calculates the assumed local schedule by performing the
 * following steps:
 * 1. Creates a map of unique nodes and their corresponding ranks.
 * 2. Forms the assumed node rank data based on the unique nodes and their
 * ranks.
 * 3. Clears and shrinks the vectors used in the previous steps to free up
 * memory.
 * 4. Generates the assumed and local schedules based on the assumed node rank
 * map.
 *
 * @param econ The vector of element connectivity.
 * @param comm The communicator concept.
 * @param assumed_shed The map to store the assumed schedule.
 * @param local_shed The map to store the local schedule.
 */
inline void
GetAssumedLocalSchedule(const std::vector<ElementConnectivity> &econ,
                        CommunicatorConcept &comm,
                        std::map<rank_t, std::vector<id_size_t>> &assumed_shed,
                        std::map<rank_t, std::vector<id_size_t>> &local_shed) {

  std::map<id_size_t, std::vector<rank_t>> assumed_node_rank;
  // The below vectors can be destroyed once assumed_node_rank
  // is generated so I am placing them inside a local scope
  std::vector<rank_t> send_proc_rank;
  std::vector<std::size_t> send_proc_displ;
  std::vector<id_size_t> unique_nodes;
  MakeUnique(econ, comm, unique_nodes, send_proc_rank, send_proc_displ);
  FormAssumedNodeRankData(
      comm, unique_nodes, send_proc_rank, send_proc_displ, assumed_node_rank);
  comm.Barrier();
  send_proc_rank.clear();
  send_proc_rank.shrink_to_fit();
  send_proc_displ.clear();
  send_proc_displ.shrink_to_fit();
  unique_nodes.clear();
  unique_nodes.shrink_to_fit();

  MakeScheduleFromAssumedNodeRankMap(
      comm, assumed_node_rank, assumed_shed, local_shed);
}

/**
 * @brief Calculates the connectivity between nodes and elements.
 *
 * Given a vector of element connectivity and a starting element index,
 * this function calculates the connectivity between nodes and elements.
 * It populates a map where the keys are node IDs and the values are vectors
 * of element IDs that are connected to the node.
 *
 * @tparam T The type of the starting element index.
 * @param econ The vector of element connectivity.
 * @param elm_begin The starting element index.
 * @param nod_elem_con The map to store the node-element connectivity.
 */
template <typename T>
void GetNodeElementConnectivity(
    const std::vector<ElementConnectivity> &econ,
    const T &elm_begin,
    std::map<id_size_t, std::vector<id_size_t>> &nod_elem_con) {
  for (std::size_t i = 0; i < econ.size(); ++i) {
    const auto &i_nodes = econ[i].nodes;
    for (const auto &i_node : i_nodes) {
      if (i_node != 0) {
        nod_elem_con[i_node].push_back(i + elm_begin);
      }
    }
  }
}

/**
 * @brief Packs the node-element connectivity information into a map.
 *
 * This function takes in a map of node-element connectivity and two maps,
 * 'shed' and 'pack'. It iterates over the 'shed' map and for each item, it
 * retrieves the corresponding node-element connectivity from the 'nod_elem_con'
 * map. It then packs the connectivity information into the 'pack' map.
 *
 * @param nod_elem_con A map of node-element connectivity, where the key is the
 * node ID and the value is a vector of element IDs connected to the node.
 * @param shed A map of ranks and their corresponding node IDs.
 * @param pack A map of ranks and their corresponding packed connectivity
 * information.
 */
inline void PackNodeElementConnectivity(
    std::map<id_size_t, std::vector<id_size_t>> &nod_elem_con,
    std::map<rank_t, std::vector<id_size_t>> &shed,
    std::map<rank_t, std::vector<id_size_t>> &pack) {
  for (const auto &item : shed) {
    auto &my_pack = pack[item.first];
    for (const auto &i_node : item.second) {
      const auto &my_node_element = nod_elem_con[i_node];
      assert(!my_node_element.empty());
      my_pack.push_back(my_node_element.size());
      for (const auto &i_element : my_node_element)
        my_pack.push_back(i_element);
    }
  }
}

/**
 * @brief Unpacks the node-element connectivity data.
 *
 * This function unpacks the node-element connectivity data from a packed format
 * and updates the nod_elem_con map with the unpacked data. The packed data is
 * provided in the pack vector, and the node IDs are provided in the node_ids
 * vector. The function iterates over each node ID, retrieves the corresponding
 * element connectivity vector from the nod_elem_con map, and adds the elements
 * connecting to the node from the packed data. If check_empty_element is set to
 * true, the function checks if the element connectivity vector for a node is
 * empty and throws an error if it is.
 *
 * @param nod_elem_con The map storing the node-element connectivity data.
 * @param node_ids The vector of node IDs.
 * @param pack The packed data vector.
 * @param check_empty_element Flag indicating whether to check for empty element
 * connectivity.
 */
inline void UnpackNodeElementConnectivity(
    std::map<id_size_t, std::vector<id_size_t>> &nod_elem_con,
    std::vector<id_size_t> &node_ids,
    std::vector<id_size_t> &pack,
    bool check_empty_element = false) {
  ///////////
  auto cur_ptr = pack.begin();
  for (const auto &item : node_ids) {
    if (check_empty_element) {
      if (nod_elem_con.find(item) == std::end(nod_elem_con)) {
        std::cout << "Error: Empty element connectivity\n";
        abort();
      }
    }
    auto &nelem_item = nod_elem_con[item];
    auto nelems = *cur_ptr; // The first entry in the packed data is size
    ++cur_ptr;
    // Rest of the entries are the elements connecting to the node
    for (id_size_t i = 0; i < nelems; ++i, ++cur_ptr) {
      // Add only if element is not found in the array
      if (std::find(nelem_item.begin(), nelem_item.end(), *cur_ptr) ==
          std::end(nelem_item)) {
        nelem_item.push_back(*cur_ptr);
      }
    }
  }
}

/**
 * @brief Updates the node-element connectivity information between partitions.
 *
 * This function updates the node-element connectivity information between
 * partitions by exchanging data between the local partition and the assumed
 * partition.
 *
 * @param nod_elem_con The node-element connectivity map.
 * @param comm The communicator object.
 * @param assumed_shed The assumed partition's shed map.
 * @param local_shed The local partition's shed map.
 */
inline void UpdateNodeElementConnectivity(
    std::map<id_size_t, std::vector<id_size_t>> &nod_elem_con,
    CommunicatorConcept &comm,
    std::map<rank_t, std::vector<id_size_t>> &assumed_shed,
    std::map<rank_t, std::vector<id_size_t>> &local_shed) {

  std::map<id_size_t, std::vector<id_size_t>> assumed_nod_elem_con;

  // Pack data in local and send to assumed partition
  std::map<rank_t, std::vector<id_size_t>> local_packed;
  PackNodeElementConnectivity(nod_elem_con, local_shed, local_packed);
  // Send the local partition node ids to assumed partition
  std::vector<MPI_Request> send_req(local_packed.size());
  std::vector<MPI_Status> send_stat(local_packed.size());
  auto my_rank = comm.Rank();
  auto count = 0;
  for (const auto &item : local_packed) {
    auto send_count = item.second.size();
    auto *send_buf = &(item.second[0]);
    auto send_rank = item.first;
    MPI_Issend(send_buf,
               send_count,
               GetMpiType<id_size_t>(),
               send_rank,
               my_rank,
               comm.RawHandle(),
               &send_req[count++]);
  }
  comm.Barrier();
  // Receive, unpack and add to assumed partition node-element connectivity
  std::vector<id_size_t> temp_buffer;
  MPI_Status recv_stat;
  for (std::size_t i = 0; i < assumed_shed.size(); ++i) {
    int recv_size;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
    MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
    temp_buffer.resize(recv_size);
    MPI_Recv(&temp_buffer[0],
             recv_size,
             GetMpiType<id_size_t>(),
             recv_stat.MPI_SOURCE,
             recv_stat.MPI_TAG,
             comm.RawHandle(),
             &recv_stat);
    UnpackNodeElementConnectivity(
        assumed_nod_elem_con, assumed_shed[recv_stat.MPI_SOURCE], temp_buffer);
  }
  temp_buffer.clear();
  MPI_Waitall(local_packed.size(), &send_req[0], &send_stat[0]);
  comm.Barrier();
  local_packed.clear();

  // Pack data in assumed and send to local partition
  std::map<rank_t, std::vector<id_size_t>> assumed_packed;
  PackNodeElementConnectivity(
      assumed_nod_elem_con, assumed_shed, assumed_packed);
  // Send the local partition node ids to assumed partition
  send_req.clear();
  send_stat.clear();
  send_req.resize(assumed_packed.size());
  send_stat.resize(assumed_packed.size());
  count = 0;
  for (const auto &item : assumed_packed) {
    auto send_count = item.second.size();
    auto *send_buf = &(item.second[0]);
    auto send_rank = item.first;
    MPI_Issend(send_buf,
               send_count,
               GetMpiType<id_size_t>(),
               send_rank,
               my_rank,
               comm.RawHandle(),
               &send_req[count++]);
  }
  comm.Barrier();
  // Receive, unpack and add to assumed partition node-element connectivity
  for (std::size_t i = 0; i < local_shed.size(); ++i) {
    int recv_size;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm.RawHandle(), &recv_stat);
    MPI_Get_count(&recv_stat, GetMpiType<id_size_t>(), &recv_size);
    temp_buffer.resize(recv_size);
    MPI_Recv(&temp_buffer[0],
             recv_size,
             GetMpiType<id_size_t>(),
             recv_stat.MPI_SOURCE,
             recv_stat.MPI_TAG,
             comm.RawHandle(),
             &recv_stat);
    UnpackNodeElementConnectivity(
        nod_elem_con, local_shed[recv_stat.MPI_SOURCE], temp_buffer, true);
  }
  temp_buffer.clear();
  temp_buffer.shrink_to_fit();
  MPI_Waitall(assumed_packed.size(), &send_req[0], &send_stat[0]);
  comm.Barrier();
}

/**
 * @brief Makes a local dual graph based on the element connectivity and
 * node-element connectivity.
 *
 * This function takes the element connectivity, element iterator, node-element
 * connectivity, and other parameters to construct a local dual graph. The dual
 * graph represents the neighboring elements of each element in the mesh.
 *
 * @tparam T The type of the element iterator.
 * @tparam T1 The type of the adjacency list.
 * @param econ The element connectivity vector.
 * @param elm_begin The beginning iterator for the elements.
 * @param nod_elem_con The node-element connectivity map.
 * @param xadj The CSR format for the adjacency list.
 * @param adjncy The adjacency list.
 * @param is_2d Flag indicating whether the mesh is 2D or 3D.
 * @return The maximum number of neighbors for any element in the dual graph.
 */
template <typename T, typename T1>
int MakeLocalDualGraph(
    const std::vector<ElementConnectivity> &econ,
    const T &elm_begin,
    std::map<id_size_t, std::vector<id_size_t>> &nod_elem_con,
    std::vector<T1> &xadj,
    std::vector<T1> &adjncy,
    bool is_2d = false) {
  // Fix ncommon based on 2D/3D mesh
  unsigned ncommon = is_2d ? 2 : 3;
  std::vector<std::vector<id_size_t>> dual_graph(econ.size());
  std::size_t adj_size = 0;
  // Loop over all elements
  for (my_size_t i = 0; i < econ.size(); ++i) {
    auto &i_element = econ[i];
    // Loop over all nodes of element (i_element)
    // Add the connecting element (j_element) of
    // the node (i_node) to an unordered_set counter
    std::unordered_map<id_size_t, unsigned> temp;
    for (const auto &i_node : i_element.nodes) {
      if (i_node > 0)
        for (const auto &j_element : nod_elem_con[i_node])
          temp[j_element]++;
    }
    // For 2D any element in the unordered_set exceeds 2 or (3 for 3D)
    // becomes a dual graph neighbour of element (i_element)
    for (const auto &item : temp) {
      if (my_size_t(item.first) != i + elm_begin) {
        if (item.second >= ncommon) {
          dual_graph[i].push_back(item.first);
        }
      }
    }
  }
  // Copy dual_graph to xadj, adjncy CSR format for input to graph partitioner
  xadj.clear();
  xadj.resize(dual_graph.size() + 1);
  adjncy.clear();
  adjncy.reserve(adj_size);
  int max_ngbr = 0;
  for (std::size_t i = 0; i < dual_graph.size(); ++i) {
    int num_ngbr = dual_graph[i].size();
    max_ngbr = (max_ngbr < num_ngbr) ? num_ngbr : max_ngbr;
    xadj[i + 1] = xadj[i] + dual_graph[i].size();
    adjncy.insert(std::end(adjncy), dual_graph[i].begin(), dual_graph[i].end());
  }
  return max_ngbr;
}

/**
 * @brief Calculates weights for a dual graph based on the given element
 * connectivity. The weights are calculated using the vertex weights and
 * optionally the edge weights.
 *
 * @tparam T The type of the weights.
 * @param econ The element connectivity.
 * @param elm_begin The beginning index of the elements.
 * @param elm_count The number of elements.
 * @param comm The communicator concept.
 * @param xadj The adjacency array.
 * @param adjncy The adjacency list.
 * @param vtxdist The vertex distribution.
 * @param vtxwgt The vertex weights.
 * @param adjwgt The edge weights.
 * @param disable_adjwgt Flag to disable edge weights calculation.
 * @param is_2D Flag indicating if the graph is 2D.
 */
template <typename T>
void CalculateWeights(const std::vector<ElementConnectivity> &econ,
                      const my_size_t elm_begin,
                      const my_size_t elm_count,
                      CommunicatorConcept &comm,
                      std::vector<T> &xadj,
                      std::vector<T> &adjncy,
                      std::vector<T> &vtxdist,
                      std::vector<T> &vtxwgt,
                      std::vector<T> &adjwgt,
                      bool disable_adjwgt,
                      bool is_2D) {
  if (comm.IsMaster())
    std::cout << "Message: Calculating Graph Weights ";
  vtxdist.resize(comm.size() + 1, 0);
  id_size_t nelem = econ.size();
  MPI_Allgather(&nelem,
                1,
                GetMpiType<id_size_t>(),
                &vtxdist[1],
                1,
                GetMpiType<id_size_t>(),
                comm.RawHandle());
  for (rank_t i = 1; i < rank_t(vtxdist.size()) - 1; ++i)
    if (vtxdist[i] == 0)
      throw std::runtime_error(
          "Error: Found zero vertex size in dual-graph vertex dist\n");
  for (rank_t i = 0; i < rank_t(vtxdist.size()) - 1; ++i)
    vtxdist[i + 1] += vtxdist[i];
  // Fill edge weights with default value of 1
  adjwgt.resize(adjncy.size(), T(1));
  // Weights calculation
  // 1. Check if sum of weights > the number of elements (if so we need edge
  // weights)
  auto sum_of_elems = std::accumulate(vtxwgt.begin(), vtxwgt.end(), T(0));
  if (comm.IsMaster())
    if (sum_of_elems > long(vtxwgt.size()))
      std::cout << "(enabling vertex weights) ... ";
  // 2. Calculate the edge-weights based on the vertex weights
  if (!disable_adjwgt) {
    if (comm.IsMaster())
      std::cout << " (enabling edge weights) ... ";
    // Loop over the adj array and replace edges with vtxwgt > 1
    for (std::size_t i = 0; i < econ.size(); ++i)
      for (auto j = xadj[i]; j < xadj[i + 1]; ++j)
        if ((adjncy[j] - T(elm_begin)) < 0 ||
            (adjncy[j] - T(elm_begin)) >= T(elm_count))
          adjwgt[j] = T(1000);
        else if (vtxwgt[adjncy[j] - T(elm_begin)] > T(1) || vtxwgt[i] > T(1))
          adjwgt[j] = T(1000);
  }
  if (comm.IsMaster())
    std::cout << " Done\n";
}

/**
 * @brief Creates a dual graph from the given element connectivity.
 *
 * This function generates a dual graph from the provided element connectivity.
 * The dual graph represents the adjacency relationships between the nodes of
 * the mesh.
 *
 * @tparam T The type of the elements in the vectors.
 * @param econ The element connectivity vector.
 * @param comm The communicator object.
 * @param xadj The output vector that will store the adjacency information for
 * each node.
 * @param adjncy The output vector that will store the adjacent nodes for each
 * node.
 * @param is_2D Flag indicating whether the mesh is 2D or not. Default is false.
 */
template <typename T>
void MakeDualGraphKumar(const std::vector<ElementConnectivity> &econ,
                        CommunicatorConcept &comm,
                        std::vector<T> &xadj,
                        std::vector<T> &adjncy,
                        bool is_2D = false) {

  my_size_t elm_begin = 0;
  my_size_t elm_count = econ.size();
  rank_t myrank = 0;
  MPI_Exscan(&elm_count,
             &elm_begin,
             1,
             GetMpiType<my_size_t>(),
             MPI_SUM,
             comm.RawHandle());
  std::map<id_size_t, std::vector<id_size_t>> nod_elem_con;
  std::map<rank_t, std::vector<id_size_t>> assumed_shed;
  std::map<rank_t, std::vector<id_size_t>> local_shed;
  GetAssumedLocalSchedule(econ, comm, assumed_shed, local_shed);
  comm.Barrier();
  myrank = comm.Rank();
  if (myrank == 0)
    std::cout << "Message: Completed GetAssumedLocalSchedule (Dual-graph)\n";
  GetNodeElementConnectivity(econ, elm_begin, nod_elem_con);
  comm.Barrier();
  if (myrank == 0)
    std::cout << "Message: Completed GetNodeElementConnectivity (Dual-graph)\n";
  UpdateNodeElementConnectivity(nod_elem_con, comm, assumed_shed, local_shed);
  comm.Barrier();
  if (myrank == 0)
    std::cout << "Message: Completed UpdateNodeElementConnectivity "
                 "(Dual-graph)\n";
  assumed_shed.clear();
  local_shed.clear();

  auto max_ngbr =
      MakeLocalDualGraph(econ, elm_begin, nod_elem_con, xadj, adjncy, is_2D);
  comm.Barrier();
  auto my_max_ngbr = max_ngbr;
  MPI_Reduce(&max_ngbr, &my_max_ngbr, 1, MPI_INT, MPI_MAX, 0, comm.RawHandle());
  if (myrank == 0)
    std::cout
        << "Message: Completed MakeLocalDualGraph with maximum neighbours of "
        << max_ngbr << " (Dual-graph)\n";
}

/**
 * @brief Construct the dual-graph using the element connectivity and
 *        provides the edge and element weights for adaptation balancing.
 *        The way the edge weights are calculated is by first looking at
 *        non-zero element weights (interface nodes) and make sure all
 *        edges/faces sharing this element get a high value (not cut easily)
 *        *NOTE* Assumes the validity of the communicator @param comm
 *
 * @tparam T The type of the weights.
 * @param econ The element connectivity.
 * @param comm The communicator concept.
 * @param xadj The adjacency list structure for the vertices.
 * @param adjncy The adjacency list structure for the edges.
 * @param vtxdist The distribution of vertices across processes.
 * @param vtxwgt The weights of the vertices.
 * @param adjwgt The weights of the edges.
 * @param disable_adjwgt Flag indicating whether to disable edge weights.
 * @param is_2D Flag indicating whether the mesh is 2D.
 */
template <typename T>
void MakeDualGraphWgt(const std::vector<ElementConnectivity> &econ,
                      CommunicatorConcept &comm,
                      std::vector<T> &xadj,
                      std::vector<T> &adjncy,
                      std::vector<T> &vtxdist,
                      std::vector<T> &vtxwgt,
                      std::vector<T> &adjwgt,
                      bool disable_adjwgt,
                      bool is_2D) {
  my_size_t elm_begin = 0;
  my_size_t elm_count = econ.size();
  MPI_Exscan(&elm_count,
             &elm_begin,
             1,
             GetMpiType<my_size_t>(),
             MPI_SUM,
             comm.RawHandle());
  MakeDualGraphKumar(econ, comm, xadj, adjncy, is_2D);
  // Sort adjncy structure
  for (id_size_t i = 0; i < id_size_t(xadj.size()) - 2; ++i)
    std::sort(adjncy.data() + xadj[i], adjncy.data() + xadj[i + 1]);
  CalculateWeights(econ,
                   elm_begin,
                   elm_count,
                   comm,
                   xadj,
                   adjncy,
                   vtxdist,
                   vtxwgt,
                   adjwgt,
                   disable_adjwgt,
                   is_2D);
}

/**
 * @brief Generates a dual graph from the given element connectivity.
 *
 * This function generates a dual graph from the provided element connectivity.
 * The dual graph is represented using adjacency lists.
 *
 * @tparam T The type of the elements in the adjacency lists.
 * @param econ The element connectivity.
 * @param comm The communicator concept.
 * @param xadj The adjacency list for each vertex.
 * @param adjncy The adjacent vertices for each vertex.
 * @param vtxdist The distribution of vertices across processes.
 * @param disable_adjwgt Flag to disable adjacency weights (default: false).
 * @param is_2D Flag indicating if the mesh is 2D (default: false).
 */
template <typename T>
void MakeDualGraph(const std::vector<ElementConnectivity> &econ,
                   CommunicatorConcept &comm,
                   std::vector<T> &xadj,
                   std::vector<T> &adjncy,
                   std::vector<T> &vtxdist,
                   bool disable_adjwgt = false,
                   bool is_2D = false) {
  MakeDualGraphKumar(econ, comm, xadj, adjncy, is_2D);
}

} // namespace taru
