/** @file colouring.hpp
 *  @brief Documentation for colouring.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 03 November 2019
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "graph_colouring.hpp"
#include "partitioner_metis.h"
#include "treepart_config.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <set>

/*
 *  @brief Group cells into cache block sizes and colour them
 *
 *  perm (integer[ne])
 *  |-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-|
 *
 *  cell groups (integer[ng])
 *  |--------|---|------|----|----|----|-------|------|---------|
 *
 *  element type offset (obtained from eptr and eind itself integer[ntypes+1])
 *  |------TET---------------|------PYR--------|------HEX-------|
 *
 *  @param colour array (integer[ng]) stores the group ids that have the same
 * colour
 *  @param offset colour offset (integer[ng + 1])
 *  @param nn number of nodes
 *  @param ne number of elements
 *  @param mcg number of element groups
 *  @param ncommon number of common nodes (dual-graph)
 *  @param eptr Metis style element ptr
 *  @param eind Metis style element node index
 *  @param epart Metis style element partition colour
 *  @param perm array permutation to recover the reordered cache groups (size of
 * ne)
 */
inline taru::id_size_t colour_api_call1(const taru::id_size_t nn,
                                        const taru::id_size_t ne,
                                        taru::id_size_t mcg,
                                        taru::id_size_t ncommon,
                                        const taru::id_size_t *const eptr,
                                        const taru::id_size_t *const eind,
                                        taru::id_size_t *gofs,
                                        taru::id_size_t *gcolour,
                                        taru::id_size_t *epart,
                                        taru::id_size_t *const perm) {
  // Use recursive bisection since max load imbalance is very low
  taru::id_size_t options[50];
#ifdef TARU_USE_METIS
  METIS_SetDefaultOptions(options);
  // // options[METIS_OPTION_PTYPE] = METIS_PTYPE_RB;
  options[METIS_OPTION_NUMBERING] = 1; // Fortran style numbering
  options[METIS_OPTION_UFACTOR] = 1;   // 1% load imbalance
#endif
  // Allocate the part arrays
  // Call METIS to form the groups of cells
  {
    taru::id_size_t objval;
    std::vector<taru::id_size_t> npart(nn);

#if 0
    // std::cerr << "Number of cell groups " << mcg << "\n";
    // std::cerr << "ne = " << ne << "\n";
    // std::cerr << "nn = " << ne << "\n";
    // std::cerr << "eptr[0-1] = " << eptr[0] << ", " << eptr[1] << "\n";
    // std::cerr << "eind[0-2] = " << eind[0] << ", " << eind[1] << ", " <<
    // eind[2] << "\n"; std::cerr << "epart[0] = " << epart[0] << "\n";
    // std::cerr
    // << "perm[0] = " << perm[0] << "\n";
#endif

    auto metis_ret = partition_metis_serial(const_cast<taru::id_size_t *>(&ne),
                                            const_cast<taru::id_size_t *>(&nn),
                                            const_cast<taru::id_size_t *>(eptr),
                                            const_cast<taru::id_size_t *>(eind),
                                            nullptr,
                                            nullptr,
                                            &ncommon,
                                            &mcg,
                                            nullptr,
                                            options,
                                            &objval,
                                            &epart[0],
                                            &npart[0]);
    std::cerr << "METIS returned = " << metis_ret << "\n";
  }

  // Node-Element graph
  std::vector<taru::id_size_t> ieptr(nn + 1), ieind;
  std::vector<std::set<taru::id_size_t>> iegraph(nn);
  for (taru::id_size_t i = 0; i < ne; ++i)
    for (taru::id_size_t j = eptr[i] - 1; j < eptr[i + 1] - 1; ++j)
      iegraph[eind[j] - 1].insert(i + 1);

  ieptr[0] = 1;
  for (taru::id_size_t i = 0; i < nn; ++i) {
    ieptr[i + 1] = ieptr[i] + iegraph[i].size();
    std::copy(iegraph[i].begin(), iegraph[i].end(), std::back_inserter(ieind));
  }
  iegraph.clear();

  // Form the connectivity between the
  // groups using ieptr and ieind
  std::vector<taru::id_size_t> cg_xadj, cg_adjncy;
  {
    std::vector<std::set<taru::id_size_t>> cg_graph(mcg);
    for (taru::id_size_t i = 0; i < ne; ++i) {
      auto my_grp_part = epart[i] - 1;
      for (auto j = eptr[i] - 1; j < eptr[i + 1] - 1; ++j) {
        auto inode = eind[j] - 1;
        for (auto k = ieptr[inode] - 1; k < ieptr[inode + 1] - 1; ++k) {
          auto ng_grp_part = epart[ieind[k] - 1] - 1;
          cg_graph[my_grp_part].insert(ng_grp_part);
        }
      }
    }
    ieind.clear();
    ieptr.clear();

    // Convert graph to CSR format for colouring
    cg_xadj.resize(mcg + 1);
    cg_xadj[0] = 0;
    for (taru::id_size_t i = 0; i < mcg; ++i) {
      cg_xadj[i + 1] = cg_xadj[i] + cg_graph[i].size();
      std::copy(cg_graph[i].begin(),
                cg_graph[i].end(),
                std::back_inserter(cg_adjncy));
    }
  }

  // Init the colouring interface and form the group colours
  {
    std::vector<taru::id_size_t> left, right, edges;
    std::tie(left, right, edges) =
        BipartieGraphFromCSR(&cg_xadj[0], mcg, mcg, &cg_adjncy[0]);
    auto cg_colour = PartialDistanceTwoColumnColoring(left, right, edges);
    CheckPartialDistanceTwoColumnColoring(left, right, edges, cg_colour);
    // Colour the graph
    for (taru::id_size_t i = 0; i < mcg; ++i)
      gcolour[i] = cg_colour[i];
  }

  // Init the permutation (original order)
  for (taru::id_size_t i = 0; i < ne; ++i)
    perm[i] = i;

  // Reorder the global ids (element-wise) as per the group id
  std::sort(perm,
            perm + ne,
            /* Sort lambda function */
            [&epart](const taru::id_size_t &a, const taru::id_size_t &b) {
              return epart[a] < epart[b];
            });

  // Form the group offsets
  gofs[0] = 1;
  taru::id_size_t old_cg = epart[perm[0]], count = 1;
  for (taru::id_size_t i = 0; i < ne; ++i) {
    if (old_cg != epart[perm[i]]) {
      old_cg = epart[perm[i]];
      gofs[count++] = i + 1;
    }
  }
  gofs[mcg] = ne + 1;

  std::sort(epart, epart + ne);

  // Find the maximum size of a group
  taru::id_size_t max_group_size = 0;
  for (taru::id_size_t i = 0; i < mcg; ++i) {
    auto my_size = gofs[i + 1] - gofs[i];
#if 0
    std::cout << "Group-ID " << i << " cells in group " << my_size << "\n";
#endif
    if (my_size > max_group_size)
      max_group_size = my_size;
  }
  std::cout << "Maximum cells in group = " << max_group_size << "\n";
  return max_group_size;
}

/**
 * @brief For a 2D triangle mesh with group colours, write the mesh to a tecplot
 * file.
 *
 * @param nn The number of nodes.
 * @param ne The number of elements.
 * @param mcg The number of group colors.
 * @param x Pointer to the X coordinates.
 * @param y Pointer to the Y coordinates.
 * @param z Pointer to the Z coordinates (optional).
 * @param eptr Pointer to the element pointer array.
 * @param eind Pointer to the element index array.
 * @param gofs Pointer to the group offset array.
 * @param gcolour Pointer to the group color array.
 * @param perm Pointer to the permutation array.
 */
inline void write_colour_triangle1(taru::id_size_t nn,
                                   taru::id_size_t ne,
                                   taru::id_size_t mcg,
                                   double *x,
                                   double *y,
                                   double *z,
                                   taru::id_size_t *eptr,
                                   taru::id_size_t *eind,
                                   taru::id_size_t *gofs,
                                   taru::id_size_t *gcolour,
                                   taru::id_size_t *perm) {
  std::ofstream fout("tec.dat");

  // Title header
  fout << "VARIABLES =\"X\", \"Y\", ";
  if (z != nullptr)
    fout << "\"Z\", ";
  fout << "\"G_COLOUR\", \"CG_COLOUR\"\n";

  // Zone header
  fout << "ZONE DATAPACKING=BLOCK, NODES=" << nn;
  fout << ", ELEMENTS=" << ne << ", ZONETYPE=FETRIANGLE,";
  fout << " VARLOCATION=([3-4]=CELLCENTERED)\n";

  // Write coordinates
  for (taru::id_size_t i = 0; i < nn; ++i)
    fout << x[i] << "\n";
  for (taru::id_size_t i = 0; i < nn; ++i)
    fout << y[i] << "\n";
  if (z != nullptr)
    for (taru::id_size_t i = 0; i < nn; ++i)
      fout << z[i] << "\n";

  // Write group colour
  for (taru::id_size_t i = 0; i < mcg; ++i)
    for (taru::id_size_t j = gofs[i] - 1; j < gofs[i + 1] - 1; ++j)
      fout << i << "\n";
  for (taru::id_size_t i = 0; i < mcg; ++i)
    for (taru::id_size_t j = gofs[i] - 1; j < gofs[i + 1] - 1; ++j)
      fout << gcolour[i] << "\n";

  // Write the permutation to a Tecplot file
  for (taru::id_size_t ii = 0; ii < ne; ++ii) {
    auto i = perm[ii];
    for (auto j = eptr[i] - 1; j < eptr[i + 1] - 1; ++j) {
      fout << eind[j] << " ";
    }
    fout << "\n";
  }
}
