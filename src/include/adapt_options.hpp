/**
 * @file adapt_options.hpp
 * @author Pavanakumar Mohanamuraly (mpkumar@cerfacs.fr)
 * @brief Adaptation options structure
 * @copyright Copyright (c) 2023 CERFACS
 *
 */

#pragma once

#include <limits>
#include <string>

namespace taru {

const int least_squares_interpol = 0;
const int barycentric_interpol = 1;

/**
 * @brief Structure to hold the adaptation options
 *
 */
struct AdaptOptions {
  int adapt_step_counter = -1; // Non-positive counter is ignored
  int m_verbosity = -1;        // Mmg verbosity level -1 means silent.
  bool use_deviation_weights =
      false; // Use the deviation metric as graph vertex weight
  bool use_interface_weights =
      false; // Use large weight to the interface element
  bool use_interface_edge_weights =
      false; // Use large graph edge weights of interface vertices
  bool do_interpolation_check =
      false; // Check the interpolation error for a linear function
  bool m_write_intermediate = false;
  bool m_write_final = true; // write final adapted mesh
  double m_mmg_hausdorff =
      std::numeric_limits<double>::max(); // Global Hausdorff
  double m_mmg_hGrad = 1.4;               //  Global gradation
  double m_mmg_hmin =
      std::numeric_limits<double>::min(); // Global minimum edge length
  double m_mmg_hmax =
      std::numeric_limits<double>::max(); //  Global maximum edge length
  unsigned m_mmg_max_iter = 10; // Maximum iteration for the inner loop of adapt
  unsigned m_mmg_cur_iter =
      0; // Current iteration counter for the inner loop of adapt
  double m_adapt_threshold_max =
      60.0; //  max value for adaptation threshold for metric deviation
  double m_adapt_threshold_min =
      10.0; //  min value for adaptation threshold for metric deviation
  unsigned m_threshold_count =
      std::numeric_limits<unsigned>::max(); // Counter to store the nodes above
                                            // threshold
  int m_adapt_interp_type = barycentric_interpol; // Type of interp to use
  std::string m_adapt_mesh_filename; // The adapted mesh filename to write the
                                     // mesh datasets to
};

} // namespace taru
