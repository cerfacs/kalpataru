/**
 *  @file communicator_taru.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Documentation for tree_comm.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */
#pragma once

#include "communicator_level.hpp"
#include "partition_method.hpp"

namespace taru {

/**
 * @brief The tree communicator concept
 *
 */
template <typename T_Payload> class TreeCommunicator {

public:
  /**
   * @brief Root comm constructor
   *
   * @param root_communicator
   */
  explicit TreeCommunicator(const CommunicatorConcept &root_communicator)
      : m_root_communicator("root") {
    m_root_communicator.Copy(root_communicator);
    if (m_root_communicator.IsValid())
      if (m_root_communicator.IsMaster())
        std::cout << "Message: Init communicator TreeCommunicator with "
                  << m_root_communicator.size() << " ranks at root\n";
  }

  /**
   * @brief Add hierarchical levels based on leaves_per_branch
   *
   * @param leaves_per_branch
   */
  void AddLevel(const rank_t leaves_per_branch, const partition_method method) {
    CommunicatorConcept parent("add_level_parent");
    parent.Copy(m_levels.empty() ? m_root_communicator
                                 : m_levels.back().Branch());
    m_levels.emplace_back(CommunicatorLevel<T_Payload>());
    m_levels.back().SetLeavesPerBranch(leaves_per_branch);
    m_levels.back().Payload().SetPartitionMethod(method);
    if (parent.IsValid()) {
      m_levels.back().set(parent, leaves_per_branch);
      if (parent.IsMaster())
        std::cout << "Message: Created new communicator level with "
                  << m_levels.back().Branch().size() << " branches and "
                  << m_levels.back().Leaf().size() << "("
                  << m_levels.back().GetLeavesPerBranch() << ")"
                  << " leaves per m_branch\n";
    }
  }

  /**
   * @brief
   *
   * @tparam T
   * @param agg_level
   * @param data_to_agg
   */
  template <typename T>
  void Aggregate(int agg_level, std::vector<T> &data_to_agg) {
    if (m_root_communicator.IsValid())
      for (auto i = 0; i <= agg_level; ++i)
        // Aggregate m_leaf data to m_branch
        m_levels[i].Leaf().AggregateMaster(data_to_agg);
  }

  /**
   * @brief
   *
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::iterator

  begin() {
    return m_levels.begin();
  }

  /**
   * @brief
   *
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::iterator end() {
    return m_levels.end();
  }

  /**
   * @brief
   *
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::reference front() {
    return m_levels.front();
  }

  /**
   * @brief
   *
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::const_reference
  front() const {
    return m_levels.front();
  }

  /**
   * @brief
   *
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::reference back() {
    return m_levels.back();
  }

  /**
   * @brief
   *
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::const_reference
  back() const {
    return m_levels.back();
  }

  /**
   * @brief
   *
   * @param n
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::reference
  operator[](std::size_t n) {
    return m_levels[n];
  }

  /**
   * @brief
   *
   * @param n
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::const_reference
  operator[](std::size_t n) const {
    return m_levels[n];
  }

  /**
   * @brief
   *
   * @param n
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::reference
  at(std::size_t n) {
    return m_levels.at(n);
  }

  /**
   * @brief
   *
   * @param n
   * @return
   */
  typename std::vector<CommunicatorLevel<T_Payload>>::const_reference
  at(std::size_t n) const {
    return m_levels.at(n);
  }

  /**
   * @brief
   *
   * @return
   */
  typename std::size_t size() const noexcept { return m_levels.size(); }

  /**
   * @brief
   *
   * @return
   */
  typename std::size_t capacity() const noexcept { return m_levels.capacity(); }

  /**
   * @brief
   *
   * @param n
   */
  void reserve(std::size_t n) { m_levels.reserve(n); }

  /**
   * @brief select IO level
   *
   * @param in
   */
  void set_io_level(rank_t in) { m_io_level = in; }

  /**
   *
   * @return
   */
  bool IsValidTreeRoot() { return m_root_communicator.IsValid(); }

  /**
   * @brief
   *
   * @return
   */
  CommunicatorConcept &Root() { return m_root_communicator; }

  /**
   *
   * @return
   */
  bool IsMasterTreeRoot() { return m_root_communicator.IsMaster(); }

  /**
   * @brief Set the Partition Method object
   *
   * @param method
   */
  void SetPartitionMethod(const partition_method method) {
    for (auto &level : m_levels)
      level.Payload().SetPartitionMethod(method);
  }

private:
  rank_t m_io_level =
      0; //!< Used to input/identify the level that does file I/O
  std::vector<CommunicatorLevel<T_Payload>> m_levels; //!< The vector of levels
  CommunicatorConcept
      m_root_communicator; //!< The root communicator for the hierarchy
  typename std::vector<CommunicatorLevel<T_Payload>>::iterator m_marker; //!<
};
} // namespace taru
