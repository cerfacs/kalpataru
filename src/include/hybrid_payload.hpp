/**
 *  @file hybrid_payload.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 14 May 2019
 *  @brief Documentation for hybrid_payload.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */
#pragma once

#include "communicator_taru.hpp"
#include "mesh_common.hpp"
#include "mesh_dual_graph.hpp"
#include "partitioner_scotch.h"
#include "partitioner_metis.h"
#include "partition_method.hpp"
#include <list>

namespace taru {

/**
 * @brief Payload data for RCB and RIB partitioning
 *
 */
class HybridPayload {

public:
  partition_method m_partition_method; // !<
  int m_dim = 3;                       //!< 3D by default
  std::vector<Point3d> coordinates;    //!< The element centroids
  std::vector<id_size_t> global_ids;   //!< The global index of the elements
  std::vector<ElementConnectivity>
      connectivity;           //!< The element node connectivity
  std::vector<float> weights; //!< The weights associated with the element
  bool disable_adjwgt = false;

  /**
   *
   * @return
   */
  int GetDimension() const { return m_dim; }

  /**
   *
   * @param dim
   */
  void SetDimension(const int dim) { m_dim = dim; }

  /**
   *
   * @param name
   */
  void SetPartitionMethod(const partition_method p) { m_partition_method = p; }

  /**
   *
   * @return
   */
  const partition_method GetPartitionMethod() { return m_partition_method; }

  /**
   *
   * @return
   */
  double GetMemUsage() {
    uint64_t mem_used = 0;
    mem_used += coordinates.size() * sizeof(Point3d);
    mem_used += global_ids.size() * sizeof(id_size_t);
    mem_used += connectivity.size() * sizeof(ElementConnectivity);
    mem_used += weights.size() * sizeof(float);
    return double(mem_used) / 1048576.0;
  }

  /**
   * @brief Get the local element data for graph partitioning
   *
   * @param eptr
   * @param eind
   * @return
   */
  template <typename T_Int>
  my_size_t GetLocalElementGraph(std::vector<T_Int> &eptr,
                                 std::vector<T_Int> &eind) {
    std::map<id_size_t, T_Int> unique_map;
    eptr.clear();
    eptr.reserve(connectivity.size() + 1);
    eptr.push_back(0);
    id_size_t unique_counter = 0;
    for (const auto &item : connectivity) {
      int node_count = 0;
      for (const auto &node : item.nodes) {
        if (node > 0) {
          node_count++;
          if (unique_map.find(node) == std::end(unique_map)) {
            unique_map[node] = unique_counter;
            unique_counter++;
          } // unique node in the element
        } // non-zero node index
      } // All nodes of an element
      eptr.push_back(node_count + eptr.back());
    }
    eind.clear();
    eind.reserve(eptr.back());
    for (const auto &item : connectivity)
      for (const auto &node : item.nodes)
        if (node > 0)
          eind.push_back(unique_map[node]); // Note C indexing since
                                            // unique_counter starts from 0
    return unique_map.size();
  }

  /**
   * @brief Get the element data for graph partitioning in global numbering
   * @param xadj
   * @param adjncy
   * @return
   */
  id_size_t GetElementParts(CommunicatorConcept &comm,
                            std::vector<rank_t> &parts) {
    id_size_t edgecut = 0;
    int partition_return;

    if (comm.IsValid()) {
      if (comm.Rank() == 0)
        std::cout << "Message: Starting Dual-graph construction\n";

      id_size_t ndims = GetDimension();
      id_size_t ncon = 1;
      id_size_t nparts = comm.size();
      id_size_t options[3] = {1, 0, 1};
      id_size_t numflag = 0; // C-style
      id_size_t wgtflag = 3; /* 0 = no weights
                                1 = weights on edges only
                                2 = weights on vertices only
                                3 = weights on both vertices and edges */
      std::vector<id_size_t> xadj, adjncy, vtxdist, adjwgt;
      std::vector<id_size_t> vtxwgt(connectivity.size());
      std::vector<id_size_t> part(connectivity.size());
      double ubvec = 1.01;
      std::vector<double> tpwgts;
      std::vector<id_size_t> tpwgts_integer;

      auto xyz = reinterpret_cast<double *>(&coordinates[0]);

      // Warning: Float to integer conversion is exact
      for (std::size_t i = 0; i < connectivity.size(); ++i)
        vtxwgt[i] = weights[i];

      // Construct the dual graph using correct weights
      MakeDualGraphWgt(connectivity,
                       comm,
                       xadj,
                       adjncy,
                       vtxdist,
                       vtxwgt,
                       adjwgt,
                       disable_adjwgt,
                       (GetDimension() == 2));

      if (!disable_adjwgt)
        std::fill(vtxwgt.begin(), vtxwgt.end(), id_size_t(1));

      if (comm.Rank() == 0)
        std::cout << "Message: Starting parallel "
                  << get_partition_name(m_partition_method)
                  << " partitioning of " << GetDimension() << "D dual graph\n";

      switch (m_partition_method) {
      case partition_method::Scotch:
        tpwgts_integer.resize(nparts, 1);
        partition_return = partition_ptscotch(vtxdist.data(),
                                              xadj.data(),
                                              adjncy.data(),
                                              vtxwgt.data(),
                                              adjwgt.data(),
                                              &wgtflag,
                                              &numflag,
                                              &nparts,
                                              tpwgts_integer.data(),
                                              &edgecut,
                                              part.data(),
                                              &(comm.RawHandle()));
        break;

      case partition_method::METIS:
        tpwgts.resize(nparts, 1.0 / nparts);
        partition_return = partition_metis(vtxdist.data(),
                                           xadj.data(),
                                           adjncy.data(),
                                           vtxwgt.data(),
                                           adjwgt.data(),
                                           &wgtflag,
                                           &numflag,
                                           &ndims,
                                           xyz,
                                           &ncon,
                                           &nparts,
                                           tpwgts.data(),
                                           &ubvec,
                                           options,
                                           &edgecut,
                                           part.data(),
                                           &(comm.RawHandle()));
        break;

      case partition_method::KaHIP:
        throw std::runtime_error("KaHIP or METIS partitioner not implemented");

      default:
        throw std::runtime_error(
            "Cannot select geometric partitioner as graph partitioner method");
        break;
      }

      parts.clear();
      parts.shrink_to_fit();
      std::copy(part.begin(), part.end(), std::back_inserter(parts));
      if (partition_return != 1) {
        std::cout << "Error in " << get_partition_name(m_partition_method)
                  << "partitioner\n";
        comm.Abort();
      }
    }
    return edgecut;
  }

  /**
   * @brief Get the Element Parts using Serial graph partitioner
   *
   * @param comm
   * @param edge_cuts
   * @return std::vector<id_size_t>
   */
  std::vector<id_size_t> GetElementPartsSerial(CommunicatorConcept &comm,
                                               int &edge_cuts) {
    std::vector<id_size_t> eptr, eind;
    auto nnodes = GetLocalElementGraph(eptr, eind);
    std::vector<id_size_t> epart(eptr.size() - 1);
    std::vector<id_size_t> npart(nnodes);
    id_size_t ne = eptr.size() - 1;
    id_size_t nn = nnodes;
    id_size_t nparts = comm.size();
    id_size_t objval;
    id_size_t ncommon = GetDimension();
    std::vector<id_size_t> vwgt(weights.size());
    std::vector<id_size_t> options(50);
    for (std::size_t iv = 0; iv < weights.size(); ++iv)
      vwgt[iv] = weights[iv];

    if (comm.Rank() == 0)
      std::cout << "Message: Starting serial "
                << get_partition_name(m_partition_method) << " partitioning of "
                << GetDimension() << "D dual graph\n";

    switch (m_partition_method) {
    case partition_method::METIS: {
#ifdef TARU_USE_METIS
      METIS_SetDefaultOptions(options.data());
#endif
      edge_cuts = partition_metis_serial(&ne,
                                         &nn,
                                         eptr.data(),
                                         eind.data(),
                                         vwgt.data(),
                                         nullptr,
                                         &ncommon,
                                         &nparts,
                                         nullptr,
                                         options.data(),
                                         &objval,
                                         epart.data(),
                                         npart.data());
    } break;

    default:
      throw std::runtime_error(
          "Serial partitioner other than METIS not implemented");
      break;
    }
#ifdef TARU_USE_METIS
    if (edge_cuts != METIS_OK)
      throw std::runtime_error("Serial Metis returned !METIS_OK !");
#endif
    npart.clear();
    npart.shrink_to_fit();
    vwgt.clear();
    vwgt.shrink_to_fit();
    eind.clear();
    eind.shrink_to_fit();
    eptr.clear();
    eptr.shrink_to_fit();
    return epart;
  }

  /**
   * @brief Get the number of objects in the payload
   * @param data
   * @param error_code
   * @return
   */
  static int GetNumObjects(void *data, int *error_code) {
    HybridPayload &payload = *(reinterpret_cast<HybridPayload *>(data));
    *error_code = ZOLTAN_OK;
    return payload.global_ids.size();
  }

  /**
   * @brief Get the list of objects from the object pointer (Zoltan callback)
   * @param data
   * @param num_gid_entries
   * @param num_lid_entries
   * @param global_ids
   * @param local_ids
   * @param wgt_dim
   * @param object_weights
   * @param error_code
   */
  static void GetObjectLists(void *data,
                             int num_gid_entries,
                             int num_lid_entries,
                             ZOLTAN_ID_PTR global_ids,
                             ZOLTAN_ID_PTR local_ids,
                             int wgt_dim,
                             float *object_weights,
                             int *error_code) {
    HybridPayload &payload = *(reinterpret_cast<HybridPayload *>(data));
    for (unsigned i = 0; i < payload.global_ids.size(); ++i) {
      global_ids[i] = payload.global_ids[i];
      local_ids[i] = i;
      object_weights[i] = payload.weights[i];
    }
    *error_code = ZOLTAN_OK;
  }

  /**
   * @brief Geometry dimension (Zoltan callback)
   * @param data
   * @param error_code
   * @return
   */
  static int GetGeometryDimension(void *data, int *error_code) {
    // HybridPayload &payload = *(reinterpret_cast<HybridPayload *>(data));
    return 3;
  }

  /**
   *
   * @param data
   * @param num_gid_entries
   * @param num_lid_entries
   * @param num_obj
   * @param global_ids
   * @param local_ids
   * @param num_dim
   * @param geom_vec
   * @param error_code
   */
  static void GetCoordinateVector(void *data,
                                  int num_gid_entries,
                                  int num_lid_entries,
                                  int num_obj,
                                  ZOLTAN_ID_PTR global_ids,
                                  ZOLTAN_ID_PTR local_ids,
                                  int num_dim,
                                  double *geom_vec,
                                  int *error_code) {
    HybridPayload &payload = *(reinterpret_cast<HybridPayload *>(data));
    auto *coordinate = reinterpret_cast<double *>(&(payload.coordinates[0].x));
    memcpy(geom_vec, coordinate, 3 * sizeof(double) * num_obj);
    *error_code = ZOLTAN_OK;
  }

  /**
   *
   * @param data
   * @param gidSize
   * @param lidSize
   * @param num_ids
   * @param globalID
   * @param localID
   * @param sizes
   * @param error_code
   */
  static void GetMigrationMessageSizes(void *data,
                                       int gidSize,
                                       int lidSize,
                                       int num_ids,
                                       ZOLTAN_ID_PTR globalID,
                                       ZOLTAN_ID_PTR localID,
                                       int *sizes,
                                       int *error_code) {
    for (int i = 0; i < num_ids; i++)
      sizes[i] = sizeof(Point3d) + sizeof(ElementConnectivity) + sizeof(float);
    *error_code = ZOLTAN_OK;
  }

  /**
   *
   * @param data
   * @param gidSize
   * @param lidSize
   * @param num_ids
   * @param globalIDs
   * @param localIDs
   * @param dests
   * @param sizes
   * @param idx
   * @param buf
   * @param error_code
   */
  static void PackObjectMessages(void *data,
                                 int gidSize,
                                 int lidSize,
                                 int num_ids,
                                 ZOLTAN_ID_PTR globalIDs,
                                 ZOLTAN_ID_PTR localIDs,
                                 int *dests,
                                 int *sizes,
                                 int *idx,
                                 char *buf,
                                 int *error_code) {
    ZOLTAN_ID_TYPE lid;
    HybridPayload &payload = *(reinterpret_cast<HybridPayload *>(data));
    /* For each exported vertex, write its neighbor
     * global IDs to the supplied buffer */
    for (int i = 0; i < num_ids; i++) {
      lid = localIDs[i];
      /**
       *  Element centroid x,y,z
       */
      auto *centroid_buf = reinterpret_cast<Point3d *>(buf + idx[i]);
      *centroid_buf = payload.coordinates[lid];
      /**
       *  Write 8 nodes of element
       */
      auto *element_buf =
          reinterpret_cast<ElementConnectivity *>(centroid_buf + 1);
      *element_buf = payload.connectivity[lid];
      /**
       *  Pack float weights
       */
      auto *weight_buf = reinterpret_cast<float *>(element_buf + 1);
      *weight_buf = payload.weights[lid];
    }
    *error_code = ZOLTAN_OK;
  }

  /**
   *
   * @param data
   * @param gidSize
   * @param lidSize
   * @param numImport
   * @param importGlobalID
   * @param importLocalID
   * @param importProc
   * @param importPart
   * @param numExport
   * @param exportGlobalID
   * @param exportLocalID
   * @param exportProc
   * @param exportPart
   * @param error_code
   */
  static void MidMigrationManipulation(void *data,
                                       int gidSize,
                                       int lidSize,
                                       int numImport,
                                       ZOLTAN_ID_PTR importGlobalID,
                                       ZOLTAN_ID_PTR importLocalID,
                                       int *importProc,
                                       int *importPart,
                                       int numExport,
                                       ZOLTAN_ID_PTR exportGlobalID,
                                       ZOLTAN_ID_PTR exportLocalID,
                                       int *exportProc,
                                       int *exportPart,
                                       int *error_code) {
    HybridPayload &payload = *(reinterpret_cast<HybridPayload *>(data));
    std::vector<bool> retain_item(payload.global_ids.size(), true);

    for (int i = 0; i < numExport; i++)
      retain_item[exportLocalID[i]] = false;

    std::size_t next_item_index = 0;
    for (std::size_t current_item_index = 0;
         current_item_index < payload.global_ids.size();
         current_item_index++) {
      /* Check if item has to be retained */
      if (retain_item[current_item_index]) {
        /* only move items if current item index is past the next item index
         * pointer */
        if (current_item_index > next_item_index) {
          /* shift element GIDs to next_item_index */
          payload.global_ids[next_item_index] =
              payload.global_ids[current_item_index];
          /* Copy element nodes to next_item_index */
          payload.coordinates[next_item_index] =
              payload.coordinates[current_item_index];
          payload.connectivity[next_item_index] =
              payload.connectivity[current_item_index];
          payload.weights[next_item_index] =
              payload.weights[current_item_index];
        }
        next_item_index++;
      }
    }
    /**
     * Resizing these will not reallocate this vector so we can recover the
     * space during migration unpack but reduce the coding effort
     */
    payload.global_ids.resize(next_item_index);
    payload.coordinates.resize(next_item_index);
    payload.connectivity.resize(next_item_index);
    payload.weights.resize(next_item_index);
    *error_code = ZOLTAN_OK;
  }

  /**
   *
   * @param data
   * @param gidSize
   * @param num_ids
   * @param globalIDs
   * @param size
   * @param idx
   * @param buf
   * @param error_code
   */
  static void UnpackObjectMessages(void *data,
                                   int gidSize,
                                   int num_ids,
                                   ZOLTAN_ID_PTR globalIDs,
                                   int *size,
                                   int *idx,
                                   char *buf,
                                   int *error_code) {
    int num_vertex, next_vertex;
    HybridPayload &payload = *(reinterpret_cast<HybridPayload *>(data));

    /* Add incoming vertices to local data */
    next_vertex = payload.global_ids.size();
    num_vertex = next_vertex + num_ids;

    /* Resize to accommodate imported data */
    payload.global_ids.resize(num_vertex);
    payload.coordinates.resize(num_vertex);
    payload.connectivity.resize(num_vertex);
    payload.weights.resize(num_vertex);

    /* Copy buf contents to local data */
    for (int i = 0; i < num_ids; i++) {
      payload.global_ids[next_vertex] = globalIDs[i];
      auto *point_ptr = reinterpret_cast<Point3d *>(buf + idx[i]);
      payload.coordinates[next_vertex] = *point_ptr;
      auto *element_ptr =
          reinterpret_cast<ElementConnectivity *>(point_ptr + 1);
      payload.connectivity[next_vertex] = *element_ptr;
      auto *weight_ptr = reinterpret_cast<float *>(element_ptr + 1);
      payload.weights[next_vertex] = *weight_ptr;
      next_vertex++;
    }
    *error_code = ZOLTAN_OK;
  }

  /**
   * @brief Register all Zoltan callback functions
   * @param zz
   * @return
   */
  bool RegisterCallback(Zoltan_Struct *zz) {
    /* Query functions, to provide geometry to Zoltan */
    Zoltan_Set_Num_Obj_Fn(zz, HybridPayload::GetNumObjects, this);
    Zoltan_Set_Obj_List_Fn(zz, HybridPayload::GetObjectLists, this);
    Zoltan_Set_Num_Geom_Fn(zz, HybridPayload::GetGeometryDimension, this);
    Zoltan_Set_Geom_Multi_Fn(zz, HybridPayload::GetCoordinateVector, this);

    /* Migration functions */
    Zoltan_Set_Obj_Size_Multi_Fn(
        zz, HybridPayload::GetMigrationMessageSizes, this);
    Zoltan_Set_Pack_Obj_Multi_Fn(zz, HybridPayload::PackObjectMessages, this);
    Zoltan_Set_Unpack_Obj_Multi_Fn(
        zz, HybridPayload::UnpackObjectMessages, this);
    Zoltan_Set_Mid_Migrate_PP_Fn(
        zz, HybridPayload::MidMigrationManipulation, this);
    return true;
  }

  /**
   *  @brief Clear all payload data
   */
  void clear() {
    global_ids.clear();
    coordinates.clear();
    connectivity.clear();
    weights.clear();
  }

  /**
   *  @brief Clear all payload data
   */
  void shrink_to_fit() {
    global_ids.shrink_to_fit();
    coordinates.shrink_to_fit();
    connectivity.shrink_to_fit();
    weights.shrink_to_fit();
  }
};

/**
 * @brief Bootstrap functor for HybridPayload type
 * @tparam T_Mesh
 */
template <typename T_Mesh> struct BootstrapHybridFunctor {

  /**
   * @brief This is the constructor to the bootstrap functor for the Hybrid
   * payload
   * @param input_dht
   */
  explicit BootstrapHybridFunctor(T_Mesh &mesh)
      : m_mesh(mesh) {}

  /**
   *  @brief Default constructor
   *  @param input_dht
   */
  BootstrapHybridFunctor()
      : m_mesh(nullptr) {}

  /**
   * @brief Apply the bootstrap functor
   *
   * @param input_tree
   * @param bootstrap_level
   */
  void operator()(TreeCommunicator<HybridPayload> &input_tree,
                  int bootstrap_level = 0) {
    if (input_tree.IsValidTreeRoot()) {
      Time(global_timer::TIME_BOOTSTRAP, global_timer::C_TimerStart);
      auto &current_level = input_tree[bootstrap_level];
      auto &current_payload = current_level.Payload();
      auto &current_branch = current_level.Branch();

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout << "Message: Bootstrapping "
                    << "Hybrid Payload at level " << bootstrap_level << "\n";
      /**
       *  Create keys and data at the root level and aggregate
       */
      id_size_t payload_size = this->m_mesh.CentroidHashTable().size();
      current_payload.global_ids.resize(payload_size);
      current_payload.coordinates.resize(payload_size);
      current_payload.connectivity.resize(payload_size);
      current_payload.weights.resize(payload_size);

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout << "Message: Bootstrap payload resize complete.\n";

      /**
       *  Generate local key distribution
       */
      for (id_size_t i = 0; i < payload_size; ++i)
        current_payload.global_ids[i] =
            i + this->m_mesh.CentroidHashTable().begin();
      /**
       *  Get the key data for element centroids
       */
      this->m_mesh.CentroidHashTable().GetDataFromKeys(
          current_payload.global_ids, current_payload.coordinates);

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout << "Message: Get centroid of bootstrap data complete.\n";

      /**
       *  Get the key data for element connectivity
       */
      this->m_mesh.ElementConnectivityHashTable().GetDataFromKeys(
          current_payload.global_ids, current_payload.connectivity);

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout
              << "Message: Get connectivity of bootstrap data complete.\n";

      /**
       *  Get the key data for element weights
       *  if present in the mesh otherwise default to 1.0
       */
      if (this->m_mesh.ElementWeightsHashTable().GlobalSize() > 0) {
        if (current_branch.IsValid())
          if (current_branch.IsMaster())
            std::cout << "Message: Using graph vertex weights from dist hash "
                         "table.\n";
        this->m_mesh.ElementWeightsHashTable().GetDataFromKeys(
            current_payload.global_ids, current_payload.weights);
      } else {
        if (current_branch.IsValid())
          if (current_branch.IsMaster())
            std::cout << "Message: Setting default uniform graph vertex weight "
                         "= 1.\n";
        for (auto &item : current_payload.weights)
          item = 1.0;
      }

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout << "Message: Start aggregation of bootstrap data.\n";

      /**
       *  Aggregate data to top level
       */
      input_tree.Aggregate(bootstrap_level, current_payload.coordinates);
      input_tree.Aggregate(bootstrap_level, current_payload.connectivity);
      input_tree.Aggregate(bootstrap_level, current_payload.global_ids);
      input_tree.Aggregate(bootstrap_level, current_payload.weights);

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout << "Message: Completed aggregation of bootstrap data.\n";

      Time(global_timer::TIME_BOOTSTRAP, global_timer::C_TimerStop);
    }
  }

  static int FindConnectedComponents(const HybridPayload &p,
                                     CommunicatorConcept &comm,
                                     std::vector<int> &colour,
                                     std::vector<int> &colour_nranks,
                                     std::vector<int> &colour_ranks) {
    int num_connected_components = 0;
    if (comm.IsMaster()) {
      // Note that the dual graph is locally indexed and the
      // halos will also be local with rank id in dual_ranks
      // So they are int and not id_size_t to save memory and note
      // that dual_xadj.size() != p.connectivity.size() because this
      // includes the halos from the neighbour processor
      std::vector<int> dual_xadj, dual_adjncy;
      std::vector<rank_t> dual_ranks;
      //        ConstructDualGraph(p, comm, dual_xadj, dual_adjncy);
      std::vector<bool> visited(dual_xadj.size() - 1, false);
      colour.resize(dual_xadj.size() - 1);
      std::fill(colour.begin(), colour.end(), -1);
      for (;;) {
        auto start_vertex_it = std::find(visited.begin(), visited.end(), false);
        // If all vertices have been visited then break out of loop
        if (start_vertex_it == std::end(visited))
          break;
        const id_size_t start_vertex =
            std::distance(visited.begin(), start_vertex_it) - 1;
        DoBFS(num_connected_components,
              start_vertex,
              dual_xadj,
              dual_adjncy,
              visited,
              colour);
        num_connected_components++;
        assert(num_connected_components > 100);
      }
      FormConnectedGraph(colour, colour_nranks, colour_ranks);
    }
    return num_connected_components;
  }

  static void FormConnectedGraph(std::vector<int> &colour,
                                 std::vector<int> &colour_nranks,
                                 std::vector<int> &colour_ranks) {}

  static void ConstructDualGraph(const HybridPayload &p,
                                 CommunicatorConcept &comm,
                                 std::vector<id_size_t> &d_xadj,
                                 std::vector<id_size_t> &d_adjncy,
                                 std::vector<id_size_t> &d_vtxdist) {
    if (comm.IsValid())
      if (comm.Rank() == 0)
        std::cout << "Message: Starting Dual-graph construction\n";
    MakeDualGraph(p.connectivity, comm, d_xadj, d_adjncy, d_vtxdist);
  }

  static void DoBFS(const int cur_colour,
                    const id_size_t start_vertex,
                    const std::vector<int> &d_xadj,
                    const std::vector<int> &d_adjncy,
                    std::vector<bool> &visited,
                    std::vector<int> &colour) {
    // Create a queue for BFS
    std::list<id_size_t> queue;

    // Mark the current node as visited and enqueue it
    visited[start_vertex] = true;
    queue.push_back(start_vertex);

    // Visit till all nodes are exhausted
    while (!queue.empty()) {
      // Dequeue a vertex from queue and print it
      const auto current = queue.front();
      queue.pop_front();

      // Get all adjacent vertices of the dequeued
      // vertex s. If a adjacent has not been visited,
      // then mark it visited and enqueue it
      for (auto irow = d_xadj[current]; irow < d_xadj[current] + 1; ++irow) {
        const auto i = d_adjncy[irow];
        if (!visited[i]) {
          visited[i] = true;
          colour[i] = cur_colour;
          queue.push_back(i);
        }
      }
    }
  }

private:
  T_Mesh &m_mesh;
};

/**
 * @brief Empty bootstrap functor (useful to run the
 *        paritioner in re-balancing mode or when boot-
 *        strapping is performed outside
 * @tparam T_Mesh
 */
template <typename T_Mesh> struct EmptyBootstrapFunctior {

  /**
   * @brief This is the constructor to the bootstrap functor for the Hybrid
   * payload
   * @param input_dht
   */
  explicit EmptyBootstrapFunctior(T_Mesh &mesh)
      : m_mesh(mesh) {}

  /**
   *  @brief Default constructor
   *  @param input_dht
   */
  EmptyBootstrapFunctior()
      : m_mesh(nullptr) {}

  /**
   * @brief Empty apply
   *
   * @param input_tree
   * @param bootstrap_level
   */
  void operator()(TreeCommunicator<HybridPayload> &input_tree,
                  int bootstrap_level = 0) {
    if (input_tree.IsValidTreeRoot()) {
      Time(global_timer::TIME_BOOTSTRAP, global_timer::C_TimerStart);
      auto &current_level = input_tree[bootstrap_level];
      auto &current_payload = current_level.Payload();
      auto &current_branch = current_level.Branch();

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout << "Message: Empty bootstrapping "
                    << "Payload at level " << bootstrap_level << "\n";

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout << "Message: Start aggregation of bootstrap data.\n";

      /**
       *  Aggregate data to top level
       */
      input_tree.Aggregate(bootstrap_level, current_payload.coordinates);
      input_tree.Aggregate(bootstrap_level, current_payload.connectivity);
      input_tree.Aggregate(bootstrap_level, current_payload.global_ids);
      input_tree.Aggregate(bootstrap_level, current_payload.weights);

      if (current_branch.IsValid())
        if (current_branch.IsMaster())
          std::cout << "Message: Completed aggregation of bootstrap data.\n";

      Time(global_timer::TIME_BOOTSTRAP, global_timer::C_TimerStop);
    }
  }

private:
  T_Mesh &m_mesh;
};

} // namespace taru
