/**
 *  @file communicator_mpi.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Documentation for communicator_mpi.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <mpi.h>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#ifdef HAVE_HWLOC
#include <hwloc.h>
#endif

/**
 * @brief Get the processor rank given the global index i,
 *        threshold, quotient, and remainder
 *
 * @param i
 * @param threshold
 * @param quotient
 * @param remainder
 */
#define PROC_ID(i, threshold, quotient, remainder)                             \
  (i > threshold) ? (i - 1 - threshold) / quotient + remainder                 \
                  : (i - 1) / (quotient + 1)

/**
 * @brief Testing routine to check the working of @ref PROC_ID macro
 * @tparam T
 * @param total
 * @param nparts
 */
template <typename T> static void TestDistribution(T total, T nparts) {
  int64_t quotient = total / nparts;
  int64_t remainder = total % nparts;
  int64_t threshold = remainder * (quotient + 1);
  std::cout << "threshold : " << threshold << "\n";
  std::cout << "remainder : " << remainder << "\n";
  std::cout << "quotient : " << quotient << "\n";
  for (T i = 1; i <= total; ++i) {
    auto my_proc_id = PROC_ID(int64_t(i), threshold, quotient, remainder);
    std::cout << i << " : " << my_proc_id << "\n";
  }
}

/**
 * @brief Returns the name of the
 *        variable x as a C++ string
 **/
#define nameof(x) std::string(#x)

/**
 * @brief Wrapper call for adding a variable
 *        to the GatherMaster pattern
 **/
#define AddGatherMasterTuple(x, y) __impl_AddGatherMasterTuple(x, nameof(y), y)

/**
 * @brief Wrapper call for resizeing a variable
 *        in the GatherMasterMap
 **/
#define ResizeFunction(m, x, n) std::get<RESIZE_FN>(m[nameof(x)])(n)

/**
 * @brief Wrapper call for getting the size in bytes
 *        of variable in the GatherMasterMap
 **/
#define SizeInBytesFunction(m, x) std::get<SIZE_IN_BYTES_FN>(m[nameof(x)])()

/**
 * @brief Wrapper call for getting the size in bytes
 *        of variable in the GatherMasterMap
 **/
#define SizeFunction(m, x) std::get<SIZE_FN>(m[nameof(x)])()

/**
 * @brief Wrapper call for getting the size in bytes
 *        of variable in the GatherMasterMap
 **/
#define RawAccessFunction(m, x, loc) std::get<ACCESS_FN>(m[nameof(x)])(loc)

/**
 * @namespace taru
 * @brief @ref treepart namespace which holds all new functions
 *
 */

namespace taru {

/**
 * @brief Tuples to store the lambdas of the
 *        DoGatherMaster pattern data variables
 */
typedef std::tuple<std::function<void(const std::size_t)>,
                   std::function<std::size_t()>,
                   std::function<std::size_t()>,
                   std::function<void *(const std::size_t)>>
    GatherMasterTuple;

/**
 * @brief Enums to access lambdas in the GatherMaster tuple
 */
enum GatherMasterEnum {
  RESIZE_FN =
      0,   //!< Callback function takes a size in bytes and resize the container
  SIZE_FN, //!< Callback function returns the number of elements in the
           //!< container
  SIZE_IN_BYTES_FN, //!< Callback function returns the size of the container in
                    //!< bytes
  ACCESS_FN //!< Callback function returns pointer to the requested index item
};

/**
 * @brief Hash map with key->variable_name and data->GatherMasterTuple
 */
typedef std::unordered_map<std::string, GatherMasterTuple> GatherMasterTupleMap;

/**
 * @brief Overloaded version of GatherMaster pattern
 *        lambda tuples for (is_pod<T>::value == true)
 *        C++ vector<T>
 */
template <typename T>
void __impl_AddGatherMasterTuple(GatherMasterTupleMap &map,
                                 const std::string name,
                                 std::vector<T> &v) {
  static_assert(std::is_pod<T>::value == true,
                "Type is not POD cannot do Tuple");
  auto resize_fn = [&v](const std::size_t sz) { v.resize(sz / sizeof(T)); };
  auto size_fn = [&v]() { return v.size(); };
  auto size_in_bytes_fn = [&v]() { return v.size() * sizeof(T); };
  auto access_fn = [&v](const std::size_t loc) {
    return reinterpret_cast<void *>(&v[loc]);
  };
  map[name] = std::make_tuple(resize_fn, size_fn, size_in_bytes_fn, access_fn);
}

/**
 * @brief Overloaded version of GatherMaster pattern
 *        lambda tuples for fixed size static arrays
 *        v[T_Size]
 * @details See j_random_hacker's answer in
 *          (https://stackoverflow.com/questions/453099/size-of-static-array#453131)
 */
template <typename T, std::size_t T_Size>
void __impl_AddGatherMasterTuple(GatherMasterTupleMap &map,
                                 const std::string name,
                                 T (&v)[T_Size]) {
  auto resize_fn = [&v](const std::size_t sz) { /* error */ };
  auto size_fn = [&v]() { return T_Size; };
  auto size_in_bytes_fn = [&v]() { return T_Size * sizeof(T); };
  auto access_fn = [&v](const std::size_t loc) {
    return reinterpret_cast<void *>(v + loc);
  };
  map[name] = std::make_tuple(resize_fn, size_fn, size_in_bytes_fn, access_fn);
}

typedef int rank_t; //!< Define rank type

/**
 * @brief Generic template function to get the MPI types
 *        specialised for each type
 *
 * @tparam T
 * @return
 */
template <typename T> static inline MPI_Datatype GetMpiType();

/**
 * @brief Specialised template function for int
 *
 * @return
 */
template <> inline MPI_Datatype GetMpiType<int>() { return MPI_INTEGER; }

/**
 * @brief Specialised template function for long
 *
 * @return
 */
template <> inline MPI_Datatype GetMpiType<long>() { return MPI_LONG; }

/**
 * @brief Specialised template function for long long
 *
 * @return
 */
template <> MPI_Datatype GetMpiType<long long>() { return MPI_LONG_LONG; }

/**
 * @brief Specialised template function for unsigned int
 *
 * @return
 */
template <> inline MPI_Datatype GetMpiType<unsigned int>() {
  return MPI_INTEGER;
}

/**
 * @brief Specialised template function for unsigned long
 *
 * @return
 */
template <> inline MPI_Datatype GetMpiType<unsigned long>() { return MPI_LONG; }

/**
 * @brief Specialised template function for unsigned long long
 *
 * @return
 */
template <> inline MPI_Datatype GetMpiType<unsigned long long>() {
  return MPI_UNSIGNED_LONG_LONG;
}

template <> inline MPI_Datatype GetMpiType<float>() { return MPI_FLOAT; }

template <> inline MPI_Datatype GetMpiType<double>() { return MPI_DOUBLE; }

template <> inline MPI_Datatype GetMpiType<long double>() {
  return MPI_LONG_DOUBLE;
}

template <> inline MPI_Datatype GetMpiType<char>() { return MPI_CHAR; }

template <> inline MPI_Datatype GetMpiType<unsigned char>() {
  return MPI_UNSIGNED_CHAR;
}

template <> inline MPI_Datatype GetMpiType<signed char>() {
  return MPI_SIGNED_CHAR;
}

/**
 * @brief MPI communicator concept
 *
 * @todo Make the m_comm object a shared pointer so that a communicator is not
 * copied making too many copies at runtime
 */
class CommunicatorConcept {

  typedef std::pair<void *, std::size_t> DataSizePair;
  typedef std::unordered_map<const char *, DataSizePair> TagDataMap;

public:
  /**
   * @brief Construct from an MPI communicator
   *
   * @param in_comm
   * @param name
   */
  CommunicatorConcept(MPI_Comm in_comm, std::string name)
      : m_name(std::move(name)) {
    MPI_Comm_dup(in_comm, &m_comm);
  }

  /**
   * @brief Accepts empty constructor
   *
   */
  CommunicatorConcept()
      : m_comm(MPI_COMM_NULL),
        m_name("") {}

  /**
   * @brief Accepts empty constructor with a name
   *
   * @param str
   */
  explicit CommunicatorConcept(std::string str)
      : m_comm(MPI_COMM_NULL),
        m_name(std::move(str)) {}

  /**
   * @brief Copy constructor
   *
   * @param c
   */
  CommunicatorConcept(const CommunicatorConcept &c) { Copy(c); }

  /**
   * @brief Check if null communicator
   *
   * @return
   */
  bool IsValid() const { return (m_comm != MPI_COMM_NULL); }

  /**
   * @brief Check if current rank is master in communicator
   *
   * @return
   */
  bool IsMaster() const { return (Rank() == 0); }

  /**
   * @brief Return communicator size
   *
   * @return
   */
  rank_t size() const {
    rank_t num_procs;
    MPI_Comm_size(m_comm, &num_procs);
    return num_procs;
  }

  /**
   * @brief Returns the name of the communicator
   *
   * @return
   * @return
   */
  const std::string &Name() const { return m_name; }

  /**
   * @brief Return the processor rank in the communicator
   *
   * @return
   */
  rank_t Rank() const {
    rank_t my_rank;
    MPI_Comm_rank(m_comm, &my_rank);
    return my_rank;
  }

  /**
   * @brief Return the master processor rank in the communicator
   *
   * @return
   */
  static rank_t MasterRank() { return 0; }

  /**
   * @brief Split the communicator and copy new communicator to new_comm
   *
   * @param colour
   * @param new_comm
   */
  void Split(rank_t colour, CommunicatorConcept &new_comm) {
    MPI_Comm_split(m_comm, colour, Rank(), &(new_comm.RawHandle()));
  }

  /**
   * @brief Select list of ranks from this->comm() and copy new communicator to
   * new_comm
   *
   * @param ranks
   * @param new_comm
   */
  void Select(std::vector<rank_t> &ranks, CommunicatorConcept &new_comm) {
    MPI_Group new_group, my_group;
    MPI_Comm_group(m_comm, &my_group);
    MPI_Group_incl(my_group, ranks.size(), &(ranks[0]), &new_group);
    MPI_Comm_create_group(m_comm, new_group, 0, &(new_comm.RawHandle()));
    MPI_Group_free(&my_group);
    MPI_Group_free(&new_group);
  }

  /**
   * @brief Copy constructor helper function
   *
   * @param c
   * @return
   */
  bool Copy(const CommunicatorConcept &c) {
    auto &const_c = const_cast<CommunicatorConcept &>(c);
    /* Duplicate communicator only if it is a valid communicator */
    if (const_c.IsValid()) {
      MPI_Comm_dup(const_c.RawHandle(), &m_comm);
      /* Already not a named communicator */
      if (m_name.empty())
        m_name = const_c.Name();
      return true;
    } else {
      m_name = const_c.Name();
      return false;
    }
  }

  /**
   * @brief Barrier function for communicator
   *
   */
  void Barrier() const { MPI_Barrier(m_comm); }

  /**
   * @brief Allow access to raw implementation of the communicator
   *
   * @return
   */
  MPI_Comm &RawHandle() { return m_comm; }

  /**
   * @brief Broadcast function from a master rank
   *
   * @tparam T_Data
   * @tparam T_Size
   * @param data
   * @param len
   */
  template <typename T_Data, typename T_Size>
  void BroadcastMaster(T_Data *data, T_Size len) const {
    MPI_Bcast(data, len, GetMpiType<T_Data>(), 0, m_comm);
  }

  /**
   * @brief Abort function in communicator
   *
   */
  void Abort() const {
    if (IsMaster())
      std::cerr << "Message: Aborting communicator due to fatal error!\n";
    Barrier();
    MPI_Abort(m_comm, -1);
  }

  /**
   * @brief Creates an assumed distribution from the given list of ranks
   *
   * @tparam T
   * @param global_size
   * @param local_begin
   * @param local_size
   */
  template <typename T>
  void
  RankDistribution(const T &global_size, T &local_begin, T &local_size) const {
    auto cur_rank = Rank();
    auto cur_size = size();
    DistributionFunction(
        cur_rank, cur_size, global_size, local_begin, local_size);
  }

  /**
   *
   * @tparam T
   * @tparam T_Vec
   * @param data
   * @return
   */
  template <typename T, typename T_Vec>
  std::array<T, 2> MinMaxMaster(const T_Vec &data) {
    std::array<T, 2> local, global;
    auto mm = std::minmax_element(std::begin(data), std::end(data));
    local[0] = *mm.first;
    local[1] = -*mm.second;
    MPI_Reduce(&local[0],
               &global[0],
               2,
               GetMpiType<T>(),
               MPI_MIN,
               MasterRank(),
               RawHandle());
    global[1] = -global[1];
    return global;
  }

  /**
   *
   * @tparam T
   * @tparam T_Vec
   * @param data
   * @return
   */
  template <typename T, typename T_Vec>
  std::array<T, 2> MinMaxGlobal(const T_Vec &data) {
    std::array<T, 2> local, global;
    auto mm = std::minmax_element(std::begin(data), std::end(data));
    local[0] = *mm.first;
    local[1] = -*mm.second;
    MPI_Allreduce(
        &local[0], &global[0], 2, GetMpiType<T>(), MPI_MIN, RawHandle());
    global[1] = -global[1];
    return global;
  }

  template <typename T_Key, typename T_Data>
  std::array<T_Data, 2>
  MinMaxMapDataGlobal(const std::map<T_Key, T_Data> &data) {
    std::array<T_Data, 2> local, global;
    auto mm = std::minmax_element(
        data.begin(),
        data.end(),
        [](const std::pair<T_Key, T_Data> &x,
           const std::pair<T_Key, T_Data> &y) { return x.second < y.second; });
    local[0] = mm.first->second;
    local[1] = -mm.second->second;
    MPI_Allreduce(
        &local[0], &global[0], 2, GetMpiType<T_Data>(), MPI_MIN, RawHandle());
    global[1] = -global[1];
    return global;
  }

  /**
   * @brief Assumed distribution function for a given global size
   * @tparam T
   * @param global_size
   * @param local_begin
   * @param local_size
   */
  template <typename T>
  static void DistributionFunction(const rank_t &rank,
                                   const rank_t &num_procs,
                                   const T &global_size,
                                   T &local_begin,
                                   T &local_size) {
    local_size = 0;
    local_begin = 0;
    for (T i = 0; i < global_size; i++) {
      rank_t proc_id = i % num_procs;
      if (proc_id == rank)
        local_size++;
      if (proc_id < rank)
        local_begin++;
    }
  }

  /**
   * @brief Cascade from master rank data vector using offset info
   * @tparam T
   * @tparam T_Int
   * @param data_to_cascade
   * @param offset
   */
  template <typename T, typename T_Int>
  void CascadeMasterToAll(std::vector<T> &data_to_cascade,
                          std::vector<T_Int> &offset) {
    // MPI handles
    std::vector<MPI_Request> send_request(size() - 1);
    std::vector<MPI_Status> send_status(size() - 1);
    MPI_Status receive_status;
    std::vector<rank_t> distribution_size;
    std::vector<rank_t> distribution_begin;
    const int mpi_tag = 0;

    if (IsValid()) {
      if (IsMaster()) {
        // Check for possible integer overflows in offset sizes
        for (const auto &item : offset)
          if (item > std::numeric_limits<rank_t>::max())
            throw std::runtime_error("Integer overflow detected in Cascade");
        std::copy(offset.begin(),
                  offset.end(),
                  std::back_inserter(distribution_begin));
        distribution_size.resize(size());
        /**
         *  Non-blocking send of data to slave ranks
         */
        for (rank_t i = 0; i < size(); ++i)
          distribution_size[i] =
              distribution_begin[i + 1] - distribution_begin[i];
        for (rank_t i = 1; i < size(); ++i) {
          std::size_t test_overflow = sizeof(T) * distribution_size[i];
          if (test_overflow >= std::numeric_limits<int>::max())
            Abort();
          MPI_Isend(&data_to_cascade[distribution_begin[i]],
                    sizeof(T) * distribution_size[i],
                    MPI_BYTE,
                    i,
                    mpi_tag,
                    RawHandle(),
                    &send_request[i - 1]);
        }
      }
      /**
       *  Slave ranks perform a blocking receive of
       *  incoming data (allocate and store using probes)
       */
      else {
        int size_in_bytes;
        MPI_Probe(MasterRank(), mpi_tag, RawHandle(), &receive_status);
        MPI_Get_count(&receive_status, MPI_BYTE, &size_in_bytes);
        data_to_cascade.resize(size_in_bytes / sizeof(T));
        MPI_Recv(&data_to_cascade[0],
                 size_in_bytes,
                 MPI_BYTE,
                 0,
                 0,
                 RawHandle(),
                 &receive_status);
      }
      /**
       *  Master waits for all ranks to finish receiving the data
       */
      if (IsMaster()) {
        MPI_Waitall(size() - 1, send_request.data(), send_status.data());
        assert(distribution_size[0] != 0);
        data_to_cascade.resize(distribution_size[0]);
        // Need to shrink to fit otherwise too much memory is left unused in the
        // master
        data_to_cascade.shrink_to_fit();
      }
    }
  }

  /**
   * @brief Cascade data from master to slaves using assumed
   *        distribution function
   * @tparam T
   * @param data_to_cascade
   */
  template <typename T>
  void CascadeMasterToAll(std::vector<T> &data_to_cascade) {
    rank_t my_comm_size = size();
    std::vector<std::size_t> data_len(my_comm_size);
    std::vector<std::size_t> offset(my_comm_size + 1);
    if (IsValid()) {
      if (IsMaster()) {
        for (rank_t i = 0; i < my_comm_size; ++i) {
          auto cur_size = my_comm_size;
          std::size_t global_size = data_to_cascade.size();
          DistributionFunction(
              i, cur_size, global_size, offset[i], data_len[i]);
        }
        offset[my_comm_size] =
            offset[my_comm_size - 1] + data_len[my_comm_size - 1];
      }
      CascadeMasterToAll(data_to_cascade, offset);
    }
  }

  /**
   * @brief Aggregate the data vector from slave into master
   *
   * @tparam T
   * @param data_to_agg
   */
  template <typename T> void AggregateMaster(std::vector<T> &data_to_agg) {
    MPI_Request send_request;
    MPI_Status send_status;
    MPI_Status receive_status;
    if (IsValid()) {
      /**
       *  Except master all ranks do non-blocking send
       */
      if (!IsMaster()) {
        std::size_t test_overflow = sizeof(T) * data_to_agg.size();
        if (test_overflow >= std::numeric_limits<int>::max())
          Abort();
        MPI_Isend(&data_to_agg[0],
                  sizeof(T) * data_to_agg.size(),
                  MPI_BYTE,
                  MasterRank(),
                  0,
                  RawHandle(),
                  &send_request);
      } else {
        for (rank_t receive_rank = 1; receive_rank < size(); ++receive_rank) {
          int size_in_bytes;
          auto old_size = data_to_agg.size();
          MPI_Probe(receive_rank, 0, RawHandle(), &receive_status);
          MPI_Get_count(&receive_status, MPI_BYTE, &size_in_bytes);
          data_to_agg.resize(old_size + size_in_bytes / sizeof(T));
          MPI_Recv(&data_to_agg[old_size],
                   size_in_bytes,
                   MPI_BYTE,
                   receive_rank,
                   0,
                   RawHandle(),
                   &receive_status);
        }
      }
      if (!IsMaster()) {
        MPI_Wait(&send_request, &send_status);
        data_to_agg.clear();
        data_to_agg.shrink_to_fit();
      }
    }
  }

  /**
   *
   * @tparam T
   * @param data
   * @param count
   */
  template <typename T> void AllReduceMax(T *data, int count) {
    MPI_Allreduce(
        MPI_IN_PLACE, data, count, GetMpiType<T>(), MPI_MAX, RawHandle());
  }

  /**
   *
   * @tparam T
   * @param data
   * @param count
   */
  template <typename T> void AllReduceMin(T *data, int count) {
    MPI_Allreduce(
        MPI_IN_PLACE, data, count, GetMpiType<T>(), MPI_MIN, RawHandle());
  }

  /**
   *
   * @tparam T
   * @param data
   * @param count
   */
  template <typename T, typename T_Int>
  void AllReduceSum(T *data, const T_Int count) {
    MPI_Allreduce(
        MPI_IN_PLACE, data, count, GetMpiType<T>(), MPI_SUM, RawHandle());
  }

  /**
   * @brief Free communicator when out of scope RAII
   *
   */
  ~CommunicatorConcept() {
    if (IsValid()) {
      if (Name().empty())
        std::cout << "Freeing " << Name() << "\n";
      MPI_Comm_free(&m_comm);
      m_comm = MPI_COMM_NULL;
    }
  }

  template <typename T1, typename T2, typename T_Data>
  void SendRecvVector(const T1 num_recvs,
                      const std::vector<rank_t> &exportRanks,
                      const std::vector<T2> &offset,
                      std::vector<T_Data> &data) {
    std::map<int, std::vector<T_Data>> recv_buf;
    std::vector<MPI_Request> send_request(exportRanks.size());
    std::vector<MPI_Status> send_status(exportRanks.size());
    MPI_Status recv_stat;
    rank_t my_rank = Rank();
    std::size_t num_items_recv = 0;
#ifdef DEBUG_SEND_RECV_VECTOR
    std::stringstream cat;
    cat << "schedule_" << my_rank << ".dat";
    std::ofstream fout(cat.str());
#endif
    // MemCopy all contents of current rank to receive buffer
    for (std::size_t i = 0; i < exportRanks.size(); ++i) {
      if (exportRanks[i] == my_rank) {
        recv_buf[my_rank].resize(offset[i + 1] - offset[i]);
        std::copy(
            &data[offset[i]], &data[offset[i + 1]], recv_buf[my_rank].begin());
        num_items_recv += offset[i + 1] - offset[i];
      }
    }
    // Non-blocking send of all other rank data
    for (std::size_t i = 0; i < exportRanks.size(); ++i) {
      if (exportRanks[i] != my_rank) {
        std::size_t test_overflow =
            sizeof(T_Data) * (offset[i + 1] - offset[i]);
        if (test_overflow > std::numeric_limits<int>::max())
          Abort();
#ifdef DEBUG_SEND_RECV_VECTOR
        fout << "Send to rank = " << exportRanks[i] << " in rank " << my_rank
             << "\n"
             << std::flush;
#endif
        MPI_Issend(&data[offset[i]],
                   sizeof(T_Data) * (offset[i + 1] - offset[i]),
                   MPI_BYTE,
                   exportRanks[i],
                   my_rank,
                   RawHandle(),
                   &send_request[i]);
      }
    }
#ifdef DEBUG_SEND_RECV_VECTOR
    fout.close();
    cat.str("");
    cat << "recv_" << my_rank << ".dat";
    fout.open(cat.str());
    fout << "num_recv = " << num_recvs << "\n";
#endif
    // Blocking receive of data
    for (int i = 0; i < num_recvs; ++i) {
      int recv_size;
      MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, RawHandle(), &recv_stat);
      MPI_Get_count(&recv_stat, MPI_BYTE, &recv_size);
      recv_buf[recv_stat.MPI_SOURCE].resize(recv_size / sizeof(T_Data));
#ifdef DEBUG_SEND_RECV_VECTOR
      fout << "Receive in " << my_rank << " from rank " << recv_stat.MPI_SOURCE
           << "temp.size() = " << temp.size() << " temp_last = " << temp_last
           << "\n"
           << std::flush;
#endif
      MPI_Recv(recv_buf[recv_stat.MPI_SOURCE].data(),
               recv_size,
               MPI_BYTE,
               recv_stat.MPI_SOURCE,
               recv_stat.MPI_TAG,
               RawHandle(),
               &recv_stat);
      num_items_recv += recv_size / sizeof(T_Data);
    }
#ifdef DEBUG_SEND_RECV_VECTOR
    fout << "Done in proc " << my_rank << "\n" << std::flush;
#endif
    // Wait for send completion
    for (std::size_t i = 0; i < exportRanks.size(); ++i)
      if (exportRanks[i] != my_rank)
        MPI_Wait(send_request.data() + i, send_status.data() + i);
    // Swap data with the recv buffer
    data.clear();
    data.reserve(num_items_recv);
    // Map is in sorted order of ranks so this is consistent
    for (const auto &item : recv_buf)
      std::copy(
          item.second.begin(), item.second.end(), std::back_inserter(data));
    // Barrier is inevitable because all ranks in this comm should
    // sync till all rank buffers are swapped
    if (IsValid())
      Barrier();
  }

  /**
   * @brief Use 1-sided RMA to determine the number of recv given send
   * @details This implementation is based on the Cornell notes on RMA
   *          https://cvw.cac.cornell.edu/mpionesided/synchronization-calls/fence-synchronization
   * @tparam T
   * @param send_rank_list
   * @return
   */
  template <typename T>
  int InferNumReceive(T &send_rank_list, bool ignore_current_rank = false) {
    int counter = 0;
    int increment = 1;
    MPI_Win counter_window;
    if (IsValid()) {
      auto my_rank = Rank();
      MPI_Win_create(&counter,
                     sizeof(int),
                     1,
                     MPI_INFO_NULL,
                     RawHandle(),
                     &counter_window);
      /* No local operations prior to this epoch, so give an assertion */
      MPI_Win_fence(MPI_MODE_NOPRECEDE, counter_window);
      // MPI_Win_fence(0, counter_window);
      for (const auto &rank : send_rank_list)
        MPI_Accumulate(&increment,
                       1,
                       MPI_INT,
                       rank,
                       0,
                       1,
                       MPI_INT,
                       MPI_SUM,
                       counter_window);
      /* Complete the epoch - this will block until MPI_Get is complete */
      MPI_Win_fence(0, counter_window);
      /* All done with the window - tell MPI there are no more epochs */
      MPI_Win_fence(MPI_MODE_NOSUCCEED, counter_window);
      MPI_Win_free(&counter_window);
      if (ignore_current_rank)
        for (auto &rank : send_rank_list)
          if (rank == my_rank)
            counter--;
    }
    return counter;
  }

  /**
   * @brief Use 1-sided RMA to determine the number of recv given send as map
   * @details This implementation is based on the Cornell notes on RMA
   *          https://cvw.cac.cornell.edu/mpionesided/synchronization-calls/fence-synchronization
   * @tparam T
   * @tparam T_Payload
   * @param send_rank_list
   * @param ignore_current_rank
   * @return
   */
  template <typename T, typename T_Payload>
  int InferNumReceive(std::map<T, T_Payload> &send_rank_list,
                      bool ignore_current_rank = false) {
    int counter = 0;
    int increment = 1;
    MPI_Win counter_window;
    if (IsValid()) {
      auto my_rank = Rank();
      MPI_Win_create(&counter,
                     sizeof(int),
                     1,
                     MPI_INFO_NULL,
                     RawHandle(),
                     &counter_window);
      /* No local operations prior to this epoch, so give an assertion */
      MPI_Win_fence(MPI_MODE_NOPRECEDE, counter_window);
      // MPI_Win_fence(0, counter_window);
      for (const auto &rank : send_rank_list)
        MPI_Accumulate(&increment,
                       1,
                       MPI_INT,
                       rank.first,
                       0,
                       1,
                       MPI_INT,
                       MPI_SUM,
                       counter_window);
      /* Complete the epoch - this will block until MPI_Get is complete */
      MPI_Win_fence(0, counter_window);
      /* All done with the window - tell MPI there are no more epochs */
      MPI_Win_fence(MPI_MODE_NOSUCCEED, counter_window);
      MPI_Win_free(&counter_window);
      if (ignore_current_rank)
        for (auto &rank : send_rank_list)
          if (rank.first == my_rank)
            counter--;
    }
    return counter;
  }

#ifdef HAVE_HWLOC

  /**
   *
   * @param topo_info
   */
  static void GetTopologyInfo(std::vector<int> &topo_info) {
    hwloc_topology_t topo;
    hwloc_topology_init(&topo);
    hwloc_topology_load(topo);
    int num_sockets = hwloc_get_nbobjs_by_type(topo, HWLOC_OBJ_PACKAGE);
    int num_cores = hwloc_get_nbobjs_by_type(topo, HWLOC_OBJ_CORE);
    hwloc_topology_destroy(topo);
    if (!topo_info.empty())
      topo_info.clear();
    topo_info.reserve(2);
    /**
     * HWLOC_OBJ_CORE gives the total core count
     * which is num_sockets * core_per_socket
     */
    topo_info.push_back(num_cores / num_sockets);
    if (num_sockets > 1)
      topo_info.push_back(num_sockets);
  }

#else
  /**
   *
   * @param topo_info
   */
  static void GetTopologyInfo(std::vector<int> &topo_info) {
    if (!topo_info.empty())
      topo_info.clear();
    topo_info.push_back(1);
  }
#endif

  /**
   *
   * @tparam T_Data
   * @tparam T_PostReceiveLambda
   * @tparam T_PostSendLambda
   * @param data
   * @param post_recv
   * @param post_send
   * @param aggregate
   */
  template <typename T_Data,
            typename T_PostReceiveLambda,
            typename T_PostSendLambda>
  void DoGatherMaster(T_Data data,
                      T_PostReceiveLambda post_recv,
                      T_PostSendLambda post_send,
                      bool aggregate = false) {
    auto num_procs = size();
    MPI_Status receive_status;
    /**
     *  Execute only one valid communicators
     */
    if (IsValid()) {
      /**
       *  Master receives data from slaves and executes the
       *  post-receive hook lambda
       */
      if (IsMaster()) {
        post_recv(0, data);
        for (auto i = 1; i < num_procs; ++i) {
          auto tag = 0;
          for (auto item = data.begin(); item != std::end(data);
               ++item, ++tag) {
            auto &data_pair = item->second;
            /**
             * Probe the data to get size and reallocate if
             * size is not equal
             */
            MPI_Probe(i, tag, RawHandle(), &receive_status);
            int size_in_bytes;
            MPI_Get_count(&receive_status, MPI_BYTE, &size_in_bytes);
            std::size_t alloc_size =
                aggregate ? size_in_bytes + std::get<SIZE_FN>(data_pair)()
                          : size_in_bytes;
            std::get<RESIZE_FN>(data_pair)(alloc_size);
            auto recv_buffer_ptr =
                std::get<ACCESS_FN>(data_pair)(alloc_size - size_in_bytes);
            MPI_Recv(recv_buffer_ptr,
                     std::get<SIZE_IN_BYTES_FN>(data_pair)(),
                     MPI_BYTE,
                     i,
                     tag,
                     RawHandle(),
                     &receive_status);
          }
          post_recv(i, data);
        }
      }
      /**
       *  Slaves send the data to master rank and
       *  executes post-send hook lambda
       */
      if (!IsMaster()) {
        auto tag = 0;
        for (auto item = data.begin(); item != std::end(data); ++item, ++tag) {
          auto &data_pair = item->second;
          std::size_t test_overflow = std::get<SIZE_IN_BYTES_FN>(data_pair)();
          if (test_overflow >= std::numeric_limits<int>::max())
            Abort();
          /* Sync-send ensures that the recv is not flooded with traffic
           * and precludes the need for magic byte handshake */
          MPI_Ssend(std::get<ACCESS_FN>(data_pair)(0),
                    std::get<SIZE_IN_BYTES_FN>(data_pair)(),
                    MPI_BYTE,
                    0,
                    tag,
                    RawHandle());
        }
        post_send(data);
      }
    }
  }

  /**
   *
   * @tparam T_Data
   * @tparam T_PostReceiveLambda
   * @tparam T_PostSendLambda
   * @param data
   * @param post_recv
   * @param post_send
   * @param aggregate
   */
  template <typename T_Data,
            typename T_PreSendLambda,
            typename T_PostRecvLambda>
  void DoScatterMaster(T_Data data,
                       T_PreSendLambda pre_send,
                       T_PostRecvLambda post_recv,
                       bool aggregate = false) {
    auto num_procs = size();
    MPI_Status receive_status;
    /**
     *  Execute only one valid communicators
     */
    if (IsValid()) {
      /**
       *  Master receives data from slaves and executes the
       *  post-receive hook lambda
       */
      if (IsMaster()) {
        for (auto i = 1; i < num_procs; ++i) {
          auto tag = 0;
          for (auto item = data.begin(); item != std::end(data);
               ++item, ++tag) {
            auto &data_pair = item->second;
            pre_send(i, data);
            /* Sync-send ensures that the recv is not flooded with traffic
             * and precludes the need for magic byte handshake */
            // std::cout << "Sending in master to rank " << i
            //          << " size_in_bytes = "
            //          << std::get<SIZE_IN_BYTES_FN>(data_pair)() << "\n";
            std::size_t test_overflow = std::get<SIZE_IN_BYTES_FN>(data_pair)();
            if (test_overflow >= std::numeric_limits<int>::max())
              Abort();
            MPI_Ssend(std::get<ACCESS_FN>(data_pair)(0),
                      std::get<SIZE_IN_BYTES_FN>(data_pair)(),
                      MPI_BYTE,
                      i,
                      tag,
                      RawHandle());
          }
        }
        pre_send(0, data);
      }
      /**
       *  Slaves send the data to master rank and
       *  executes post-send hook lambda
       */
      if (!IsMaster()) {
        auto tag = 0;
        for (auto item = data.begin(); item != std::end(data); ++item, ++tag) {
          auto &data_pair = item->second;
          /**
           * Probe the data to get size and reallocate if
           * size is not equal
           */
          MPI_Probe(0, tag, RawHandle(), &receive_status);
          int size_in_bytes;
          MPI_Get_count(&receive_status, MPI_BYTE, &size_in_bytes);
          std::size_t alloc_size =
              aggregate ? size_in_bytes + std::get<SIZE_FN>(data_pair)()
                        : size_in_bytes;
          std::get<RESIZE_FN>(data_pair)(alloc_size);
          auto recv_buffer_ptr =
              std::get<ACCESS_FN>(data_pair)(alloc_size - size_in_bytes);
          // std::cerr << "Message: Receive in rank " << Rank()
          //<< " size in bytes = " << std::get<SIZE_IN_BYTES_FN>(data_pair)() <<
          //"\n";
          MPI_Recv(recv_buffer_ptr,
                   std::get<SIZE_IN_BYTES_FN>(data_pair)(),
                   MPI_BYTE,
                   0,
                   tag,
                   RawHandle(),
                   &receive_status);
        }
        post_recv(data);
      }
    }
  }

private:
  MPI_Comm m_comm = MPI_COMM_NULL; //!<
  std::string m_name;              //!<
};

} // namespace taru
