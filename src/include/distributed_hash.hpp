/**
 *  @file distributed_hash.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 30 April 2019
 *  @brief Documentation for distributed_hash.hpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */

#pragma once

#include "hdf5.hpp"
#include "mesh_common.hpp"
#include <type_traits>
#include <zoltan.h>

namespace taru {

/**
 * @brief Distributed hash table class
 *
 * @tparam T_Key (Key is integral type with zero starting index : C-Style)
 * @tparam T_Data (POD type)
 * @todo Remove dependency with ZoltanDD and implement own DD structure
 */
template <typename T_Key, typename T_Data> class DistributedHashTable {

public:
  /**
   * @brief Constructor for DistributedHashTable
   * @param communicator The communicator object
   */
  explicit DistributedHashTable(CommunicatorConcept &communicator)
      : m_dht(nullptr),
        m_communicator("DDHash") {
    Time(global_timer::TIME_HASH_CREATION, global_timer::C_TimerStart);
    m_communicator.Copy(communicator);
    static_assert(std::is_pod<T_Data>(),
                  "Error: Need a POD type for template T_Data\n");
    static_assert(std::is_pod<T_Key>(),
                  "Error: Need a POD type for template T_Key\n");
    static_assert(std::is_integral<T_Key>(),
                  "Error: Need an integral key type for template T_Key\n");
    if (m_communicator.IsValid()) {
      if (m_communicator.IsMaster()) {
        if (sizeof(ZOLTAN_ID_TYPE) != sizeof(id_size_t)) {
          std::cerr << "Error: Incompatible types "
                    << typeid(ZOLTAN_ID_TYPE).name() << " and "
                    << typeid(id_size_t).name() << "\n";
          abort();
        }
      }
    }
    Time(global_timer::TIME_HASH_CREATION, global_timer::C_TimerStop);
  }

  /**
   * @brief Calculates the local begin and size for a given rank.
   *
   * This function calculates the local begin and size for a given rank based on the global size of the distributed hash and the number of processes.
   *
   * @param rank The rank of the process.
   * @param local_begin Reference to a variable that will store the local begin.
   * @param local_size Reference to a variable that will store the local size.
   */
  void
  GetRankSizeBegin(rank_t rank, my_size_t &local_begin, my_size_t &local_size) {
    my_size_t global_size = m_size;
    rank_t num_procs = m_communicator.size();
    m_communicator.DistributionFunction(
        rank, num_procs, global_size, local_begin, local_size);
  }

  /**
   * @brief Set the debug level for the Distributed Hash
   *
   * @param debug_level
   */
  void SetDebugLevel(const int debug_level) { m_debug_level = debug_level; }

  /**
   * @brief Sets the size of the distributed hash table.
   *
   * This function sets the size of the distributed hash table and performs necessary
   * initialization steps. It takes a parameter `sz` of type `my_size_t` which represents
   * the desired size of the hash table.
   *
   * @param sz The size of the distributed hash table.
   */
  void SetSize(const my_size_t &sz) {
    Time(global_timer::TIME_HASH_CREATION, global_timer::C_TimerStart);
    if (m_communicator.IsValid()) {
      m_size = sz;
      m_communicator.RankDistribution(m_size, m_begin, m_local_size);
      /*if (m_communicator.IsMaster())
        std::cout << "Message: Creating ZoltanDD with T_Key: {size: "
                  << sizeof(T_Key) / sizeof(ZOLTAN_ID_TYPE)
                  << "} with POD T_Data: {size: " << sizeof(T_Data)
                  << " bytes}\n";
      */
      int rc = Zoltan_DD_Create(
          &m_dht,
          m_communicator.RawHandle(),
          /* GID size = */ sizeof(T_Key) / sizeof(ZOLTAN_ID_TYPE),
          /* LID size = */ 0, // Not using local ids for now
          /* Size of node data in char */ sizeof(T_Data),
          m_local_size,
          m_debug_level);
      if (rc != ZOLTAN_OK) {
      }
      assert(rc == ZOLTAN_OK);
    }
    Time(global_timer::TIME_HASH_CREATION, global_timer::C_TimerStop);
  }

  /**
   * @brief Populates the distributed hash table with keys and corresponding data.
   *
   * This function populates the distributed hash table with the provided keys and data.
   * It ensures that the size of the keys vector is equal to the size of the data vector.
   *
   * @param keys The vector of keys to be inserted into the hash table.
   * @param data The vector of corresponding data to be associated with the keys.
   */
  void Populate(std::vector<T_Key> &keys, std::vector<T_Data> &data) {
    Time(global_timer::TIME_HASH_CREATION, global_timer::C_TimerStart);
    assert(keys.size() == data.size());
    int rc = Zoltan_DD_Update(m_dht,
                              reinterpret_cast<ZOLTAN_ID_PTR>(&keys[0]),
                              NULL,
                              reinterpret_cast<char *>(&data[0]),
                              NULL,
                              data.size());
    if (rc != ZOLTAN_OK) {
    }
    assert(rc == ZOLTAN_OK);
    Time(global_timer::TIME_HASH_CREATION, global_timer::C_TimerStop);
  }

  /**
   * @brief Populates the point data from an HDF5 file.
   *
   * This function reads the point data from the specified HDF5 file and populates
   * the internal data structures with the read data. It uses the HDF5 library to
   * perform the file I/O operations.
   *
   * @param filename The path to the HDF5 file.
   * @param dset The dataset name within the HDF5 file.
   */
  void PopulatePoint(std::string filename, std::string dset) {
    auto plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, m_communicator.RawHandle(), MPI_INFO_NULL);
    /* Open file collectively */
    auto file = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, plist_id);
    H5Pclose(plist_id);
    /* Size was not set so read size from HDF5 file */
    if (size() == 0) {
      hsize_t file_dims[10];
      hdf5::GetDimensions(file, (dset + "/x").c_str(), file_dims);
      SetSize(file_dims[0]);
    }
    std::vector<T_Data> buffer(size());
    std::vector<T_Key> keys(size());
    for (T_Key i = 0; i < T_Key(size()); ++i)
      keys[i] = begin() + i;
    hdf5::Hyperslab<hsize_t, 1> f, m;
    m.offset[0] = 0;
    m.dims[0] = size() * 3;
    m.count[0] = f.count[0] = size();
    m.stride[0] = 3;
    f.stride[0] = 1;
    f.dims[0] = GlobalSize();
    f.offset[0] = begin();
    hdf5::ReadCollective(
        file, dset + "/x", f, m, reinterpret_cast<double *>(&buffer[0]));
    m.offset[0] = 1;
    hdf5::ReadCollective(
        file, dset + "/y", f, m, reinterpret_cast<double *>(&buffer[0]));
    m.offset[0] = 2;
    hdf5::ReadCollective(
        file, dset + "/z", f, m, reinterpret_cast<double *>(&buffer[0]), false);
    H5Fclose(file);
    int rc = Zoltan_DD_Update(m_dht,
                              reinterpret_cast<ZOLTAN_ID_PTR>(&keys[0]),
                              NULL,
                              reinterpret_cast<char *>(&buffer[0]),
                              NULL,
                              buffer.size());
    if (rc != ZOLTAN_OK) {
    }
    assert(rc == ZOLTAN_OK);
  }

  /**
   * @brief Populates the distributed hash table with data from an HDF5 file.
   *
   * This function opens the specified HDF5 file and reads data from the specified dataset.
   * It uses MPI parallel I/O to read the data collectively. If the size of the distributed hash table
   * is not set, it reads the size from the HDF5 file and sets it accordingly.
   * The function then populates the distributed hash table with the read data.
   *
   * @param filename The path to the HDF5 file.
   * @param dset The name of the dataset to read from.
   * @tparam T_Shadow The type of the data to read from the HDF5 file.
   */
  template <typename T_Shadow>
  void Populate(std::string filename, std::string dset) {
    auto plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, m_communicator.RawHandle(), MPI_INFO_NULL);
    /* Open file collectively */
    auto file = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, plist_id);
    H5Pclose(plist_id);
    /* Size was not set so read size from HDF5 file */
    if (size() == 0) {
      hsize_t file_dims[10];
      hdf5::GetDimensions(file, dset.c_str(), file_dims);
      SetSize(file_dims[0]);
    }
    std::vector<T_Data> buffer(size());
    std::vector<T_Key> keys(size());
    for (T_Key i = 0; i < T_Key(size()); ++i)
      keys[i] = begin() + i;
    hdf5::Hyperslab<hsize_t, 1> f, m;
    m.dims[0] = size();
    m.count[0] = f.count[0] = m.dims[0];
    m.stride[0] = f.stride[0] = 1;
    f.dims[0] = GlobalSize();
    f.offset[0] = begin();
    hdf5::ReadCollective(
        file, dset.c_str(), f, m, reinterpret_cast<T_Shadow *>(&buffer[0]));
    H5Fclose(file);
    int rc = Zoltan_DD_Update(m_dht,
                              reinterpret_cast<ZOLTAN_ID_PTR>(&keys[0]),
                              NULL,
                              reinterpret_cast<char *>(&buffer[0]),
                              NULL,
                              buffer.size());
    if (rc != ZOLTAN_OK) {
    }
    assert(rc == ZOLTAN_OK);
  }

  /**
   * @brief Clears all contents of the distributed hash table
   */
  void clear() {
    if (m_dht != nullptr) {
      Zoltan_DD_Destroy(&m_dht);
      m_dht = nullptr;
    }
    m_begin = 0;
    m_local_size = 0;
    m_size = 0;
  }

  /**
   * @brief Populates the `keys` and `buffer` vectors with data read from an HDF5 file.
   *
   * @param filename The path to the HDF5 file.
   * @param dset An array of dataset names corresponding to each element type.
   * @param global_offsets An array of global offsets for each element type.
   * @param keys The vector to store the keys read from the file.
   * @param buffer The vector to store the data read from the file.
   * @tparam T_Int The type of the global offsets.
   */
  template <typename T_Int>
  void PopulateElements(const char filename[],
                        const char *dset[],
                        T_Int global_offsets[C_NumElementTypes + 1],
                        std::vector<T_Key> &keys,
                        std::vector<T_Data> &buffer) {
    buffer.clear();
    keys.clear();
    buffer.resize(size());
    keys.resize(size());
    for (T_Int i = 0; i < size(); ++i)
      keys[i] = begin() + i;

    T_Int local_offsets[C_NumElementTypes + 1] = {0};
    T_Int file_position[C_NumElementTypes] = {0};
    GetElementOffsets(global_offsets,
                      begin(),
                      begin() + size(),
                      local_offsets,
                      file_position);
    hdf5::Hyperslab<hsize_t, 2> f, m;
    auto plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, m_communicator.RawHandle(), MPI_INFO_NULL);
    /* Open file collectively */
    auto file = H5Fopen(filename, H5F_ACC_RDONLY, plist_id);
    H5Pclose(plist_id);

    for (unsigned i = 0; i < C_NumElementTypes; ++i) {
      // Only do a collective read if the element type is found in the file
      if (global_offsets[i + 1] - global_offsets[i] > 0) {
        // Memory layout for read
        m.dims[0] = local_offsets[C_NumElementTypes];
        m.dims[1] = 8;
        m.offset[0] = local_offsets[i];
        m.offset[1] = 0;
        m.count[0] = local_offsets[i + 1] - local_offsets[i];
        m.count[1] = C_ElementNumNodes[i];
        m.stride[0] = 1;
        m.stride[1] = 1;
        // File layout for read
        f.offset[0] = file_position[i] * C_ElementNumNodes[i];
        f.offset[1] = 0;
        f.stride[0] = 1;
        f.stride[1] = 1;
        f.count[0] =
            (local_offsets[i + 1] - local_offsets[i]) * C_ElementNumNodes[i];
        f.count[1] = 1;
        hdf5::ReadCollective(
            file, dset[i], f, m, reinterpret_cast<T_Key *>(&buffer[0]));
      }
    }
    H5Fclose(file);
    Populate(keys, buffer);
  }

  /**
   * @brief Retrieves data from the distributed hash table based on the given keys.
   *
   * @param keys The vector of keys to retrieve data for.
   * @param data The vector to store the retrieved data.
   *
   * @note This function assumes that the size of the 'keys' vector is equal to the size of the 'data' vector.
   */
  void GetDataFromKeys(const std::vector<T_Key> &keys,
                       const std::vector<T_Data> &data) const {
    Time(global_timer::TIME_HASH_FIND, global_timer::C_TimerStart);
    assert(keys.size() == data.size());
    auto nc_keys_ptr = const_cast<T_Key *>(&keys[0]);
    auto nc_data_ptr = const_cast<T_Data *>(&data[0]);
    int rc = Zoltan_DD_Find(m_dht,
                            reinterpret_cast<ZOLTAN_ID_PTR>(nc_keys_ptr),
                            NULL,
                            reinterpret_cast<char *>(nc_data_ptr),
                            NULL,
                            data.size(),
                            NULL);
    if (rc != ZOLTAN_OK) {
    }
    assert(rc == ZOLTAN_OK);
    Time(global_timer::TIME_HASH_FIND, global_timer::C_TimerStop);
  }

  /**
   * @brief Get the DHT communicator handle
   *
   * @return Reference to the CommunicatorConcept handle
   */
  CommunicatorConcept &CommunicatorHandle() { return m_communicator; }

  /**
   * @brief Destructor to cleanup data
   *
   */
  ~DistributedHashTable() {
    if (m_dht != nullptr)
      Zoltan_DD_Destroy(&m_dht);
  }

  /**
   * @brief Access the local hunk size of the hash data
   *
   * @return The local size of the hash hunk
   */
  my_size_t size() const { return m_local_size; }

  /**
   * @brief The begin offset of the hash hunk
   *
   * @return The begin offset of the hash hunk
   */
  my_size_t begin() const { return m_begin; }

  /**
   * @brief The begin offset of the hash hunk
   *
   * @return The begin offset of the hash hunk
   */
  my_size_t GlobalSize() const { return m_size; }

  /**
   * @brief Checks if the given `DistributedHashTable` object is equal to the current `DistributedHashTable` object.
   *
   * @tparam T_Stream The type of the error stream.
   * @param dht The `DistributedHashTable` object to compare with.
   * @param err Pointer to the error stream. If provided, error messages will be written to this stream.
   * @return `true` if the two `DistributedHashTable` objects are equal, `false` otherwise.
   *
   * This function compares the local size and global size of the two `DistributedHashTable` objects. If they are not equal,
   * an error message is written to the error stream (if provided) and `false` is returned. If the sizes are equal, the function
   * compares the data associated with each key in the two `DistributedHashTable` objects. If any data mismatch is found, an
   * error message is written to the error stream (if provided) and `false` is returned. If all comparisons pass, `true` is returned.
   *
   * @note The `T_Key` and `T_Data` template parameters must be the same for both `DistributedHashTable` objects.
   */
  template <typename T_Stream>
  bool IsEqual(DistributedHashTable<T_Key, T_Data> &dht,
               T_Stream *err = nullptr) {
    if (dht.size() != size()) {
      if (err != nullptr)
        *err << "Error: DHT local size not equal for <T_Key : "
             << typeid(T_Key).name() << ", T_Data : " << typeid(T_Data).name()
             << "\n";
      return false;
    }
    if (dht.GlobalSize() != GlobalSize()) {
      if (err != nullptr)
        *err << "Error: DHT global size not equal for <T_Key : "
             << typeid(T_Key).name() << ", T_Data : " << typeid(T_Data).name()
             << "\n";
      return false;
    }
    bool all_ok = true;
    if (GlobalSize() > 0) {
      std::vector<T_Data> buffer(size());
      std::vector<T_Data> buffer_dht(size());
      std::vector<T_Key> keys(size());
      for (T_Key i = 0; i < T_Key(size()); ++i)
        keys[i] = begin() + i;
      GetDataFromKeys(keys, buffer);
      dht.GetDataFromKeys(keys, buffer_dht);
      // Compate data of key
      for (T_Key i = 0; i < T_Key(size()); ++i) {
        if (buffer[i] == buffer_dht[i]) {
        } else {
          if (err != nullptr)
            *err << "Error: Key " << i
                 << " data mismatch [Data1 : " << buffer[i]
                 << ", Data2 : " << buffer_dht[i] << "]\n";
          all_ok = false;
        }
      }
    }
    return all_ok;
  }

private:
  Zoltan_DD_Struct *m_dht;            //!<
  CommunicatorConcept m_communicator; //!<
  my_size_t m_begin = 0;              //!<
  my_size_t m_local_size = 0;         //!<
  my_size_t m_size = 0;               //!<
  int m_debug_level = 0;
};

} // namespace taru
