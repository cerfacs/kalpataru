/**
 *  @file libtreeadapt.h
 *  @brief Documentation for the KalpaTARU C-API library interface
 *  @details TODO
 *  @author Pavanakumar Mohanamuraly (mpkumar@cerfacs.fr)
 *  @date 31 January 2021
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse
 */
#pragma once

#include "treepart_config.h"
#include <mpi.h>

#ifdef __cplusplus
typedef taru::id_size_t
    t_index; //!< AVBP t_index (if incompatible with id_size_t)
extern "C" {
#else
typedef id_size_t t_index; //!< AVBP t_index (if incompatible with id_size_t)
#endif
typedef double t_scalar; //!< AVBP t_scalar (if incompatible with double)

/**
 * @brief This is an AVBP conforming API call to partition mesh
 *         expects the inputs from AVBP in the correct ordering
 *         (refer to AVBP documentation for details).
 *
 * @param elmdist TODO
 * @param eptr TODO
 * @param eind TODO
 * @param part TODO
 * @param numflag TODO
 * @param el_coords TODO
 * @param lcl_cnt TODO
 * @param numdims TODO
 * @param num_parts TODO
 * @param comm TODO
 * @param dg_ptr TODO
 * @param dg_graph TODO
 */
void libtreepart_avbp(t_index *elmdist,
                      t_index *eptr,
                      t_index *eind,
                      t_index *part,
                      t_index numflag,
                      t_scalar *el_coords,
                      int lcl_cnt,
                      int numdims,
                      t_index num_parts,
                      MPI_Comm comm,
                      t_index *dg_ptr,
                      t_index *dg_graph);

/**
 * @brief Assumes a Fortran program to call this routine
 *        passing a MPI Communicator object
 *
 * @param num_dims
 * @param fcomm
 */
void init_libtreeadapt(int *num_dims, MPI_Fint *fcomm);

/**
 * @brief Close the treeadapt API interface
 */
void close_libtreeadapt();

/**
 * @brief Node coordinates callback
 *
 * @param nnodes TODO
 * @param mynodes TODO
 * @param nodeid TODO
 * @param x TODO
 */
void libtreeadapt_set_nodes(int *nnodes, int *mynodes, int *nodeid, double *x);

/**
 * @brief Get the tri elm object
 *
 * @param ntrielm
 * @param trielm
 */
void libtreeadapt_set_triangles(int *ntrielm, int *trielm);

/**
 * @brief Get the tet elm object
 *
 * @param ntetelm
 * @param tetelm
 */
void libtreeadapt_set_tets(int *ntetelm, int *tetelm);

/**
 * @brief Get the bnode info object
 *
 * @param npatch TODO
 * @param nbnodes TODO
 * @param bnode_begin TODO
 * @param bnode_size TODO
 * @param bnode_to_node TODO
 */
void libtreeadapt_set_bnodes(int *npatch,
                             int *nbnodes,
                             int *bnode_begin,
                             int *bnode_size,
                             int *bnode_to_node);

/**
 * @brief TODO
 *
 * @param mmg_verbosity TODO
 */
void libtreeadapt_option_mmg_verbosity(int *mmg_verbosity);

/**
 * @brief TODO
 *
 * @param write_intermediate TODO
 */
void libtreeadapt_option_dowrite(int *write_intermediate);

/**
 * @brief TODO
 *
 * @param hGrad TODO
 */
void libtreeadapt_option_hGrad(double *hGrad);

/**
 * @brief TODO
 *
 * @param hausdorff TODO
 */
void libtreeadapt_option_hausdorff(double *hausdorff);

/**
 * @brief TODO
 *
 * @param m_mmg_hmin TODO
 */
void libtreeadapt_option_hmin(double *m_mmg_hmin);

/**
 * @brief TODO
 *
 * @param m_mmg_hmax TODO
 */
void libtreeadapt_option_hmax(double *m_mmg_hmax);

/**
 * @brief TODO
 *
 * @param m_mmg_max_iter TODO
 */
void libtreeadapt_option_maxIter(int *m_mmg_max_iter);

/**
 * @brief TODO
 *
 * @param m_adapt_threshold_max TODO
 */
void libtreeadapt_option_maxAdaptThreshold(double *m_adapt_threshold_max);

/**
 * @brief TODO
 *
 * @param m_adapt_threshold_min TODO
 */
void libtreeadapt_option_minAdaptThreshold(double *m_adapt_threshold_min);

/**
 * @brief Set deviation weights for adapt load-balancing
 *
 * @param fbool TODO
 */
void libtreeadapt_option_deviation_weights(int *fbool);

/**
 * @brief print all options
 */
void libtreeadapt_print_opt();

/**
 * @brief Set the grid dimension (2-2D, 3-3D)
 */
void libtreeadapt_set_ndim(int *);

/**
 * @brief Get the grid dimension (2-2D, 3-3D)
 */
void libtreeadapt_get_ndim(int *);

/**
 * @brief Reset the solution variables
 */
void libtreeadapt_reset_solution();

/**
 * @brief Solution variable being set here
 *
 * @param offset TODO
 * @param stride TODO
 * @param field TODO
 */
void libtreeadapt_add_solution(int *offset, int *stride, double *field);

/**
 * @brief TODO
 *
 * @param n TODO
 * @param nsol TODO
 * @param offset TODO
 * @param stride TODO
 * @param gids TODO
 * @param var TODO
 */
void libtreeadapt_get_solution(int *n,
                               int *nsol,
                               int *offset,
                               int *stride,
                               int *gids,
                               double *var);

/**
 * @brief Set the iso metric
 *
 * @param n TODO
 * @param gids TODO
 * @param metric TODO
 */
void libtreeadapt_get_iso_metric(int *n, int *gids, double *metric);

/**
 * @brief Set the iso metric
 *
 * @param metric TODO
 */
void libtreeadapt_iso_metric(double *metric);

/**
 * @brief Allocate the pointer array of boundary faces for data transfer
 *
 * @param npatches
 */
void libtreeadapt_allocate_bfaces(int *npatches);

/**
 * @brief Set the boundary face data pointer for patch given in patch_no
 *
 * @param patch_no TODO
 * @param nbfaces TODO
 * @param bface_elm TODO
 * @param bface_no TODO
 */
void libtreeadapt_set_bfaces(int *patch_no,
                             int *nbfaces,
                             int *bface_elm,
                             int *bface_no);

/**
 * @brief API interface for copying mesh from external solver
 *        using PointerMeshReader class template
 */
void libtreeadapt_mesh_from_ptr();

/**
 * @brief Read the mesh data (bootstrap) from hip HDF5 mesh file
 *
 */
void libtreeadapt_mesh_from_file();

/**
 * @brief TODO
 *
 * @param name_len
 * @param name
 * @todo This is a hack to quickly couple with AVBP (i.e., reading
 *       directly from the file the boundary information instead of
 *       constructing it from local data). But eventually we will
 *       allow TreePart to take over the file I/O part so is this
 *       so bad? Needs that moment of enlightenment !
 */
void libtreeadapt_mesh_filename(int *name_len, char *name);

/**
 * @brief Copy the external solution to the dictionary for adaptation loop
 */
void libtreeadapt_copy_external_solution();

/**
 * @brief TODO
 *
 */
void libtreeadapt_copy_node_coordinates();

/**
 * @brief TODO
 */
void libtreeadapt_copy_external_iso_metric();

/**
 * @brief TODO
 */
void libtreeadapt_allocate_solution();

/**
 * @brief TODO
 */
void libtreeadapt_adapt_loop();

/**
 * @brief Calculates the sum of all length of all edges connected to a given
 * node and also output the count. Assumes elmcon (element connectivity is
 *        contiguously numbers for tet/tri element like node1_elm1, node2_elm1,
 * ...) Also assumes elmconn is locally indexed from 1 to nnodes
 *
 * @param nnodes TODO
 * @param nelm TODO
 * @param elmcon TODO
 * @param shared_proc_cnt TODO
 * @param shared_proc_ranks TODO
 * @param shared_nodes_cnt TODO
 * @param shared_nodes_lid TODO
 * @param xyz TODO
 * @param sumlen TODO
 * @param edgcnt TODO
 */
void libtreeadapt_compute_edge_length_sum(int *nnodes,
                                          int *nelm,
                                          int *elmcon,
                                          int *shared_proc_cnt,
                                          int *shared_proc_ranks,
                                          int *shared_nodes_cnt,
                                          int *shared_nodes_lid,
                                          double *xyz,
                                          double *sumlen,
                                          double *edgcnt);

/**
 * @brief Pass the communicator info via API
 *
 * @param fcomm
 * @param comm_size
 * @param my_rank
 */
void libtreeadapt_copy_comm(MPI_Fint *fcomm, int *comm_size, int *my_rank);

/**
 * @brief TODO
 *
 * @param node_count TODO
 */
void libtreeadapt_get_nnode(int *node_count);

/**
 * @brief TODO
 *
 * @param node_coords TODO
 * @param node_global_indices TODO
 */
void libtreeadapt_get_nodes(double *node_coords, int *node_global_indices);

/**
 * @brief Give the count of each element type in the local mesh
 *
 * @param elmnod
 */
void libtreeadapt_num_elements(int *elmnod);

/**
 * @brief TODO
 *
 * @param elmnodptr TODO
 */
void libtreeadapt_get_element_node_conn(int *elmnodptr);

/**
 * @brief TODO
 *
 * @param num_owned TODO
 */
void libtreeadapt_get_num_unique_nodes(int *num_owned);

/**
 * @brief TODO
 *
 * @param num_nodes TODO
 */
void libtreeadapt_get_global_nnodes(int *num_nodes);

/**
 * @brief TODO
 *
 * @param num_shared_rank TODO
 */
void libtreeadapt_get_num_ngbr_procs(int *num_shared_rank);

/**
 * @brief TODO
 *
 * @param shared_node_count TODO
 */
void libtreeadapt_get_shared_node_count(int *shared_node_count);

/**
 * @brief TODO
 *
 * @param shared_lids TODO
 */
void libtreeadapt_get_shared_nodes(int *shared_lids);

/**
 * @brief TODO
 *
 * @param patch_cnt TODO
 * @param bnode_cnt TODO
 */
void libtreeadapt_get_patch_count(int *patch_cnt, int *bnode_cnt);

/**
 * @brief TODO
 *
 * @param patch_ofs TODO
 * @param bnodes_lidx TODO
 */
void libtreeadapt_get_bnode_data(int *patch_ofs, int *bnodes_lidx);

/**
 * @brief TODO
 *
 * @param nnodes TODO
 * @param deviation TODO
 */
void libtreeadapt_get_deviation(int *nnodes, double *deviation);

/**
 * @brief TODO
 */
void libtreeadapt_clear_adapt_meta();

/**
 * @brief TODO
 *
 * @param ndim TODO
 * @param nnode TODO
 * @param nelm TODO
 * @param npatch TODO
 * @param elmnod TODO
 * @param bnode_cnt TODO
 * @param bnode TODO
 * @param xyz TODO
 */
void libtreeadapt_debug_tecplot(int *ndim,
                                int *nnode,
                                int *nelm,
                                int *npatch,
                                int *elmnod,
                                int *bnode_cnt,
                                int *bnode,
                                double *xyz);

/**
 * @brief Return the owned nodes marker
 *
 * @param nnodes TODO
 * @param marker TODO
 */
void libtreeadapt_get_owned_marker(int *nnodes, int *marker);

/**
 * @brief Toggle for checking the interpolation agaist linear function
 *
 * @param do_check TODO
 */
void libtreeadapt_option_interpCheck(int *do_check);

/**
 * @brief Set the interpolation type in adaptation steps
 *
 * @param interp_type TODO
 */
void libtreeadapt_option_interpType(int *interp_type);

/**
 * @brief Set the output filename of the final adapted mesh
 *
 * @param filename
 * @param len
 */
void libtreeadapt_option_mesh_filename(int *len, char *filename);

/*
 *  @brief Group cells into cache block sizes and colour them
 *
 *  perm (integer[ne])
 *  |-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-|
 *
 *  cell groups (integer[ng])
 *  |--------|---|------|----|----|----|-------|------|---------|
 *
 *  element type offset (obtained from eptr and eind itself integer[ntypes+1])
 *  |------TET---------------|------PYR--------|------HEX-------|
 *
 *  @param colour array (integer[ng]) stores the group ids that have the same
 * colour
 *  @param offset colour offset (integer[ng + 1])
 *  @param nn number of nodes
 *  @param ne number of elements
 *  @param mcg number of element groups
 *  @param ncommon number of common nodes (dual-graph)
 *  @param eptr Metis style element ptr
 *  @param eind Metis style element node index
 *  @param epart Metis style element partition colour
 *  @param perm array permutation to recover the reordered cache groups (size of
 * ne)
 *  @param max_gsize Maximum group size of cache block
 */
void colour_api_call(t_index *nn,
                     t_index *ne,
                     t_index *mcg,
                     t_index *ncommon,
                     t_index *eptr,
                     t_index *eind,
                     t_index *gofs,
                     t_index *gcolour,
                     t_index *epart,
                     t_index *perm,
                     t_index *max_gsize);

/**
 * @brief For a 2D triangle mesh with group colours, write the mesh to a tecplot
 * file tec.dat
 *
 * @param nn The number of nodes.
 * @param ne The number of elements.
 * @param mcg The number of group colors.
 * @param x Pointer to the X coordinates.
 * @param y Pointer to the Y coordinates.
 * @param z Pointer to the Z coordinates (optional).
 * @param eptr Pointer to the element pointer array.
 * @param eind Pointer to the element index array.
 * @param gofs Pointer to the group offset array.
 * @param gcolour Pointer to the group color array.
 * @param perm Pointer to the permutation array.
 */
void write_colour_triangle(t_index *nn,
                           t_index *ne,
                           t_index *mcg,
                           double *x,
                           double *y,
                           double *z,
                           t_index *eptr,
                           t_index *eind,
                           t_index *gofs,
                           t_index *gcolour,
                           t_index *perm);

#ifdef __cplusplus
};
#endif
