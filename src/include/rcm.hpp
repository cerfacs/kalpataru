/** @file rcm.hpp
 *  @author Pavanakumar Mohanamuraly
 *  @date 26 February 2020
 *  @brief RCM Algorithm by Alan George, Joseph Liu,
 *         Computer Solution of Large Sparse Positive Definite Systems,
 *         Prentice Hall, 1981.
 * @copyright Copyright (c) 2024 ALGO-COOP Team CERFACS.
 * @license This project is released under the MIT license.
 */

#ifndef __REVERSE_CUTHILL_MCKEE_HPP__

#define __REVERSE_CUTHILL_MCKEE_HPP__

// #include <fmt/core.h>
#include <fstream>
#include <vector>

namespace taru {

/**
 * @brief Morton ordering hash function 2d (64bit)
 *
 * @param x Integer coordinate of X
 * @param y Integer coordinate of Y
 * @return Integer hash of Morton index
 */
inline std::uint64_t morton2d_64bit(std::uint64_t x, std::uint64_t y) {
  std::uint64_t z = 0;
  /* x */
  x = (x | (x << 16)) & 0x0000FFFF0000FFFF;
  x = (x | (x << 8)) & 0x00FF00FF00FF00FF;
  x = (x | (x << 4)) & 0x0F0F0F0F0F0F0F0F;
  x = (x | (x << 2)) & 0x3333333333333333;
  x = (x | (x << 1)) & 0x5555555555555555;
  /* y */
  y = (y | (y << 16)) & 0x0000FFFF0000FFFF;
  y = (y | (y << 8)) & 0x00FF00FF00FF00FF;
  y = (y | (y << 4)) & 0x0F0F0F0F0F0F0F0F;
  y = (y | (y << 2)) & 0x3333333333333333;
  y = (y | (y << 1)) & 0x5555555555555555;
  /* final key */
  z = x | (y << 1);
  return z;
}

/**
 * @brief Morton ordering hash function 3d (10bit)
 *
 * @param x Integer coordinate of X
 * @param y Integer coordinate of Y
 * @param z Integer coordinate of Z
 * @return Integer hash of Morton index
 */
inline std::uint32_t
morton3d_10bit(std::uint32_t x, std::uint32_t y, std::uint32_t z) {
  x = (x | (x << 16)) & 0x030000FF;
  x = (x | (x << 8)) & 0x0300F00F;
  x = (x | (x << 4)) & 0x030C30C3;
  x = (x | (x << 2)) & 0x09249249;

  y = (y | (y << 16)) & 0x030000FF;
  y = (y | (y << 8)) & 0x0300F00F;
  y = (y | (y << 4)) & 0x030C30C3;
  y = (y | (y << 2)) & 0x09249249;

  z = (z | (z << 16)) & 0x030000FF;
  z = (z | (z << 8)) & 0x0300F00F;
  z = (z | (z << 4)) & 0x030C30C3;
  z = (z | (z << 2)) & 0x09249249;

  return x | (y << 1) | (z << 2);
}

/**
 * @brief Morton ordering hash function 3d (20bit)
 *
 * @param x Integer coordinate of X
 * @param y Integer coordinate of Y
 * @param z Integer coordinate of Z
 * @return Integer hash of Morton index
 */
inline std::uint64_t
morton3d_20bit(std::uint32_t x, std::uint32_t y, std::uint32_t z) {
  std::uint32_t lo_x = x & 1023u;
  std::uint32_t lo_y = y & 1023u;
  std::uint32_t lo_z = z & 1023u;
  std::uint32_t hi_x = x >> 10u;
  std::uint32_t hi_y = y >> 10u;
  std::uint32_t hi_z = z >> 10u;
  return (std::uint64_t(morton3d_10bit(hi_x, hi_y, hi_z)) << 30) |
         std::uint64_t(morton3d_10bit(lo_x, lo_y, lo_z));
}

namespace details {
/**
 * @brief DEGREE computes the degrees of the nodes in the connected component
 * @details The connected component is specified by MASK and ROOT.
 *          Nodes for which MASK is zero are ignored.
 *
 * @tparam T_Integer
 * @param root Node that defines the connected component
 * @param adj_num Number of adjacency entries ([NODE_NUM+1])
 * @param adj_row Information about row I is stored in entries ADJ_ROW(I)
 * through ADJ_ROW(I+1)-1 of ADJ ([ADJ_NUM])
 * @param adj Adjacency structure, for each row, it contains the column indices
 * of the nonzero entries
 * @param mask Nonzero for those nodes which are to be considered ([NODE_NUM])
 * @param deg Contains, for each  node in the connected component, its degree
 * ([NODE_NUM])
 * @param iccsze Number of nodes in the connected component
 * @param ls Stores in entries 1 through ICCSIZE the nodes in the connected
 * component, starting with ROOT, and proceeding by levels ([NODE_NUM])
 * @param node_num Number of nodes
 *
 */
template <typename T_Integer>
void degree(T_Integer root,
            T_Integer adj_num,
            T_Integer adj_row[],
            T_Integer adj[],
            T_Integer mask[],
            T_Integer deg[],
            T_Integer &iccsze,
            T_Integer ls[],
            const T_Integer node_num) {
  //
  //  The sign of ADJ_ROW(I) is used to indicate if node I has been considered.
  //
  ls[0] = root;
  adj_row[root - 1] = -adj_row[root - 1];
  T_Integer lvlend = 0;
  iccsze = 1;
  //
  //  LBEGIN is the pointer to the beginning of the current level, and
  //  LVLEND points to the end of this level.
  //
  for (;;) {
    auto lbegin = lvlend + 1;
    lvlend = iccsze;
    //
    //  Find the degrees of nodes in the current level,
    //  and at the same time, generate the next level.
    //
    for (T_Integer i = lbegin; i <= lvlend; i++) {
      auto node = ls[i - 1];
      auto jstrt = -adj_row[node - 1];
      auto jstop = abs(adj_row[node]) - 1;
      T_Integer ideg = 0;

      for (T_Integer j = jstrt; j <= jstop; j++) {
        auto nbr = adj[j - 1];

        if (mask[nbr - 1] != 0) {
          ideg = ideg + 1;

          if (0 <= adj_row[nbr - 1]) {
            adj_row[nbr - 1] = -adj_row[nbr - 1];
            iccsze = iccsze + 1;
            ls[iccsze - 1] = nbr;
          }
        }
      }
      deg[node - 1] = ideg;
    }
    //
    //  Compute the current level width.
    //
    auto lvsize = iccsze - lvlend;
    //
    //  If the current level width is nonzero, generate another level.
    //
    if (lvsize == 0) {
      break;
    }
  }
  //
  //  Reset ADJ_ROW to its correct sign and return.
  //
  for (T_Integer i = 0; i < iccsze; i++) {
    auto node = ls[i] - 1;
    adj_row[node] = -adj_row[node];
  }

  return;
}

/**
 * @brief LEVEL_SET generates the connected level structure rooted at a given
 * node
 * @details  Only nodes for which MASK is nonzero will be considered.
 *           The root node chosen by the user is assigned level 1, and masked.
 *           All (unmasked) nodes reachable from a node in level 1 are
 *           assigned level 2 and masked.  The process continues until there
 *           are no unmasked nodes adjacent to any node in the current level.
 *           The number of levels may vary between 2 and NODE_NUM.
 *
 * @tparam T_Integer
 * @param root Node at which the level structure is to be rooted
 * @param adj_num Number of adjacency entries
 * @param adj_row Information about row I is stored in entries ADJ_ROW(I)
 * through ADJ_ROW(I+1)-1 of ADJ
 * @param adj Adjacency structure, For each row, it contains the column indices
 * of the nonzero entries
 * @param mask On input, only nodes with nonzero MASK are to be processed.  On
 * output, those nodes which were included in the level set have MASK set to 1
 * @param level_num Number of levels in the level structure.  ROOT is in
 * level 1.  The neighbors of ROOT are in level 2, and so on
 * @param level_row The rooted level structure rows [NODE_NUM+1]
 * @param level The rooted level structure
 * @param node_num Number of nodes
 *
 */
template <typename T_Integer>
void level_set(T_Integer root,
               T_Integer adj_num,
               T_Integer adj_row[],
               T_Integer adj[],
               T_Integer mask[],
               T_Integer &level_num,
               T_Integer level_row[],
               T_Integer level[],
               const T_Integer node_num) {

  mask[root - 1] = 0;
  level[0] = root;
  level_num = 0;
  auto lvlend = 0;
  auto iccsze = 1;
  //
  //  LBEGIN is the pointer to the beginning of the current level, and
  //  LVLEND points to the end of this level.
  //
  for (;;) {
    auto lbegin = lvlend + 1;
    lvlend = iccsze;
    level_num = level_num + 1;
    level_row[level_num - 1] = lbegin;
    //
    //  Generate the next level by finding all the masked neighbors of nodes
    //  in the current level.
    //
    for (T_Integer i = lbegin; i <= lvlend; i++) {
      auto node = level[i - 1];
      auto jstrt = adj_row[node - 1];
      auto jstop = adj_row[node] - 1;

      for (T_Integer j = jstrt; j <= jstop; j++) {
        auto nbr = adj[j - 1];

        if (mask[nbr - 1] != 0) {
          iccsze = iccsze + 1;
          level[iccsze - 1] = nbr;
          mask[nbr - 1] = 0;
        }
      }
    }
    //
    //  Compute the current level width (the number of nodes encountered.)
    //  If it is positive, generate the next level.
    //
    auto lvsize = iccsze - lvlend;

    if (lvsize <= 0) {
      break;
    }
  }

  level_row[level_num] = lvlend + 1;
  //
  //  Reset MASK to 1 for the nodes in the level structure.
  //
  for (T_Integer i = 0; i < iccsze; i++) {
    mask[level[i] - 1] = 1;
  }

  return;
}

/**
 * @brief ROOT_FIND finds a pseudo-peripheral node.
 *
 * @details The diameter of a graph is the maximum distance (number of edges)
 *          between any two nodes of the graph.
 *
 *          The eccentricity of a node is the maximum distance between that
 *          node and any other node of the graph.
 *
 *          A peripheral node is a node whose eccentricity equals the
 *          diameter of the graph.
 *
 *          A pseudo-peripheral node is an approximation to a peripheral node;
 *          it may be a peripheral node, but all we know is that we tried our
 *          best.
 *
 *          The routine is given a graph, and seeks pseudo-peripheral nodes,
 *          using a modified version of the scheme of Gibbs, Poole and
 *          Stockmeyer.  It determines such a node for the section subgraph
 *          specified by MASK and ROOT.
 *
 *          The routine also determines the level structure associated with
 *          the given pseudo-peripheral node; that is, how far each node
 *          is from the pseudo-peripheral node.  The level structure is
 *          returned as a list of nodes LS, and pointers to the beginning
 *          of the list of nodes that are at a distance of 0, 1, 2, ...,
 *          NODE_NUM-1 from the pseudo-peripheral node.
 *
 *    @tparam T_Integer
 *
 *    @param root Input/output, T_Integer *ROOT.  On input, ROOT is a node in
 * the the component of the graph for which a pseudo-peripheral node is sought.
 * On output, ROOT is the pseudo-peripheral node obtained.
 *
 *    @param adj_num Input, T_Integer ADJ_NUM, the number of adjacency entries.
 *
 *    @param adj_row Input, T_Integer ADJ_ROW[NODE_NUM+1].  Information about
 * row I is stored in entries ADJ_ROW(I) through ADJ_ROW(I+1)-1 of ADJ.
 *
 *    @param adj Input, T_Integer ADJ[ADJ_NUM], the adjacency structure.
 *    For each row, it contains the column indices of the nonzero entries.
 *
 *    @param mask Input, T_Integer MASK[NODE_NUM], specifies a section subgraph.
 * Nodes for which MASK is zero are ignored by FNROOT.
 *
 *    @param level_num Output, T_Integer *LEVEL_NUM, is the number of levels in
 * the level structure rooted at the node ROOT.
 *
 *    @param level_row Output, T_Integer LEVEL_ROW(NODE_NUM+1)
 *
 *    @param level LEVEL(NODE_NUM), the
 *    level structure array pair containing the level structure found.
 *
 *    @param node_num Input, T_Integer NODE_NUM, the number of nodes.
 */
template <typename T_Integer>
void root_find(T_Integer &root,
               T_Integer adj_num,
               T_Integer adj_row[],
               T_Integer adj[],
               T_Integer mask[],
               T_Integer &level_num,
               T_Integer level_row[],
               T_Integer level[],
               const T_Integer node_num) {
  //
  //  Determine the level structure rooted at ROOT.
  //
  level_set(
      root, adj_num, adj_row, adj, mask, level_num, level_row, level, node_num);
  //
  //  Count the number of nodes in this level structure.
  //
  auto iccsze = level_row[level_num] - 1;
  //
  //  Extreme case:
  //    A complete graph has a level set of only a single level.
  //    Every node is equally good (or bad).
  //
  if (level_num == 1) {
    return;
  }
  //
  //  Extreme case:
  //    A "line graph" 0--0--0--0--0 has every node in its only level.
  //    By chance, we've stumbled on the ideal root.
  //
  if (level_num == iccsze) {
    return;
  }
  //
  //  Pick any node from the last level that has minimum degree
  //  as the starting point to generate a new level set.
  //
  T_Integer level_num2;
  for (;;) {
    auto mindeg = iccsze;
    auto jstrt = level_row[level_num - 1];
    root = level[jstrt - 1];

    if (jstrt < iccsze) {
      for (T_Integer j = jstrt; j <= iccsze; j++) {
        auto node = level[j - 1];
        auto ndeg = 0;
        auto kstrt = adj_row[node - 1];
        auto kstop = adj_row[node] - 1;

        for (T_Integer k = kstrt; k <= kstop; k++) {
          auto nabor = adj[k - 1];
          if (0 < mask[nabor - 1]) {
            ndeg = ndeg + 1;
          }
        }

        if (ndeg < mindeg) {
          root = node;
          mindeg = ndeg;
        }
      }
    }
    //
    //  Generate the rooted level structure associated with this node.
    //
    level_set(root,
              adj_num,
              adj_row,
              adj,
              mask,
              level_num2,
              level_row,
              level,
              node_num);
    //
    //  If the number of levels did not increase, accept the new ROOT.
    //
    if (level_num2 <= level_num) {
      break;
    }

    level_num = level_num2;
    //
    //  In the unlikely case that ROOT is one endpoint of a line graph,
    //  we can exit now.
    //
    if (iccsze <= level_num) {
      break;
    }
  }

  return;
}

/**
 * @brief RCM renumbers a connected component by the reverse Cuthill McKee
 *        algorithm.
 *
 * @details The connected component is specified by a node ROOT and a mask.
 *          The numbering starts at the root node.
 *
 *          An outline of the algorithm is as follows:
 *
 *          X(1) = ROOT
 *
 *          for ( I = 1 to N-1)
 *            Find all unlabelled neighbors of X(I),
 *            assign them the next available labels, in order of increasing
 *            degree. When done, reverse the ordering.
 *
 * @tparam T_Integer
 * @param root Input, T_Integer ROOT, the node that defines the connected
 * component. It is used as the starting point for the RCM ordering.
 * @param adj_num Input, T_Integer ADJ_NUM, the number of adjacency entries.
 * @param adj_row Input, T_Integer ADJ_ROW[NODE_NUM+1].  Information about row I
 * is stored in entries ADJ_ROW(I) through ADJ_ROW(I+1)-1 of ADJ.
 * @param adj Input, T_Integer ADJ[ADJ_NUM], the adjacency structure.
 *            For each row, it contains the column indices of the nonzero
 * entries.
 * @param mask Input/output, T_Integer MASK[NODE_NUM], a mask for the nodes.
 * Only those nodes with nonzero input mask values are considered by the
 *             routine.  The nodes numbered by RCM will have their mask values
 *             set to zero.
 * @param perm Output, T_Integer PERM[NODE_NUM], the RCM ordering.
 * @param iccsze Output, T_Integer *he size of the connected component
 *               that has been numbered.
 * @param node_num Input, T_Integer NODE_NUM, the number of nodes.
 *
 *        Local Parameters:
 *
 *          Workspace, T_Integer DEG[NODE_NUM], a temporary vector used to hold
 *          the degree of the nodes in the section graph specified by mask and
 * root.
 *
 */
template <typename T_Integer>
void rcm(T_Integer root,
         T_Integer adj_num,
         T_Integer adj_row[],
         T_Integer adj[],
         T_Integer mask[],
         T_Integer perm[],
         T_Integer &iccsze,
         const T_Integer node_num) {
  //
  //  If node_num out of bounds, something is wrong.
  //
  if (node_num < 1) {
    // fmt::print(
    //     "\nRCM - Fatal error!\n  Unacceptable input value of NODE_NUM =
    //     {}\n", node_num);
    exit(1);
  }
  //
  //  If the root is out of bounds, something is wrong.
  //
  if (root < 1 || node_num < root) {
    // fmt::print(
    //     "\nRCM - Fatal error!\n  Unacceptable input value of ROOT = {}\n",
    //     root);
    // fmt::print("  Acceptable values are between 1 and {}, inclusive.\n",
    //            node_num);
    exit(1);
  }
  //
  //  Allocate memory for the degree array.
  //
  std::vector<T_Integer> deg(node_num);
  //
  //  Find the degrees of the nodes in the component specified by MASK and ROOT.
  //
  degree(root, adj_num, adj_row, adj, mask, deg.data(), iccsze, perm, node_num);
  //
  //  If the connected component size is less than 1, something is wrong.
  //
  if (iccsze < 1) {
    // fmt::print("\nRCM - Fatal error!\n  Connected component size ICCSZE "
    //            "returned from DEGREE as {}\n",
    //            iccsze);
    exit(1);
  }
  //
  //  Set the mask value for the root.
  //
  mask[root - 1] = 0;
  //
  //  If the connected component is a singleton, there is no ordering necessary.
  //
  if (iccsze == 1) {
    return;
  }
  //
  //  Carry out the reordering.
  //
  //  LBEGIN and LVLEND point to the beginning and
  //  the end of the current level respectively.
  //
  T_Integer lvlend = 0;
  T_Integer lnbr = 1;

  while (lvlend < lnbr) {
    auto lbegin = lvlend + 1;
    lvlend = lnbr;

    for (T_Integer i = lbegin; i <= lvlend; i++) {
      //
      //  For each node in the current level...
      //
      auto node = perm[i - 1];
      auto jstrt = adj_row[node - 1];
      auto jstop = adj_row[node] - 1;
      //
      //  Find the unnumbered neighbors of NODE.
      //
      //  FNBR and LNBR point to the first and last neighbors
      //  of the current node in PERM.
      //
      auto fnbr = lnbr + 1;

      for (T_Integer j = jstrt; j <= jstop; j++) {
        auto nbr = adj[j - 1];

        if (mask[nbr - 1] != 0) {
          lnbr = lnbr + 1;
          mask[nbr - 1] = 0;
          perm[lnbr - 1] = nbr;
        }
      }
      //
      //  If no neighbors, skip to next node in this level.
      //
      if (lnbr <= fnbr) {
        continue;
      }
      //
      //  Sort the neighbors of NODE in increasing order by degree.
      //  Linear insertion is used.
      //
      auto k = fnbr;

      while (k < lnbr) {
        auto l = k;
        k = k + 1;
        auto nbr = perm[k - 1];

        while (fnbr < l) {
          auto lperm = perm[l - 1];

          if (deg[lperm - 1] <= deg[nbr - 1]) {
            break;
          }

          perm[l] = lperm;
          l = l - 1;
        }
        perm[l] = nbr;
      }
    }
  }
  //
  //  We now have the Cuthill-McKee ordering.
  //  Reverse it to get the Reverse Cuthill-McKee ordering.
  //
  std::reverse(perm, perm + iccsze);
  return;
}

} // namespace details

/**
 * @brief GetOrderingRCM Finds the Reverse Cuthill-Mckee ordering for a general
 * graph
 *
 * @details For each connected component in the graph, the routine obtains
 *          an ordering by calling RCM
 *
 * @tparam T_Integer
 * @param adj_row Input, T_Integer ADJ_ROW[NODE_NUM+1].  Information about row I
 * is stored in entries ADJ_ROW(I) through ADJ_ROW(I+1)-1 of ADJ
 * @param adj     Input, T_Integer  ADJ[ADJ_NUM], the adjacency structure
 *                For each row, it contains the column indices of the nonzero
 * entries.
 * @param c_order Input, BOOL, True if C ordering is used for adj structue
 * @return std::tuple<std::vector<T_Integer>, std::vector<T_Integer>>
 * @param perm    Output, T_Integer  PERM[NODE_NUM], the RCM ordering (reorder)
 * @param iperm    Output, T_Integer  IPERM[NODE_NUM], the RCM inverse ordering
 * (renumber)
 */
template <typename T_Integer>
std::tuple<std::vector<T_Integer>, std::vector<T_Integer>>
GetOrderingRCM(std::vector<T_Integer> &adj_row,
               std::vector<T_Integer> &adj,
               const bool c_order = false) {
  typedef decltype(adj.size()) T_SizeInt;
  // Convert C ordering to Fortran (TODO: Fix this to be C ordering always)
  if (c_order) {
    for (auto &item : adj_row)
      ++item;
    for (auto &item : adj)
      ++item;
  }
  /*
   *  Local Parameters:
   *
   *    Local, T_Integer  LEVEL_ROW[NODE_NUM+1], the index vector for a level
   *    structure.  The level structure is stored in the currently unused
   *    spaces in the permutation vector PERM.
   *
   *    Local, T_Integer MASK[NODE_NUM], marks variables that have been
   * numbered.
   */
  T_Integer node_num = adj_row.size() - 1, adj_num = adj.size();
  std::vector<T_Integer> perm(node_num);
  std::vector<T_Integer> iperm(node_num);
  std::vector<T_Integer> level_row(node_num + 1);
  std::vector<T_Integer> mask(node_num, 1);

  T_Integer num = 1;
  T_Integer iccsze;
  T_Integer level_num;

  for (T_Integer i = 0; i < node_num; i++) {
    //
    //  For each masked connected component...
    //
    if (mask[i] != 0) {
      auto root = i + 1;
      //
      //  Find a pseudo-peripheral node ROOT.  The level structure found by
      //  ROOT_FIND is stored starting at PERM(NUM).
      //
      details::root_find(root,
                         adj_num,
                         adj_row.data(),
                         adj.data(),
                         mask.data(),
                         level_num,
                         level_row.data(),
                         perm.data() + num - 1,
                         node_num);
      //
      //  RCM orders the component using ROOT as the starting node.
      //
      details::rcm(root,
                   adj_num,
                   adj_row.data(),
                   adj.data(),
                   mask.data(),
                   perm.data() + num - 1,
                   iccsze,
                   node_num);

      num = num + iccsze;
      //
      //  We can stop once every node is in one of the connected components.
      //
      if (node_num < num) {
        for (T_SizeInt ip = 0; ip < perm.size(); ++ip) {
          iperm[perm[ip] - 1] = ip + 1;
        }
        if (c_order) {
          for (T_SizeInt ip = 0; ip < perm.size(); ++ip) {
            --iperm[ip];
            --perm[ip];
          }
        }
        return std::make_tuple(std::move(perm), std::move(iperm));
      }
    }
  }
  for (T_SizeInt ip = 0; ip < perm.size(); ++ip) {
    iperm[perm[ip] - 1] = ip + 1;
  }
  if (c_order) {
    for (T_SizeInt ip = 0; ip < perm.size(); ++ip) {
      --iperm[ip];
      --perm[ip];
    }
  }
  return std::make_tuple(std::move(perm), std::move(iperm));
}

/**
 * @brief Matlab like Spy format for SpMat viz
 *
 * @tparam T_Container
 * @param spy_file File name to output the sparsity data
 * @param left The left cellIDs
 * @param right The right cellIDs
 * @param reverse Set to "true" for upper triangular or "false" otherwise
 */
template <typename T_Container>
void write_spy(const std::string spy_file,
               const T_Container &left,
               const T_Container &right,
               const bool reverse = false) {
  typedef decltype(left.size()) T_SizeInt;
  // fmt::print("Writing sparsity data to {}\n", spy_file);
  std::ofstream fspy(spy_file);
  if (reverse) {
    for (T_SizeInt i = 0; i < left.size(); ++i) {
      auto i1 = std::min(left[i], right[i]);
      auto i2 = std::max(left[i], right[i]);
      // fspy << fmt::format("{} {}\n", i1, i2);
    }
  } else {
    for (T_SizeInt i = 0; i < left.size(); ++i) {
      auto i1 = std::max(left[i], right[i]);
      auto i2 = std::min(left[i], right[i]);
      // fspy << fmt::format("{} {}\n", i1, i2);
    }
  }
}

/**
 * @brief Matlab like Spy format for SpMat viz based on distance from diagonal
 *
 * @tparam T_Container
 * @param spy_file File name to output the sparsity data
 * @param left The left cellIDs
 * @param right The right cellIDs
 * @param reverse Set to "true" for upper triangular or "false" otherwise
 */
template <typename T_Container>
void write_spy_distance(const std::string spy_file,
                        const T_Container &left,
                        const T_Container &right,
                        const bool reverse = false) {
  typedef decltype(left.size()) T_SizeInt;
  // fmt::print("Writing sparsity data to {}\n", spy_file);
  std::ofstream fspy(spy_file);
  if (reverse) {
    for (T_SizeInt i = 0; i < left.size(); ++i) {
      auto i1 = std::min(left[i], right[i]);
      auto i2 = std::max(left[i], right[i]) - i1;
      // fspy << fmt::format("{} {}\n", i1, i2);
    }
  } else {
    for (T_SizeInt i = 0; i < left.size(); ++i) {
      auto i2 = std::min(left[i], right[i]);
      auto i1 = std::max(left[i], right[i]) - i2;
      // fspy << fmt::format("{} {}\n", i1, i2);
    }
  }
}

} // End of namespace taru

#endif
