# External dependency that are not automatically handled by the
# library.

#################################
### Zoltan Graph partitioner ###
#################################
include(ExternalProject)
# Zoltan dependency package installer
# This is still experimental so report problems if found
if(TARU_ENABLE_BUNDLEDLIB)
  set(ZOLTAN_DIR ${CMAKE_BINARY_DIR}/thirdparty/zoltan)
else()
  set(ZOLTAN_DIR ${CMAKE_INSTALL_PREFIX})
endif()
set(ZOLTAN_STATIC_LIB ${ZOLTAN_DIR}/lib/libzoltan.a)
set(ZOLTAN_IDTYPE "uint")
if(TARU_USE_INT64_GIDS)
  set(ZOLTAN_IDTYPE "ulong")
endif()
if(TARU_ENABLE_DOWNLOAD)
  set(USE_ZOLTAN_URL https://github.com/sandialabs/Zoltan/archive/refs/tags/v3.901.tar.gz)
else()
  set(USE_ZOLTAN_URL ${CMAKE_SOURCE_DIR}/thirdparty/Zoltan-v3.901.tar.gz)
endif()
ExternalProject_Add(libzoltan
  URL ${USE_ZOLTAN_URL}
  INSTALL_DIR ${ZOLTAN_DIR}
  CONFIGURE_COMMAND CC=${MPI_C_COMPILER} CXX=${MPI_CXX_COMPILER} <SOURCE_DIR>/configure --with-id-type=${ZOLTAN_IDTYPE} --prefix=<INSTALL_DIR>
  BUILD_COMMAND make
  INSTALL_COMMAND make install
  BUILD_BYPRODUCTS ${ZOLTAN_STATIC_LIB}
)
add_library(zoltan STATIC IMPORTED GLOBAL)
add_dependencies(zoltan libzoltan)
set_target_properties(zoltan PROPERTIES IMPORTED_LOCATION ${ZOLTAN_STATIC_LIB})
if(NOT EXISTS ${ZOLTAN_DIR}/include)
  file(MAKE_DIRECTORY ${ZOLTAN_DIR}/include)
endif()
set_target_properties(zoltan PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${ZOLTAN_DIR}/include)
set(TARU_PART_LIBS ${TARU_PART_LIBS} zoltan)

#################################
### Scotch Graph partitioner ####
#################################
set(SCOTCH_INTSIZE_STRING "INTSIZE 32") # Default size is 32bit integer unless 64bit is specified
if(TARU_USE_INT64_GIDS)
  set(SCOTCH_INTSIZE_STRING "INTSIZE 64")
endif()

set(SCOTCH_EXTRA_OPTIONS)
if(APPLE)
  if(NOT BISON_EXECUTABLE OR BISON_EXECUTABLE STREQUAL "")
    MESSAGE(FATAL_ERROR "Explicity specify homebrew version of GNU bison using BISON_EXECUTABLE")
  endif()

  if(NOT FLEX_EXECUTABLE OR FLEX_EXECUTABLE STREQUAL "")
    MESSAGE(FATAL_ERROR "Explicity specify homebrew version of GNU bison using FLEX_EXECUTABLE")
  endif()

  set(SCOTCH_EXTRA_OPTIONS "BISON_EXECUTABLE ${BISON_EXECUTABLE}" "FLEX_EXECUTABLE ${FLEX_EXECUTABLE}")
endif()

# PT-Scotch
if(NOT TARU_ENABLE_DOWNLOAD)
  CPMAddPackage(
    NAME SCOTCH
    URL ${CMAKE_SOURCE_DIR}/thirdparty/scotch-v7.0.4.tar.gz
    URL_HASH SHA256=8ef4719d6a3356e9c4ca7fefd7e2ac40deb69779a5c116f44da75d13b3d2c2c3
    OPTIONS "MPI_THREAD_MULTIPLE OFF" "BUILD_LIBESMUMPS OFF" "THREADS OFF" "${SCOTCH_INTSIZE_STRING}" "${SCOTCH_EXTRA_OPTIONS}" "CMAKE_C_FLAGS -std=c99" "BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS}" "BUILD_LIBSCOTCHMETIS OFF" "SCOTCH_DETERMINISTIC ON" "COMMON_RANDOM_FIXED_SEED ON"
    EXCLUDE_FROM_ALL ${TARU_ENABLE_BUNDLEDLIB}
  )
else()
  CPMAddPackage(
    NAME SCOTCH
    URL https://gitlab.inria.fr/scotch/scotch/-/archive/v7.0.4/scotch-v7.0.4.tar.gz
    OPTIONS "MPI_THREAD_MULTIPLE OFF" "BUILD_LIBESMUMPS OFF" "THREADS OFF" "${SCOTCH_INTSIZE_STRING}" "${SCOTCH_EXTRA_OPTIONS}" "CMAKE_C_FLAGS -std=c99" "BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS}" "BUILD_LIBSCOTCHMETIS OFF" "SCOTCH_DETERMINISTIC ON" "COMMON_RANDOM_FIXED_SEED ON"
    EXCLUDE_FROM_ALL ${TARU_ENABLE_BUNDLEDLIB}
  )
endif()
set(TARU_PART_LIBS ${TARU_PART_LIBS} ptscotch scotch scotcherr)


#################################################################
##     METIS package dependency (not handled inside TARU)      ##
#################################################################
find_package(METIS)
if(METIS_FOUND)

  # Make sure 64-bit version of TARU uses METIS with 64-bit idx_t support
  if(TARU_USE_INT64_GIDS)
    if(METIS_IDX_T EQUAL 64)
      MESSAGE("Type idx_t of METIS matches size of id_size_t")
    else()
      MESSAGE(WARNING "Type idx_t of METIS library does not match idx_size_t")
      unset(METIS_FOUND)
    endif()
  endif()

  # Make sure 32-bit version of TARU uses METIS with 32-bit idx_t support
  if(NOT TARU_USE_INT64_GIDS)
   if(METIS_IDX_T EQUAL 32)
      MESSAGE("Type idx_t of METIS matches size of id_size_t")
    else()
      MESSAGE(WARNING "Type idx_t of METIS library does not match id_size_t")
      unset(METIS_FOUND)
    endif()
  endif()

  # Make sure real_t of METIS and double are of same size
  if(METIS_REAL_T EQUAL 64)
    MESSAGE("Type real_t of METIS matches size of double")
  else()
    MESSAGE(WARNING "Type real_t of METIS does not match size of double")
    unset(METIS_FOUND)
  endif()

endif()

#################################################################
##    ParMETIS package dependency (not handled inside TARU)    ##
#################################################################
find_package(PARMETIS)
if(PARMETIS_FOUND)

  # Make sure 64-bit version of TARU uses PARMETIS with 64-bit idx_t support
  if(TARU_USE_INT64_GIDS)
    if(PARMETIS_IDX_T EQUAL 64)
      MESSAGE("Type idx_t of PARMETIS matches size of id_size_t")
    else()
      MESSAGE(WARNING "Type idx_t of PARMETIS library does not match idx_size_t")
      unset(PARMETIS_FOUND)
    endif()
  endif()

  # Make sure 32-bit version of TARU uses PARMETIS with 32-bit idx_t support
  if(NOT TARU_USE_INT64_GIDS)
   if(PARMETIS_IDX_T EQUAL 32)
      MESSAGE("Type idx_t of PARMETIS matches size of id_size_t")
    else()
      MESSAGE(WARNING "Type idx_t of PARMETIS library does not match id_size_t")
      unset(PARMETIS_FOUND)
    endif()
  endif()

  # Make sure real_t of PARMETIS and double are of same size
  if(PARMETIS_REAL_T EQUAL 64)
    MESSAGE("Type real_t of PARMETIS matches size of double")
  else()
    MESSAGE(WARNING "Type real_t of PARMETIS does not match size of double")
    unset(PARMETIS_FOUND)
  endif()

endif()

if(METIS_FOUND AND PARMETIS_FOUND)
  set(METIS_DEFINE "#define TARU_USE_METIS")
  set(TARU_PART_LIBS ${TARU_PART_LIBS} PARMETIS::PARMETIS METIS::METIS)
else()
  set(METIS_DEFINE "")
endif()

# # KaHIP graph partitioner
# if(TARU_ENABLE_DOWNLOAD)
#   CPMAddPackage(
#     NAME KaHIP
#     GIT_TAG v3.16
#     GITHUB_REPOSITORY KaHIP/KaHIP
#     OPTIONS "DETERMINISTIC_PARHIP ON"
#     EXCLUDE_FROM_ALL ON
#   )
# else()
#   CPMAddPackage(
#     NAME kahip
#     URL ${CMAKE_SOURCE_DIR}/thirdparty/KaHIP-3.16.tar.gz
#     URL_HASH SHA256=b0ef72a26968d37d9baa1304f7a113b61e925966a15e86578d44e26786e76c75
#     OPTIONS "DETERMINISTIC_PARHIP ON"
#     EXCLUDE_FROM_ALL ON
#   )
# endif()
# set(TARU_PART_LIBS ${TARU_PART_LIBS} kahip_static)
