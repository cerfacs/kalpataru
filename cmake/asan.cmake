
if(TARU_ENABLE_ASAN)
  include(CheckCXXCompilerFlag)
  set(CMAKE_REQUIRED_FLAGS "-Werror -fsanitize=address -static-libasan") # Also needs to be a link flag for test to pass
  CHECK_CXX_COMPILER_FLAG( "-fsanitize=address -static-libasan" HAVE_FLAG_SANITIZE_ADDRESS)
  unset(CMAKE_REQUIRED_FLAGS)

  if (HAVE_FLAG_SANITIZE_ADDRESS)
      set(TARU_CFLAGS ${TARU_CFLAGS} -fsanitize=address -static-libasan)
      set(TARU_LDFLAGS ${TARU_LDFLAGS} -fsanitize=address -static-libasan)
  else ()
      message(FATAL_ERROR "ASAN not found for memory leak checks (please set TARU_ENABLE_ASAN=OFF)!")
  endif (HAVE_FLAG_SANITIZE_ADDRESS)
endif ()
