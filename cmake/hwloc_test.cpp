/** @file hwloc_test.cpp
 *  @author mohanamuraly
 *  @date 11 May 2019
 *  @brief Documentation for hwloc_test.cpp
 *  @copyright CERFACS, 42 Avenue Gaspard Coriolis, 31100, Toulouse 
*/
#include <iostream>
#include <hwloc.h>

int main( int nargs, char *args[] ) {
  hwloc_topology_t topo;
  
  hwloc_topology_init(&topo);
  hwloc_topology_load(topo);
  
  std::cerr << "Num sockets = "
            << hwloc_get_nbobjs_by_type(topo, HWLOC_OBJ_PACKAGE)
            << "\n";
  std::cerr << "Num cores = "
            << hwloc_get_nbobjs_by_type(topo, HWLOC_OBJ_CORE)
            << "\n";
  std::cerr << "Num threads = "
            << hwloc_get_nbobjs_by_type(topo, HWLOC_OBJ_PU)
            << "\n";
  std::cerr << "Num NUMA nodes = "
            << hwloc_get_nbobjs_by_type(topo, HWLOC_OBJ_NUMANODE)
            << "\n";
  hwloc_topology_destroy(topo);
  return 0;
}
