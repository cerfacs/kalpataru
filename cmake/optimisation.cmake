if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    # using LLVM/Clang
    if (CMAKE_BUILD_TYPE MATCHES Release)
        set(TARU_CFLAGS ${TARU_CFLAGS} -Wall -Wno-unused-function -Wno-unknown-pragmas -O3 -g)
    else ()
        set(TARU_CFLAGS ${TARU_CFLAGS} -Wall -Wno-unused-function -Wno-unknown-pragmas -g -DSCOTCH_DEBUG_ALL)
    endif ()
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    # using GCC
    if (CMAKE_BUILD_TYPE MATCHES Release)
        set(TARU_CFLAGS ${TARU_CFLAGS} -Wall -Wno-unused-function -O3 -g -Wno-unknown-pragmas)
    else ()
        set(TARU_CFLAGS ${TARU_CFLAGS} -Wall -Wno-unused-function -Wno-unknown-pragmas -g -fbounds-check -DSCOTCH_DEBUG_ALL)
    endif ()
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
    # using Intel C++
    if (CMAKE_BUILD_TYPE MATCHES Release)
        set(TARU_CFLAGS ${TARU_CFLAGS} -Wall -O3 -g -Wno-unknown-pragmas)
    else ()
        set(TARU_CFLAGS ${TARU_CFLAGS} -Wall -Wno-unused-function -g -Wno-unknown-pragmas -DSCOTCH_DEBUG_ALL)
    endif ()
else ()
    # using Visual Studio C++
endif ()
